# BSD 3-Clause License
#
# Copyright (c) 2024, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#
# Makefile (GNU Make 3.81)
#

.PHONY: all clean docs test wperf wperf-driver wperf-test wperf-lib
.PHONY: package regenerate sign

#
# *** INTRODUCTION ***
#
# Now when we have unit testing project `wperf-test` we depend on x64
# wperf build to link tests with unit test project. This change makes sure
# we build `wperf-test` for arch=x64. This will build `wperf` and
# `wperf-test` (later depends on `wperf`).
#
# So `make all` will:
#
#     1) Build `wperf` arch=x64
#     2) Build `wperf-test` arch=x64
#     3) Rebuild solution for `config` and `arch` (default ARM64)
#
# Please note we are building:
#
#     * cross x64 -> ARM64 for WindowsPerf suite and
#     * x64 -> x64 `wperf` and its unit tests.
#
# *** HOW TO SWITCH BETWEEN Debug/Release AND ARM64/x64 ***
#
# Use 'config' to switch between `Debug` and `Release`.
# Use 'arch' to switch between `ARM64` and `x64`.
#
# Note: `wperf-test` project require `wperf` project to be built first.
# Currently WindowsPerf solution defines this build dependency and you can
# just straight build `make ... wperf-test` and dependencies will build.
#
# Examples:
#
#     make config=Release all
#     make config=Debug all
#
#     make config=Release arch=x64 wperf-test
#     make config=Debug arch=x64 wperf-test
#
# *** BUILDING UNIT TESTS ***
#
# Note: You can currently build unit tests only on x64 host!
#
#     make test                                          (Default Debug/x64)
#     make config=Release test                           (Release/x64)
#
# or more verbose version of above:
#
#     make wperf-test wperf-test-run                     (Default Debug/x64)
#     make config=Release wperf-test wperf-test-run      (Release/x64)
#
# *** RELEASE BINARY PACKAGING ***
#
# This Makefile automates now how we release binaries for WindowsPerf,
# including signing.
#
# In `Dev Prompt for VS 2022`:
#
# > make config=Release all
# > make config=Debug+CMN all
#
# In Bash environment, e.g. `Git Bash`:
#
# $ make config=Release package
# $ make config=Debug+CMN package
#
# $ make sign server=<Server-name> config=Release
# $ make sign server=<Server-name> config=Debug+CMN
#

# By default we build for ARM64 target. Define arch variable to change default
# value.
make_arch=ARM64

# By default we build with Debug configuration. Define config variable to change default
# value.
make_config=Release

ifdef arch
	make_arch=$(arch)
endif

ifdef config
	make_config=$(config)
endif

ifdef server
	make_server=$(server)
endif

#
# Building rules
#
all: wperf-test
	devenv windowsperf.sln /Rebuild "$(make_config)|$(make_arch)" 2>&1

wperf:
	devenv windowsperf.sln /Rebuild "$(make_config)|${make_arch}" /Project wperf\wperf.vcxproj 2>&1

wperf-driver:
	devenv windowsperf.sln /Rebuild "$(make_config)|$(make_arch)" /Project wperf-driver\wperf-driver.vcxproj 2>&1

wperf-test:
	devenv windowsperf.sln /Rebuild "$(make_config)|x64" /Project wperf-test\wperf-test.vcxproj 2>&1

wperf-test-run:
	vstest.console wperf-test\x64\$(make_config)\wperf-test.dll

wperf-lib:
	devenv windowsperf.sln /Rebuild "$(make_config)|${make_arch}" /Project wperf-lib\wperf-lib.vcxproj 2>&1

test: wperf-test wperf-test-run

#
# Regenerate .def files
#
regenerate:
# Regenerate Telemetry-Solution .def file (Add LICENSE and add meta-data)
	sed -e 's#^#\/\/ #' LICENSE > wperf-common/telemetry-solution-data.def
	sed -i 's#[[:space:]]*$$##' wperf-common/telemetry-solution-data.def
	python wperf-scripts/telemetry_events_update.py >> wperf-common/telemetry-solution-data.def

#
# Simple mechanism to "flat" package build artifacts in one directory
#
package:
	rm -rf release/$(make_config)
	mkdir -p release/$(make_config)
#
# wperf and wperf-devgen. files
#
	cp -p "./wperf/$(make_arch)/$(make_config)/wperf.exe" release/$(make_config)
	cp -p "./wperf/$(make_arch)/$(make_config)/wperf-etw.rc" release/$(make_config)
	cp -p "./wperf/$(make_arch)/$(make_config)/wperf-etw_MSG00001.bin" release/$(make_config)
	cp -p "./wperf/$(make_arch)/$(make_config)/wperf-etwTEMP.BIN" release/$(make_config)
	cp -p "./wperf/wperf-app-wpr-profile.wprp" release/$(make_config)
	cp -p "./wperf/wperf-etw-manifest.xml" release/$(make_config)
	cp -p "./wperf-devgen/$(make_arch)/$(make_config)/wperf-devgen.exe" release/$(make_config)
#
# wperf-lib files
#
	cp -p "./wperf-lib/$(make_arch)/$(make_config)/wperf-lib.dll" release/$(make_config)
	cp -p "./wperf-lib/$(make_arch)/$(make_config)/wperf-lib.lib" release/$(make_config)
	cp -p "./wperf-lib/wperf-lib.h" release/$(make_config)
	cp -p "./wperf-lib-app/$(make_arch)/$(make_config)/wperf-lib-app.exe" release/$(make_config)
	cp -p "./wperf-lib-app/wperf-lib-c-compat/$(make_arch)/$(make_config)/wperf-lib-c-compat.exe" release/$(make_config)
	cp -p "./wperf-lib-app/wperf-lib-timeline/$(make_arch)/$(make_config)/wperf-lib-timeline.exe" release/$(make_config)
#
# Driver files
#
	cp -p "./wperf-driver/$(make_arch)/$(make_config)/wperf-driver/wperf-driver.sys" release/$(make_config)
	cp -p "./wperf-driver/$(make_arch)/$(make_config)/wperf-driver/wperf-driver.cat" release/$(make_config)
	cp -p "./wperf-driver/$(make_arch)/$(make_config)/wperf-driver/wperf-driver.inf" release/$(make_config)
	cp -p "./wperf-driver/$(make_arch)/$(make_config)/wperf-driver.pdb" release/$(make_config)
	cp -p "./wperf-driver/$(make_arch)/$(make_config)/wperf-driver-etw.rc" release/$(make_config)
	cp -p "./wperf-driver/$(make_arch)/$(make_config)/wperf-driver-etw_MSG00001.bin" release/$(make_config)
	cp -p "./wperf-driver/$(make_arch)/$(make_config)/wperf-driver-etwTEMP.BIN" release/$(make_config)
	cp -p "./wperf-driver/wperf-driver-etw-manifest.xml" release/$(make_config)
	cp -p "./wperf-driver/wperf-wpr-profile.wprp" release/$(make_config)

#
# Use sign script to sign release artifacts.
#
# Note:
#   1. Specify internal server with `EV certificate` signing.
#   2. Make sure your SSH connection will not ask for password multiple times
#      during script execution:
#
#       $ eval "$(ssh-agent -s)"
#       $ ssh-add ~/.ssh/id_rsa
#
sign:
	./wperf-scripts/sign-ev-certificate.sh $(make_server) release/$(make_config)

# Run signing (internal to Arm)

clean:
	devenv windowsperf.sln /Clean "$(make_config)|$(make_arch)" 2>&1

purge:
	rm -rf wperf/ARM64 wperf/ARM64EC wperf/x64
	rm -rf wperf-driver/ARM64 wperf-driver/ARM64EC wperf-driver/x64
	rm -rf wperf-test/ARM64 wperf-test/ARM64EC wperf-test/x64
	rm -rf wperf-lib/ARM64 wperf-lib/ARM64EC wperf-lib/x64
	rm -rf wperf-devgen/ARM64 wperf-devgen/x64
	rm -rf ARM64/ ARM64EC/ x64/

docs:
	doxygen

docs-clean:
	rm -rf docs/ html/ latex/
