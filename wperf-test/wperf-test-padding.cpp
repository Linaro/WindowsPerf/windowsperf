// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "pch.h"
#include "CppUnitTest.h"

#include <algorithm>
#include <windows.h>
#include "wperf/events.h"
#include "wperf/parsers.h"
#include "wperf/padding.h"

using namespace Microsoft::VisualStudio;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace wperftest_common
{
	TEST_CLASS(wperftest_padding)
	{
	public:
		TEST_METHOD(test_padding_gpc_nums_5_no_groups)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 5;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"inst_spec,vfp_spec,ase_spec,br_immed_spec,crypto_spec,ld_spec,st_spec", note, pmu_cfg);

			Assert::AreEqual<size_t>(7, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(7, ioctl_events[EVT_CORE].size());
		}

		TEST_METHOD(test_padding_gpc_nums_5_groups_331)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 5;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{inst_spec,vfp_spec,ase_spec},{br_immed_spec,crypto_spec,ld_spec},st_spec", note, pmu_cfg);

			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(10, ioctl_events[EVT_CORE].size());
		}

		TEST_METHOD(test_padding_gpc_nums_6_groups_53)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"inst_spec,vfp_spec,ase_spec,dp_spec,ld_spec,{st_spec,br_immed_spec,crypto_spec}", note, pmu_cfg);

			Assert::AreEqual<size_t>(5, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(12, ioctl_events[EVT_CORE].size());
		}

		TEST_METHOD(test_padding_gpc_nums_5_groups_233)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{inst_spec,vfp_spec},ase_spec,dp_spec,ld_spec,{st_spec,br_immed_spec,crypto_spec}", note, pmu_cfg);

			Assert::AreEqual<size_t>(3, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(12, ioctl_events[EVT_CORE].size());
		}

		TEST_METHOD(test_padding_gpc_nums_5__rf)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 5;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{rf}", note, pmu_cfg);

			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(1, ioctl_events[EVT_CORE].size());

			size_t padding = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_PADDING; });
			size_t normal = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_NORMAL; });
			size_t grouped = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_GROUPED; });

			Assert::AreEqual<size_t>(0, padding);
			Assert::AreEqual<size_t>(1, normal);
			Assert::AreEqual<size_t>(0, grouped);
		}

		TEST_METHOD(test_padding_gpc_nums_6__rf)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{rf}", note, pmu_cfg);

			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(1, ioctl_events[EVT_CORE].size());

			size_t padding = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_PADDING; });
			size_t normal = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_NORMAL; });
			size_t grouped = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_GROUPED; });

			Assert::AreEqual<size_t>(0, padding);
			Assert::AreEqual<size_t>(1, normal);
			Assert::AreEqual<size_t>(0, grouped);
		}

		TEST_METHOD(test_padding_gpc_nums_6__3_group_mix)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{inst_spec,vfp_spec},{ase_spec,br_immed_spec},crypto_spec,{ld_spec,st_spec}", note, pmu_cfg);

			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(0, ioctl_events[EVT_CORE].size() % pmu_cfg.gpc_nums[EVT_CORE]);
			Assert::AreEqual<size_t>(12, ioctl_events[EVT_CORE].size());

			size_t padding = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_PADDING; });
			size_t normal = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_NORMAL; });
			size_t grouped = std::count_if(ioctl_events[EVT_CORE].begin(), ioctl_events[EVT_CORE].end(), [](auto& e) { return e.type == EVT_GROUPED; });

			Assert::AreEqual<size_t>(5, padding);
			Assert::AreEqual<size_t>(1, normal);
			Assert::AreEqual<size_t>(6, grouped);
		}

		TEST_METHOD(test_padding_gpc_nums_5__topdown)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 5;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{l1i_tlb_refill,l1i_tlb},{inst_spec,ld_spec},"
				"{st_spec,inst_spec},{inst_retired,br_mis_pred_retired},"
				"{l1d_tlb_refill,inst_retired},{itlb_walk,l1i_tlb},"
				"{inst_spec,dp_spec},{l2d_tlb_refill,inst_retired},"
				"{stall_backend,cpu_cycles},{crypto_spec,inst_spec},"
				"{l1i_tlb_refill,inst_retired},{ll_cache_rd,ll_cache_miss_rd},"
				"{inst_retired,cpu_cycles},{l2d_cache_refill,l2d_cache},"
				"{br_indirect_spec,inst_spec,br_immed_spec},{l2d_tlb_refill,l2d_tlb},"
				"{inst_retired,itlb_walk},{dtlb_walk,inst_retired},"
				"{l1i_cache,l1i_cache_refill},{l1d_cache_refill,l1d_cache},"
				"{br_mis_pred_retired,br_retired},{inst_retired,l2d_cache_refill},"
				"{vfp_spec,inst_spec},{stall_frontend,cpu_cycles},"
				"{inst_spec,ase_spec},{ll_cache_rd,ll_cache_miss_rd},"
				"{inst_retired,l1d_cache_refill},{l1d_tlb,dtlb_walk},"
				"{inst_retired,ll_cache_miss_rd},{l1i_cache_refill,inst_retired},"
				"{l1d_tlb_refill,l1d_tlb}", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(0, ioctl_events[EVT_CORE].size() % pmu_cfg.gpc_nums[EVT_CORE]);
			Assert::AreEqual<size_t>(80, ioctl_events[EVT_CORE].size());
		}

		TEST_METHOD(test_padding_gpc_nums_6__topdown)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{l1i_tlb_refill,l1i_tlb},{inst_spec,ld_spec},"
				"{st_spec,inst_spec},{inst_retired,br_mis_pred_retired},"
				"{l1d_tlb_refill,inst_retired},{itlb_walk,l1i_tlb},"
				"{inst_spec,dp_spec},{l2d_tlb_refill,inst_retired},"
				"{stall_backend,cpu_cycles},{crypto_spec,inst_spec},"
				"{l1i_tlb_refill,inst_retired},{ll_cache_rd,ll_cache_miss_rd},"
				"{inst_retired,cpu_cycles},{l2d_cache_refill,l2d_cache},"
				"{br_indirect_spec,inst_spec,br_immed_spec},{l2d_tlb_refill,l2d_tlb},"
				"{inst_retired,itlb_walk},{dtlb_walk,inst_retired},"
				"{l1i_cache,l1i_cache_refill},{l1d_cache_refill,l1d_cache},"
				"{br_mis_pred_retired,br_retired},{inst_retired,l2d_cache_refill},"
				"{vfp_spec,inst_spec},{stall_frontend,cpu_cycles},"
				"{inst_spec,ase_spec},{ll_cache_rd,ll_cache_miss_rd},"
				"{inst_retired,l1d_cache_refill},{l1d_tlb,dtlb_walk},"
				"{inst_retired,ll_cache_miss_rd},{l1i_cache_refill,inst_retired},"
				"{l1d_tlb_refill,l1d_tlb}", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);

			Assert::AreEqual<size_t>(0, ioctl_events[EVT_CORE].size() % pmu_cfg.gpc_nums[EVT_CORE]);
			Assert::AreEqual<size_t>(66, ioctl_events[EVT_CORE].size());
		}
	};

	TEST_CLASS(wperftest_padding_low_gpc_num_count)
	{
	public:
		//
		// Test for only 1 GPC available, we will assume here all tests are aware of the limitation
		// and will not try to e.g. create event group larger than number of free GPCs.
		//

		TEST_METHOD(test_padding_low_gpc_num_1_no_group)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };
			pmu_cfg.gpc_nums[EVT_CORE] = 1;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"r40,r41,r42,r43,r44,r45,r46", note, pmu_cfg);
			Assert::AreEqual<size_t>(7, events.ungrouped[EVT_CORE].size());

			set_event_padding(ioctl_events, pmu_cfg, events);
			Assert::AreEqual<size_t>(7, ioctl_events[EVT_CORE].size());
		}

		//
		// Test for only 2 GPCs available.
		// We can have groups of two events now.
		//
		TEST_METHOD(test_padding_low_gpc_num_2_with_1_group)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };
			pmu_cfg.gpc_nums[EVT_CORE] = 2;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"r40,{r41,r42},r43,r44,r45,r46", note, pmu_cfg);

			// Check event (in grooups) types
			Assert::AreEqual<size_t>(3, events.groups[EVT_CORE].size());		//	{r41,r42}	-> placeholder,r41,r42
			Assert::AreEqual<uint16_t>(2, events.groups[EVT_CORE][0].index);		// placeholder, 2 means two events in the group (two events after this placeholder)
			Assert::AreEqual<uint16_t>(0x41, events.groups[EVT_CORE][1].index);	// {r41,___}
			Assert::AreEqual<uint16_t>(0x42, events.groups[EVT_CORE][2].index);	// {___,r42}

			// Check event (without grooups) types
			Assert::AreEqual<size_t>(5, events.ungrouped[EVT_CORE].size());		// 5 events (no group) + 2 events in one group
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][0].type);	// r41
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][1].type);	// r42
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][2].type);
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][3].type);
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][4].type);

			// Check event indexes, all events from r40 to r46 should be there
			std::vector<uint16_t> events_no_group = {0x40, 0x43, 0x44, 0x45, 0x46};
			for (auto index : events_no_group)
			{
				auto is_index = [index](struct evt_noted& n) { return n.index == index; };

				auto index_present = std::find_if(events.ungrouped[EVT_CORE].begin(), events.ungrouped[EVT_CORE].end(),
					is_index);

				Assert::IsTrue(index_present != events.ungrouped[EVT_CORE].end());
			}

			set_event_padding(ioctl_events, pmu_cfg, events);

			// `ioctl_events` stores events (evets + groupped events)
			// in flat representation with padding provided already.
			Assert::AreEqual<size_t>(0, ioctl_events[EVT_CORE].size() % pmu_cfg.gpc_nums[EVT_CORE]);	// must be a multiple of GPC_num
			Assert::AreEqual<size_t>(8, ioctl_events[EVT_CORE].size());		// 8 elements because we have N * GPC_num slots to fill

			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][0].type);
			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][1].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][2].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][3].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][4].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][5].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][6].type);
			Assert::AreEqual(EVT_PADDING, ioctl_events[EVT_CORE][7].type);

			Assert::AreEqual<uint16_t>(0x41, ioctl_events[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x42, ioctl_events[EVT_CORE][1].index);
			Assert::AreEqual<uint16_t>(0x40, ioctl_events[EVT_CORE][2].index);
			Assert::AreEqual<uint16_t>(0x43, ioctl_events[EVT_CORE][3].index);
			Assert::AreEqual<uint16_t>(0x44, ioctl_events[EVT_CORE][4].index);
			Assert::AreEqual<uint16_t>(0x45, ioctl_events[EVT_CORE][5].index);
			Assert::AreEqual<uint16_t>(0x46, ioctl_events[EVT_CORE][6].index);
			Assert::AreEqual<uint16_t>(0x1b, ioctl_events[EVT_CORE][7].index);
		}

		//
		// Test for only 3 GPCs available.
		// We can have groups of three events now.
		//
		TEST_METHOD(test_padding_low_gpc_num_3_with_1_group)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 3;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"r40,r41,r42,r43,{r44,r45,r46}", note, pmu_cfg);

			//
			// Groups (only events grouped)
			//
			Assert::AreEqual<size_t>(4, events.groups[EVT_CORE].size());		//	{r41,r42}	-> placeholder,r41,r42

			Assert::AreEqual<uint16_t>(3, events.groups[EVT_CORE][0].index);		// placeholder, 3 means three events in the group (3 events after this placeholder)
			Assert::AreEqual<uint16_t>(0x44, events.groups[EVT_CORE][1].index);	// {r44,___,___}
			Assert::AreEqual<uint16_t>(0x45, events.groups[EVT_CORE][2].index);	// {___,r45,___}
			Assert::AreEqual<uint16_t>(0x46, events.groups[EVT_CORE][3].index);	// {___,___,r46}

			// We have group header and events in the group (3)
			Assert::AreEqual(EVT_HDR, events.groups[EVT_CORE][0].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][1].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][2].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][3].type);

			//
			// Events without group
			//
			Assert::AreEqual<size_t>(4, events.ungrouped[EVT_CORE].size());		// 4 events (no group)

			// Check events indexes
			Assert::AreEqual<uint16_t>(0x40, events.ungrouped[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x41, events.ungrouped[EVT_CORE][1].index);
			Assert::AreEqual<uint16_t>(0x42, events.ungrouped[EVT_CORE][2].index);
			Assert::AreEqual<uint16_t>(0x43, events.ungrouped[EVT_CORE][3].index);

			// Check event (without grooups) types
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][0].type);		// r40
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][1].type);		// r41
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][2].type);		// r42
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][3].type);		// r43

			set_event_padding(ioctl_events, pmu_cfg, events);

			// `ioctl_events` stores events (evets + groupped events)
			// in flat representation with padding provided already.
			Assert::AreEqual<size_t>(0, ioctl_events[EVT_CORE].size() % pmu_cfg.gpc_nums[EVT_CORE]);	// must be a multiple of of GPC_num
			Assert::AreEqual<size_t>(9, ioctl_events[EVT_CORE].size());		// 9 elements because we have N * GPC_num slots to fill

			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][0].type);
			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][1].type);
			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][2].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][3].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][4].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][5].type);
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][6].type);
			Assert::AreEqual(EVT_PADDING, ioctl_events[EVT_CORE][7].type);
			Assert::AreEqual(EVT_PADDING, ioctl_events[EVT_CORE][8].type);

			Assert::AreEqual<uint16_t>(0x44, ioctl_events[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x45, ioctl_events[EVT_CORE][1].index);
			Assert::AreEqual<uint16_t>(0x46, ioctl_events[EVT_CORE][2].index);
			Assert::AreEqual<uint16_t>(0x40, ioctl_events[EVT_CORE][3].index);
			Assert::AreEqual<uint16_t>(0x41, ioctl_events[EVT_CORE][4].index);
			Assert::AreEqual<uint16_t>(0x42, ioctl_events[EVT_CORE][5].index);
			Assert::AreEqual<uint16_t>(0x43, ioctl_events[EVT_CORE][6].index);
			Assert::AreEqual<uint16_t>(0x1b, ioctl_events[EVT_CORE][7].index);
			Assert::AreEqual<uint16_t>(0x1b, ioctl_events[EVT_CORE][8].index);
		}

		//
		// Test for only 3 GPCs available.
		// We can have groups of three events now.
		//
		TEST_METHOD(test_padding_low_gpc_num_3_with_2_groups)
		{
			std::map<enum evt_class, std::vector<struct evt_noted>> ioctl_events;
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 3;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"r40,r41,{r42,r43},{r44,r45,r46}", note, pmu_cfg);

			//
			// Groups (only events grouped)
			//
			Assert::AreEqual<size_t>(7, events.groups[EVT_CORE].size());

			Assert::AreEqual<uint16_t>(2, events.groups[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x42, events.groups[EVT_CORE][1].index);
			Assert::AreEqual<uint16_t>(0x43, events.groups[EVT_CORE][2].index);
			Assert::AreEqual<uint16_t>(3, events.groups[EVT_CORE][3].index);
			Assert::AreEqual<uint16_t>(0x44, events.groups[EVT_CORE][4].index);
			Assert::AreEqual<uint16_t>(0x45, events.groups[EVT_CORE][5].index);
			Assert::AreEqual<uint16_t>(0x46, events.groups[EVT_CORE][6].index);

			// We have group header and events in the group (3)
			Assert::AreEqual(EVT_HDR, events.groups[EVT_CORE][0].type);		// {r42,r43}
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][1].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][2].type);
			Assert::AreEqual(EVT_HDR, events.groups[EVT_CORE][3].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][4].type);	// {r44,r45,r46}
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][5].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][6].type);

			//
			// Events without group
			//
			Assert::AreEqual<size_t>(2, events.ungrouped[EVT_CORE].size());		// 4 events (no group)

			// Check events indexes
			Assert::AreEqual<uint16_t>(0x40, events.ungrouped[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x41, events.ungrouped[EVT_CORE][1].index);

			// Check event (without grooups) types
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][0].type);		// r40
			Assert::AreEqual(EVT_NORMAL, events.ungrouped[EVT_CORE][1].type);		// r41

			set_event_padding(ioctl_events, pmu_cfg, events);

			// `ioctl_events` stores events (evets + groupped events)
			// in flat representation with padding provided already.
			Assert::AreEqual<size_t>(0, ioctl_events[EVT_CORE].size() % pmu_cfg.gpc_nums[EVT_CORE]);	// must be a multiple of of GPC_num
			Assert::AreEqual<size_t>(9, ioctl_events[EVT_CORE].size());		// 9 elements because we have N * GPC_num slots to fill

			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][0].type);	// g0
			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][1].type);	// g0
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][2].type);
			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][3].type);	// g1
			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][4].type);	// g1
			Assert::AreEqual(EVT_GROUPED, ioctl_events[EVT_CORE][5].type);	// g1
			Assert::AreEqual(EVT_NORMAL, ioctl_events[EVT_CORE][6].type);
			Assert::AreEqual(EVT_PADDING, ioctl_events[EVT_CORE][7].type);
			Assert::AreEqual(EVT_PADDING, ioctl_events[EVT_CORE][8].type);

			Assert::AreEqual<uint16_t>(0x42, ioctl_events[EVT_CORE][0].index);	// g0
			Assert::AreEqual<uint16_t>(0x43, ioctl_events[EVT_CORE][1].index);	// g0
			Assert::AreEqual<uint16_t>(0x40, ioctl_events[EVT_CORE][2].index);
			Assert::AreEqual<uint16_t>(0x44, ioctl_events[EVT_CORE][3].index);	// g1
			Assert::AreEqual<uint16_t>(0x45, ioctl_events[EVT_CORE][4].index);	// g1
			Assert::AreEqual<uint16_t>(0x46, ioctl_events[EVT_CORE][5].index);	// g1
			Assert::AreEqual<uint16_t>(0x41, ioctl_events[EVT_CORE][6].index);
			Assert::AreEqual<uint16_t>(0x1b, ioctl_events[EVT_CORE][7].index);
			Assert::AreEqual<uint16_t>(0x1b, ioctl_events[EVT_CORE][8].index);
		}
	};
}

template<> std::wstring CppUnitTestFramework::ToString<evt_type>(const evt_type& q)
{
	switch (q)
	{
	case EVT_NORMAL:         return L"EVT_NORMAL";
	case EVT_GROUPED:        return L"EVT_GROUPED";
	case EVT_METRIC_NORMAL:  return L"EVT_METRIC_NORMAL";
	case EVT_METRIC_GROUPED: return L"EVT_METRIC_GROUPED";
	case EVT_PADDING:        return L"EVT_PADDING";
	case EVT_HDR:            return L"EVT_HDR";
	}
	return L"???";
}
