// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "pch.h"
#include "CppUnitTest.h"
#include <windows.h>
#include <string>
#include "wperf\cmn_device.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

//
// Define specializations of ToString<> for cmn_device tests:
//
namespace Microsoft
{
	namespace VisualStudio
	{
		namespace CppUnitTestFramework
		{
			template<> std::wstring ToString<cmn_mesh_family>(const cmn_mesh_family& t) { return std::to_wstring(static_cast<uint16_t>(t)); }
			template<> std::wstring ToString<cmn_mesh_event_code>(const cmn_mesh_event_code& t) { return std::to_wstring(static_cast<uint16_t>(t)); }
			template<> std::wstring ToString<cmn_mesh_device_ids>(const cmn_mesh_device_ids& t) { return std::to_wstring(static_cast<uint16_t>(t)); }
		}
	}
}

//
// Unit tests
//
namespace wperftest
{
	TEST_CLASS(wperftest_cmn_device)
	{
	public:

		//
		// Enum values
		//
		TEST_METHOD(test_cmn_device_cmn700_family_enum_cmn_mesh_family)
		{
			Assert::AreEqual((cmn_mesh_family)0, cmn_mesh_family::CMN700_FAMILY);
			Assert::AreNotEqual(cmn_mesh_family::CMN_FAMILY_UNKNOWN, cmn_mesh_family::CMN700_FAMILY);
		}

		TEST_METHOD(test_cmn_device_cmn700_family_enum_cmn_mesh_event_code)
		{
			Assert::AreEqual((cmn_mesh_event_code)0x0004, cmn_mesh_event_code::CMN700_FAMILY_DN_PMU_DN_VICI_COUNT);
			Assert::AreEqual((cmn_mesh_event_code)0x0005, cmn_mesh_event_code::CMN700_FAMILY_HNF_PMU_HNF_POCQ_REQS_RECVD_EVENT);
			Assert::AreEqual((cmn_mesh_event_code)0x0025, cmn_mesh_event_code::CMN700_FAMILY_HNI_PMU_HNI_RRT_RD_ALLOC);
			Assert::AreEqual((cmn_mesh_event_code)0x0007, cmn_mesh_event_code::CMN700_FAMILY_HNP_PMU_HNP_P2P_WR_AWVALID_NO_AWREADY);
			Assert::AreEqual((cmn_mesh_event_code)0x001D, cmn_mesh_event_code::CMN700_FAMILY_RNI_PMU_RNI_RRT_BURST_REQ_OCCUPANCY_SLICE1);
			Assert::AreEqual((cmn_mesh_event_code)0x0010, cmn_mesh_event_code::CMN700_FAMILY_RND_PMU_RND_WRTALLOC);
			Assert::AreEqual((cmn_mesh_event_code)0x0017, cmn_mesh_event_code::CMN700_FAMILY_SBSX_PMU_SBSX_RDB_OCC_CNT_OVFL);
			Assert::AreEqual((cmn_mesh_event_code)0x0052, cmn_mesh_event_code::CMN700_FAMILY_CCGRA_PMU_CCGRA_CHI_EXT_DAT_UPLOAD_STALLS);
			Assert::AreEqual((cmn_mesh_event_code)0x0081, cmn_mesh_event_code::CMN700_FAMILY_CCGHA_PMU_CCGHA_PB_RHU_PCIE_DAT_OCC);
			Assert::AreEqual((cmn_mesh_event_code)0x0025, cmn_mesh_event_code::CMN700_FAMILY_CCGLA_PMU_CCLA_TX_CXS_LCRD_BACKPRESSURE);
			Assert::AreEqual((cmn_mesh_event_code)0x0079, cmn_mesh_event_code::CMN700_FAMILY_XP_CMN_XP_TXDAT_DEVICEPORT2_COUNT);
		}

		TEST_METHOD(test_cmn_device_cmn700_family_enum_cmn_mesh_device_ids)
		{
			Assert::AreEqual((cmn_mesh_device_ids)1, cmn_mesh_device_ids::CMN700_FAMILY_DN);
			Assert::AreEqual((cmn_mesh_device_ids)4, cmn_mesh_device_ids::CMN700_FAMILY_HNI);
			Assert::AreEqual((cmn_mesh_device_ids)5, cmn_mesh_device_ids::CMN700_FAMILY_HNF);
			Assert::AreEqual((cmn_mesh_device_ids)6, cmn_mesh_device_ids::CMN700_FAMILY_XP);
			Assert::AreEqual((cmn_mesh_device_ids)7, cmn_mesh_device_ids::CMN700_FAMILY_SBSX);
			Assert::AreEqual((cmn_mesh_device_ids)10, cmn_mesh_device_ids::CMN700_FAMILY_RNI);
			Assert::AreEqual((cmn_mesh_device_ids)13, cmn_mesh_device_ids::CMN700_FAMILY_RND);
			Assert::AreEqual((cmn_mesh_device_ids)17, cmn_mesh_device_ids::CMN700_FAMILY_HNP);
			Assert::AreEqual((cmn_mesh_device_ids)259, cmn_mesh_device_ids::CMN700_FAMILY_CCGRA);
			Assert::AreEqual((cmn_mesh_device_ids)260, cmn_mesh_device_ids::CMN700_FAMILY_CCGHA);
			Assert::AreEqual((cmn_mesh_device_ids)261, cmn_mesh_device_ids::CMN700_FAMILY_CCGLA);
		}

		//
		// Constructor
		//

		TEST_METHOD(test_cmn_device_cmn700_family_ctor)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			Assert::AreEqual(cmn_mesh_family::CMN700_FAMILY, cmn_dev.get_mesh_familty());
		}

		//
		// Preducates
		//

		TEST_METHOD(test_cmn_device_cmn700_family_is_device_id)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_DN));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_HNF));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_HNI));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_HNP));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_RNI));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_RND));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_SBSX));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA));
			Assert::IsTrue(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN700_FAMILY_XP));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_is_device_id_CMN_DEVICE_ID_UNKNOWN)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			Assert::IsFalse(cmn_dev.is_device_id(cmn_mesh_device_ids::CMN_DEVICE_ID_UNKNOWN));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_is_device_event_code)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			// Match device IDs with their event with code == 0x0001 (not all devices have events with code == 0x0001 !!!)
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_DN,	cmn_mesh_event_code::CMN700_FAMILY_DN_PMU_DN_TLBI_COUNT));
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNF, cmn_mesh_event_code::CMN700_FAMILY_HNF_PMU_HNF_CACHE_MISS_EVENT));
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNP, cmn_mesh_event_code::CMN700_FAMILY_HNP_PMU_HNP_P2P_WR_RRT_WR_OCC_CNT_OVFL));
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RNI, cmn_mesh_event_code::CMN700_FAMILY_RNI_PMU_RNI_RDATABEATS_P0));
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RND, cmn_mesh_event_code::CMN700_FAMILY_RND_PMU_RND_RDATABEATS_P0));
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_SBSX, cmn_mesh_event_code::CMN700_FAMILY_SBSX_PMU_SBSX_REQTRKR_RD_ALLOC));
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_XP, cmn_mesh_event_code::CMN700_FAMILY_XP_CMN_XP_TXREQ_EASTPORT_COUNT));

			// Assortment of device IDs and their ecents
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA, cmn_mesh_event_code::CMN700_FAMILY_CCGRA_PMU_CCGRA_REQ_TRK_OCC));
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA, cmn_mesh_event_code::CMN700_FAMILY_CCGHA_PMU_CCGHA_CHI_RSP_UPLOAD_STALLS));
			Assert::IsTrue(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA, cmn_mesh_event_code::CMN700_FAMILY_CCGLA_PMU_CCLA_RX_CXS));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_is_device_event_code_CMN_EVENT_CODE_UNKNOWN)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_DN, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNF, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNI, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNP, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RNI, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RND, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_SBSX, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
			Assert::IsFalse(cmn_dev.is_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_XP, cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN));
		}

		//
		// Extractors
		//
		TEST_METHOD(test_cmn_device_cmn700_family_get_mesh_device_id)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_DN, cmn_dev.get_mesh_device_id(L"DN"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_HNF, cmn_dev.get_mesh_device_id(L"HNF"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_HNI, cmn_dev.get_mesh_device_id(L"HNI"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_HNP, cmn_dev.get_mesh_device_id(L"HNP"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_RNI, cmn_dev.get_mesh_device_id(L"RNI"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_RND, cmn_dev.get_mesh_device_id(L"RND"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_SBSX, cmn_dev.get_mesh_device_id(L"SBSX"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA, cmn_dev.get_mesh_device_id(L"CCGRA"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA, cmn_dev.get_mesh_device_id(L"CCGHA"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA, cmn_dev.get_mesh_device_id(L"CCGLA"));
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_XP, cmn_dev.get_mesh_device_id(L"XP"));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_mesh_device_event_code)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			// Match device IDs with their event with code == 0x0001 (not all devices have events with code == 0x0001 !!!)
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_DN, L"PMU_DN_TLBI_COUNT"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNF, L"PMU_HNF_CACHE_MISS_EVENT"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNP, L"PMU_HNP_P2P_WR_RRT_WR_OCC_CNT_OVFL"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RNI, L"PMU_RNI_RDATABEATS_P0"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RND, L"PMU_RND_RDATABEATS_P0"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_SBSX, L"PMU_SBSX_REQTRKR_RD_ALLOC"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_XP, L"CMN_XP_TXREQ_EASTPORT_COUNT"));

			// Other events for device IDs without events with code == 1
			Assert::AreEqual((cmn_mesh_event_code)0x0020, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNI, L"PMU_HNI_RRT_RD_OCC_CNT_OVFL"));
			Assert::AreEqual((cmn_mesh_event_code)0x0041, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA, L"PMU_CCGRA_REQ_TRK_OCC"));
			Assert::AreEqual((cmn_mesh_event_code)0x0061, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA, L"PMU_CCGHA_RD_DAT_BYPASS"));
			Assert::AreEqual((cmn_mesh_event_code)0x0021, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA, L"PMU_CCLA_RX_CXS"));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_mesh_device_event_code_short_name)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			// Match device IDs with their event with code == 0x0001 (not all devices have events with code == 0x0001 !!!)
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_DN, L"DN_TLBI_COUNT"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNF, L"HNF_CACHE_MISS"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNP, L"HNP_P2P_WR_RRT_WR_OCC_CNT_OVFL"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RNI, L"RNI_RDATABEATS_P0"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RND, L"RND_RDATABEATS_P0"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_SBSX, L"SBSX_REQTRKR_RD_ALLOC"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_XP, L"CMN_XP_TXREQ_EASTPORT_COUNT"));

			// Other events for device IDs without events with code == 1
			Assert::AreEqual((cmn_mesh_event_code)0x0020, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNI, L"HNI_RRT_RD_OCC_CNT_OVFL"));
			Assert::AreEqual((cmn_mesh_event_code)0x0041, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA, L"CCGRA_REQ_TRK_OCC"));
			Assert::AreEqual((cmn_mesh_event_code)0x0061, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA, L"CCGHA_RD_DAT_BYPASS"));
			Assert::AreEqual((cmn_mesh_event_code)0x0021, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA, L"CCLA_RX_CXS"));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_mesh_device_event_code_lowercase)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			// Match device IDs with their event with code == 0x0001 (not all devices have events with code == 0x0001 !!!)
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_DN, L"pmu_dn_tlbi_count"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNF, L"pmu_hnf_cache_miss_event"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNP, L"pmu_hnp_p2p_wr_rrt_wr_occ_cnt_ovfl"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RNI, L"pmu_rni_rdatabeats_p0"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RND, L"pmu_rnd_rdatabeats_p0"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_SBSX, L"pmu_sbsx_reqtrkr_rd_alloc"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_XP, L"cmn_xp_txreq_eastport_count"));

			// Other events for device IDs without events with code == 1
			Assert::AreEqual((cmn_mesh_event_code)0x0020, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNI, L"pmu_hni_rrt_rd_occ_cnt_ovfl"));
			Assert::AreEqual((cmn_mesh_event_code)0x0041, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA, L"pmu_ccgra_req_trk_occ"));
			Assert::AreEqual((cmn_mesh_event_code)0x0061, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA, L"pmu_ccgha_rd_dat_bypass"));
			Assert::AreEqual((cmn_mesh_event_code)0x0021, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA, L"pmu_ccla_rx_cxs"));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_mesh_device_event_code_short_name_lowercase)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			// Match device IDs with their event with code == 0x0001 (not all devices have events with code == 0x0001 !!!)
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_DN, L"dn_tlbi_count"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNF, L"hnf_cache_miss"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNP, L"hnp_p2p_wr_rrt_wr_occ_cnt_ovfl"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RNI, L"rni_rdatabeats_p0"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_RND, L"rnd_rdatabeats_p0"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_SBSX, L"sbsx_reqtrkr_rd_alloc"));
			Assert::AreEqual((cmn_mesh_event_code)0x0001, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_XP, L"cmn_xp_txreq_eastport_count"));

			// Other events for device IDs without events with code == 1
			Assert::AreEqual((cmn_mesh_event_code)0x0020, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_HNI, L"hni_rrt_rd_occ_cnt_ovfl"));
			Assert::AreEqual((cmn_mesh_event_code)0x0041, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA, L"ccgra_req_trk_occ"));
			Assert::AreEqual((cmn_mesh_event_code)0x0061, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA, L"ccgha_rd_dat_bypass"));
			Assert::AreEqual((cmn_mesh_event_code)0x0021, cmn_dev.get_mesh_device_event_code(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA, L"ccla_rx_cxs"));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_device_description_ok)
		{
			cmn_device_description device_desc = {};
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_DN, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_HNF, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_HNP, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_RNI, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_RND, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_SBSX, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_XP, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_HNI, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_CCGRA, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_CCGHA, device_desc));
			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_CCGLA, device_desc));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_device_description_XP)
		{
			cmn_device_description device_desc = {};
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_XP, device_desc));
			Assert::AreEqual(cmn_mesh_family::CMN700_FAMILY, device_desc.mesh_family);
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_XP, device_desc.index);
			Assert::AreEqual<std::wstring>(L"XP", device_desc.name);
			Assert::IsTrue(device_desc.desc.size());
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_device_description_HNF)
		{
			cmn_device_description device_desc = {};
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);

			Assert::IsTrue(cmn_dev.get_device_description(cmn_mesh_device_ids::CMN700_FAMILY_HNF, device_desc));
			Assert::AreEqual(cmn_mesh_family::CMN700_FAMILY, device_desc.mesh_family);
			Assert::AreEqual(cmn_mesh_device_ids::CMN700_FAMILY_HNF, device_desc.index);
			Assert::AreEqual<std::wstring>(L"HNF", device_desc.name);
			Assert::IsTrue(device_desc.desc.size());
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_cmn_version_name_1)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN700_FAMILY);
			Assert::AreEqual<std::wstring>(L"CMN700_FAMILY", cmn_dev.get_cmn_version_name(cmn_mesh_family::CMN700_FAMILY));
			Assert::AreEqual<std::wstring>(L"CMN_FAMILY_UNKNOWN", cmn_dev.get_cmn_version_name(cmn_mesh_family::CMN_FAMILY_UNKNOWN));
		}

		TEST_METHOD(test_cmn_device_cmn700_family_get_cmn_version_name_2)
		{
			cmn_device cmn_dev(cmn_mesh_family::CMN_FAMILY_UNKNOWN);
			Assert::AreEqual<std::wstring>(L"CMN700_FAMILY", cmn_dev.get_cmn_version_name(cmn_mesh_family::CMN700_FAMILY));
			Assert::AreEqual<std::wstring>(L"CMN_FAMILY_UNKNOWN", cmn_dev.get_cmn_version_name(cmn_mesh_family::CMN_FAMILY_UNKNOWN));
		}

		//
		// Manipulators
		//

		TEST_METHOD(test_cmn_device_set_cmn_event_code_short_cmn_1)
		{
			Assert::AreEqual<std::wstring>(L"", cmn_device::set_cmn_event_name_short(L""));
			Assert::AreEqual<std::wstring>(L"EVENT", cmn_device::set_cmn_event_name_short(L"PMU_EVENT"));
			Assert::AreEqual<std::wstring>(L"", cmn_device::set_cmn_event_name_short(L"PMU__EVENT"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"PMU_x_EVENT"));
			Assert::AreEqual<std::wstring>(L"PMUxEVENT", cmn_device::set_cmn_event_name_short(L"PMUxEVENT"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"x_EVENT"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"PMU_x"));
		}

		TEST_METHOD(test_cmn_device_set_cmn_event_code_short_cmn_2)
		{
			Assert::AreEqual<std::wstring>(L"", cmn_device::set_cmn_event_name_short(L""));
			Assert::AreEqual<std::wstring>(L"", cmn_device::set_cmn_event_name_short(L"pMu_"));
			Assert::AreEqual<std::wstring>(L"", cmn_device::set_cmn_event_name_short(L"_EvenT"));
			Assert::AreEqual<std::wstring>(L"EVENT", cmn_device::set_cmn_event_name_short(L"pmu_EVENT"));
			Assert::AreEqual<std::wstring>(L"event", cmn_device::set_cmn_event_name_short(L"PMU_event"));
			Assert::AreEqual<std::wstring>(L"", cmn_device::set_cmn_event_name_short(L"PMU__EVENT"));
			Assert::AreEqual<std::wstring>(L"", cmn_device::set_cmn_event_name_short(L"PMu__eVENT"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"PMU_x_EVENT"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"PMu_x_eVENT"));
			Assert::AreEqual<std::wstring>(L"X", cmn_device::set_cmn_event_name_short(L"PMu_X_eVENT"));
			Assert::AreEqual<std::wstring>(L"PMUxEVENT", cmn_device::set_cmn_event_name_short(L"PMUxEVENT"));
			Assert::AreEqual<std::wstring>(L"pmuxevent", cmn_device::set_cmn_event_name_short(L"pmuxevent"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"x_EVENT"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"x_EvenT"));
			Assert::AreEqual<std::wstring>(L"X", cmn_device::set_cmn_event_name_short(L"X_EvenT"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"PMU_x"));
			Assert::AreEqual<std::wstring>(L"x", cmn_device::set_cmn_event_name_short(L"pmu_x"));
			Assert::AreEqual<std::wstring>(L"X", cmn_device::set_cmn_event_name_short(L"pmu_X"));
		}
		TEST_METHOD(test_cmn_device_set_cmn_event_code_short_cmn_event_names_1)
		{
			Assert::AreEqual<std::wstring>(L"DN_TLBI_COUNT", cmn_device::set_cmn_event_name_short(L"PMU_DN_TLBI_COUNT"));
			Assert::AreEqual<std::wstring>(L"HNF_CACHE_MISS", cmn_device::set_cmn_event_name_short(L"PMU_HNF_CACHE_MISS_EVENT"));
			Assert::AreEqual<std::wstring>(L"HNF_SNP_FWDED", cmn_device::set_cmn_event_name_short(L"PMU_HNF_SNP_FWDED_EVENT"));
			Assert::AreEqual<std::wstring>(L"HNI_NONPCIE_SERIALIZATION", cmn_device::set_cmn_event_name_short(L"PMU_HNI_NONPCIE_SERIALIZATION"));
			Assert::AreEqual<std::wstring>(L"RNI_RRTALLOC", cmn_device::set_cmn_event_name_short(L"PMU_RNI_RRTALLOC"));
			Assert::AreEqual<std::wstring>(L"RND_ATOMIC_OCCUPANCY", cmn_device::set_cmn_event_name_short(L"PMU_RND_ATOMIC_OCCUPANCY"));
			Assert::AreEqual<std::wstring>(L"SBSX_CMO_REQ_TRKR_OCC_CNT_OVFL", cmn_device::set_cmn_event_name_short(L"PMU_SBSX_CMO_REQ_TRKR_OCC_CNT_OVFL"));
			Assert::AreEqual<std::wstring>(L"CMN_XP_TXSNP2_DEVICEPORT3_PARTIAL_DAT_FLIT_COUNT", cmn_device::set_cmn_event_name_short(L"CMN_XP_TXSNP2_DEVICEPORT3_PARTIAL_DAT_FLIT_COUNT"));
		}

		TEST_METHOD(test_cmn_device_set_cmn_event_code_short_cmn_event_names_2)
		{
			Assert::AreEqual<std::wstring>(L"dn_tlbi_count", cmn_device::set_cmn_event_name_short(L"pmu_dn_tlbi_count"));
			Assert::AreEqual<std::wstring>(L"hnf_cache_miss", cmn_device::set_cmn_event_name_short(L"pmu_hnf_cache_miss_event"));
			Assert::AreEqual<std::wstring>(L"hnf_snp_fwded", cmn_device::set_cmn_event_name_short(L"pmu_hnf_snp_fwded_event"));
			Assert::AreEqual<std::wstring>(L"hni_nonpcie_serialization", cmn_device::set_cmn_event_name_short(L"pmu_hni_nonpcie_serialization"));
			Assert::AreEqual<std::wstring>(L"rni_rrtalloc", cmn_device::set_cmn_event_name_short(L"pmu_rni_rrtalloc"));
			Assert::AreEqual<std::wstring>(L"rnd_atomic_occupancy", cmn_device::set_cmn_event_name_short(L"pmu_rnd_atomic_occupancy"));
			Assert::AreEqual<std::wstring>(L"sbsx_cmo_req_trkr_occ_cnt_ovfl", cmn_device::set_cmn_event_name_short(L"pmu_sbsx_cmo_req_trkr_occ_cnt_ovfl"));
			Assert::AreEqual<std::wstring>(L"cmn_xp_txsnp2_deviceport3_partial_dat_flit_count", cmn_device::set_cmn_event_name_short(L"cmn_xp_txsnp2_deviceport3_partial_dat_flit_count"));
		}
	};
}
