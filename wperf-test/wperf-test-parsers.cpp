// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <algorithm>
#include <functional>
#include <sstream>
#include "pch.h"
#include "CppUnitTest.h"

#include <windows.h>
#include "wperf/events.h"
#include "wperf/parsers.h"
#include "wperf/exception.h"

using namespace std::literals;
using namespace Microsoft::VisualStudio;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace wperftest
{
	TEST_CLASS(wperftest_parsers_events_feat_cmn)
	{
	public:

		TEST_METHOD(parse_events_str_for_feat_cmn_incorrect_input)
		{
			std::map<std::wstring, uint64_t> flags;
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L"arm_cmn_0/"), flags));
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L"arm_cmn_0"), flags));
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L"arm_cmn_/"), flags));
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L"arm_cmn"), flags));
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L"arm_"), flags));
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L"_"), flags));
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L" "), flags));
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L""), flags));
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_incorrect_input_spe)
		{
			std::map<std::wstring, uint64_t> flags;
			Assert::IsFalse(parse_events_str_for_feat_cmn(std::wstring(L"arm_spe_0//"), flags));
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_1)
		{
			std::wstring events_str = L"arm_cmn_0/hnf_cache_miss/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(1, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"hnf_cache_miss"));
			Assert::AreEqual<uint64_t>(1, flags[L"hnf_cache_miss"]);	// default value for filters without value
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_2)
		{
			std::wstring events_str = L"arm_cmn_0/type=0x5,eventid=0x1/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(2, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"type"));
			Assert::AreEqual<uint64_t>(0x5, flags[L"type"]);

			Assert::AreEqual<size_t>(1, flags.count(L"eventid"));
			Assert::AreEqual<uint64_t>(0x1, flags[L"eventid"]);
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_3)
		{
			std::wstring events_str = L"arm_cmn_0/config=0x10005/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(1, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"config"));
			Assert::AreEqual<uint64_t>(0x10005, flags[L"config"]);
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_4)
		{
			std::wstring events_str = L"arm_cmn_0/hnf_cache_miss,bynodeid,nodeid=0x24/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(3, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"hnf_cache_miss"));
			Assert::AreEqual<uint64_t>(1, flags[L"hnf_cache_miss"]);

			Assert::AreEqual<size_t>(1, flags.count(L"bynodeid"));
			Assert::AreEqual<uint64_t>(1, flags[L"bynodeid"]);

			Assert::AreEqual<size_t>(1, flags.count(L"nodeid"));
			Assert::AreEqual<uint64_t>(0x24, flags[L"nodeid"]);
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_5)
		{
			std::wstring events_str = L"arm_cmn_0/watchpoint_up,nodeid=0x20,wp_dev_sel=0,wp_chn_sel=0,wp_grp=1,wp_val=0x0,wp_mask=0xFFFFFFFFFFFFFC0F,wp_combine=12/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(8, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"watchpoint_up"));
			Assert::AreEqual<uint64_t>(1, flags[L"watchpoint_up"]);

			Assert::AreEqual<size_t>(1, flags.count(L"nodeid"));
			Assert::AreEqual<uint64_t>(0x20, flags[L"nodeid"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_dev_sel"));
			Assert::AreEqual<uint64_t>(0, flags[L"wp_dev_sel"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_chn_sel"));
			Assert::AreEqual<uint64_t>(0, flags[L"wp_chn_sel"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_grp"));
			Assert::AreEqual<uint64_t>(1, flags[L"wp_grp"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_val"));
			Assert::AreEqual<uint64_t>(0, flags[L"wp_val"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_mask"));
			Assert::AreEqual<uint64_t>(0xFFFFFFFFFFFFFC0F, flags[L"wp_mask"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_combine"));
			Assert::AreEqual<uint64_t>(12, flags[L"wp_combine"]);
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_6)
		{
			std::wstring events_str = L"arm_cmn_0/watchpoint_up,nodeid=0x20,wp_dev_sel=0,wp_chn_sel=0,wp_grp=0,wp_val=0x300000000,wp_mask=0xFFFFFFF01FFFFFFF,wp_combine=12/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(8, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"watchpoint_up"));
			Assert::AreEqual<uint64_t>(1, flags[L"watchpoint_up"]);

			Assert::AreEqual<size_t>(1, flags.count(L"nodeid"));
			Assert::AreEqual<uint64_t>(0x20, flags[L"nodeid"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_dev_sel"));
			Assert::AreEqual<uint64_t>(0, flags[L"wp_dev_sel"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_chn_sel"));
			Assert::AreEqual<uint64_t>(0, flags[L"wp_chn_sel"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_grp"));
			Assert::AreEqual<uint64_t>(0, flags[L"wp_grp"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_val"));
			Assert::AreEqual<uint64_t>(0x300000000, flags[L"wp_val"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_mask"));
			Assert::AreEqual<uint64_t>(0xFFFFFFF01FFFFFFF, flags[L"wp_mask"]);

			Assert::AreEqual<size_t>(1, flags.count(L"wp_combine"));
			Assert::AreEqual<uint64_t>(12, flags[L"wp_combine"]);
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_7)
		{
			std::wstring events_str = L"arm_cmn_0/type=0x5,eventid=0x1,bynodeid,nodeid=0x24/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(4, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"type"));
			Assert::AreEqual<uint64_t>(0x5, flags[L"type"]);

			Assert::AreEqual<size_t>(1, flags.count(L"eventid"));
			Assert::AreEqual<uint64_t>(0x1, flags[L"eventid"]);

			Assert::AreEqual<size_t>(1, flags.count(L"bynodeid"));
			Assert::AreEqual<uint64_t>(1, flags[L"bynodeid"]);

			Assert::AreEqual<size_t>(1, flags.count(L"nodeid"));
			Assert::AreEqual<uint64_t>(0x24, flags[L"nodeid"]);
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_8)
		{
			std::wstring events_str = L"arm_cmn_0/filter_one,filter_two,filter_three/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(3, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"filter_one"));
			Assert::AreEqual<uint64_t>(1, flags[L"filter_one"]);

			Assert::AreEqual<size_t>(1, flags.count(L"filter_two"));
			Assert::AreEqual<uint64_t>(1, flags[L"filter_two"]);

			Assert::AreEqual<size_t>(1, flags.count(L"filter_three"));
			Assert::AreEqual<uint64_t>(1, flags[L"filter_three"]);
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_9)
		{
			std::wstring events_str = L"arm_cmn_0/filter_three,filter_one/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(2, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"filter_one"));
			Assert::AreEqual<uint64_t>(1, flags[L"filter_one"]);

			Assert::AreEqual<size_t>(1, flags.count(L"filter_three"));
			Assert::AreEqual<uint64_t>(1, flags[L"filter_three"]);
		}

		TEST_METHOD(parse_events_str_for_feat_cmn_example_filters_10)
		{
			std::wstring events_str = L"arm_cmn_0/filter_two/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_cmn(events_str, flags));
			Assert::AreEqual<size_t>(1, flags.size());

			Assert::AreEqual<size_t>(1, flags.count(L"filter_two"));
			Assert::AreEqual<uint64_t>(1, flags[L"filter_two"]);
		}
	};

	/****************************************************************************/

	TEST_CLASS(wperftest_parsers_events_feat_spe)
	{
	public:

		TEST_METHOD(parse_events_str_for_feat_spe_incorrect_input)
		{
			std::map<std::wstring, uint64_t> flags;
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L"arm_spe_0/"), flags));
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L"arm_spe_0"), flags));
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L"arm_spe_/"), flags));
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L"arm_sp"), flags));
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L"arm_"), flags));
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L"_"), flags));
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L" "), flags));
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L""), flags));
		}

		TEST_METHOD(parse_events_str_for_feat_spe_incorrect_input_cmn)
		{
			std::map<std::wstring, uint64_t> flags;
			Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L"arm_cmn_0//"), flags));
		}

		TEST_METHOD(parse_events_str_for_feat_spe_incorrect_input_throw)
		{
			auto process = [](std::wstring input)
			{
				std::map<std::wstring, uint64_t> flags;
				Assert::IsFalse(parse_events_str_for_feat_spe(std::wstring(L"arm_spe_0/" + input + L"/"), flags));
			};

			Assert::ExpectException<fatal_exception>([=] {process(L"load_filter=-");});
			Assert::ExpectException<fatal_exception>([=] {process(L"branch_filter=-0x1");});
			Assert::ExpectException<fatal_exception>([=] {process(L"branch_filter=-2");});
			Assert::ExpectException<fatal_exception>([=] {process(L"branch_filter=-2");});
			Assert::ExpectException<fatal_exception>([=] {process(L"=1");});
			Assert::ExpectException<fatal_exception>([=] {process(L"=");});
			Assert::ExpectException<fatal_exception>([=] {process(L"jitter=x");});
			Assert::ExpectException<fatal_exception>([=] {process(L"jitter=1,,");});
			Assert::ExpectException<fatal_exception>([=] {process(L",jitter=1");});
			Assert::ExpectException<fatal_exception>([=] {process(L"ld=1jitter=1,period=5000");});
			Assert::ExpectException<fatal_exception>([=] {process(L"ld=1,jitter=1,period==5000");});
		}

		TEST_METHOD(parse_events_str_for_feat_spe_branch_filter_1)
		{
			std::wstring events_str = L"arm_spe_0/branch_filter=1/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(1, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"branch_filter"));
			Assert::AreEqual<uint64_t>(1, flags[L"branch_filter"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_branch_filter_0)
		{
			std::wstring events_str = L"arm_spe_0/branch_filter=0/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(1, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"branch_filter"));
			Assert::AreEqual<uint64_t>(0, flags[L"branch_filter"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_ts_enable_1)
		{
			std::wstring events_str = L"arm_spe_0/ts_enable=1/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(1, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"ts_enable"));
			Assert::AreEqual<uint64_t>(1, flags[L"ts_enable"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_ts_enable_0)
		{
			std::wstring events_str = L"arm_spe_0/ts_enable=0/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(1, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"ts_enable"));
			Assert::AreEqual<uint64_t>(0, flags[L"ts_enable"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_2_filters_10)
		{
			std::wstring events_str = L"arm_spe_0/branch_filter=1,jitter=0/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(2, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"branch_filter"));
			Assert::AreEqual<size_t>(1, flags.count(L"jitter"));
			Assert::AreEqual<uint64_t>(1, flags[L"branch_filter"]);
			Assert::AreEqual<uint64_t>(0, flags[L"jitter"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_2_filters_01)
		{
			std::wstring events_str = L"arm_spe_0/branch_filter=0,jitter=1/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(2, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"branch_filter"));
			Assert::AreEqual<size_t>(1, flags.count(L"jitter"));
			Assert::AreEqual<uint64_t>(0, flags[L"branch_filter"]);
			Assert::AreEqual<uint64_t>(1, flags[L"jitter"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_2_filters_11)
		{
			std::wstring events_str = L"arm_spe_0/branch_filter=1,jitter=1/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(2, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"branch_filter"));
			Assert::AreEqual<size_t>(1, flags.count(L"jitter"));
			Assert::AreEqual<uint64_t>(1, flags[L"branch_filter"]);
			Assert::AreEqual<uint64_t>(1, flags[L"jitter"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_2_filters_00)
		{
			std::wstring events_str = L"arm_spe_0/branch_filter=0,jitter=0/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(2, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"branch_filter"));
			Assert::AreEqual<size_t>(1, flags.count(L"jitter"));
			Assert::AreEqual<uint64_t>(0, flags[L"branch_filter"]);
			Assert::AreEqual<uint64_t>(0, flags[L"jitter"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_2_filters_min_latency)
		{
			std::wstring events_str = L"arm_spe_0/branch_filter=0,min_latency=10/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_spe(events_str, flags));
			Assert::AreEqual<size_t>(2, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"branch_filter"));
			Assert::AreEqual<size_t>(1, flags.count(L"min_latency"));
			Assert::AreEqual<uint64_t>(0, flags[L"branch_filter"]);
			Assert::AreEqual<uint64_t>(10, flags[L"min_latency"]);
		}

		TEST_METHOD(parse_events_str_for_feat_spe_3_filters_00)
		{
			std::wstring events_str = L"arm_spe_0/abc=0,def=10,ghi=0x11f/";
			std::map<std::wstring, uint64_t> flags;

			Assert::IsTrue(parse_events_str_for_feat_spe(events_str, flags));
			Assert::AreEqual<size_t>(3, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"abc"));
			Assert::AreEqual<size_t>(1, flags.count(L"def"));
			Assert::AreEqual<size_t>(1, flags.count(L"ghi"));
			Assert::AreEqual<uint64_t>(0, flags[L"abc"]);
			Assert::AreEqual<uint64_t>(10, flags[L"def"]);
			Assert::AreEqual<uint64_t>(0x11f, flags[L"ghi"]);
		}

		/* Example usage could include:

		PMSFCR_EL1.FT enables filtering by operation type. When enabled PMSFCR_EL1.{ST, LD, B} define the
		collected types:
		* ST enables collection of store sampled operations, including all atomic operations.
		* LD enables collection of load sampled operations, including atomic operations that return a value to 
		  register.
		* B enables collection of branch sampled operations, including direct and indirect branches and exception
		  returns.
		* TS enables timestamping with value of generic timer (PMSCR.TS).
		*/

		TEST_METHOD(parse_events_str_for_feat_spe_2_filters_010)
		{
			std::wstring events_str = L"arm_spe_0/ld=0,st=1,b=0,ts=0/";
			std::map<std::wstring, uint64_t> flags;

			bool ret = parse_events_str_for_feat_spe(events_str, flags);

			Assert::IsTrue(ret);
			Assert::AreEqual<size_t>(4, flags.size());
			Assert::AreEqual<size_t>(1, flags.count(L"ld"));
			Assert::AreEqual<size_t>(1, flags.count(L"st"));
			Assert::AreEqual<size_t>(1, flags.count(L"b"));
			Assert::AreEqual<size_t>(1, flags.count(L"ts"));
			Assert::AreEqual<uint64_t>(0, flags[L"ld"]);
			Assert::AreEqual<uint64_t>(1, flags[L"st"]);
			Assert::AreEqual<uint64_t>(0, flags[L"b"]);
			Assert::AreEqual<uint64_t>(0, flags[L"ts"]);
		}
	};

	/****************************************************************************/

	TEST_CLASS(wperftest_parsers_events_sampling)
	{
	public:
		TEST_METHOD(test_parse_events_str_for_sample_1)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;	// Sorted by index !
			std::map<uint32_t, uint32_t> sampling_inverval;		// [event_idx] => frequency

			parse_events_str_for_sample(L"vfp_spec:10000", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual<size_t>(1, sampling_inverval.size());
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0075));

			Assert::AreEqual(10000u, sampling_inverval[0x0075]);

			Assert::AreEqual<size_t>(1, ioctl_events_sample.size());	// Sorted by index !

			Assert::AreEqual<uint32_t>(0x0075, ioctl_events_sample[0].index);			// vfp_spec
			Assert::AreEqual<uint32_t>(10000, ioctl_events_sample[0].interval);
		}

		TEST_METHOD(test_parse_events_str_for_sample_2)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;	// Sorted by index !
			std::map<uint32_t, uint32_t> sampling_inverval;		// [event_idx] => frequency

			parse_events_str_for_sample(L"vfp_spec:12345,ld_spec:67890", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual<size_t>(2, sampling_inverval.size());
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0075));
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0070));

			Assert::AreEqual(67890u, sampling_inverval[0x0070]);
			Assert::AreEqual(12345u, sampling_inverval[0x0075]);

			Assert::AreEqual<size_t>(2, ioctl_events_sample.size());	// Sorted by index !

			Assert::AreEqual<uint32_t>(0x0070, ioctl_events_sample[0].index);			// ld_spec
			Assert::AreEqual<uint32_t>(67890, ioctl_events_sample[0].interval);

			Assert::AreEqual<uint32_t>(0x0075, ioctl_events_sample[1].index);			// vfp_spec
			Assert::AreEqual<uint32_t>(12345, ioctl_events_sample[1].interval);
		}

		TEST_METHOD(test_parse_events_str_for_sample_3)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;	// Sorted by index !
			std::map<uint32_t, uint32_t> sampling_inverval;		// [event_idx] => frequency

			parse_events_str_for_sample(L"vfp_spec:12345,ld_spec:67890,cpu_cycles:1234321", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual<size_t>(3, sampling_inverval.size());
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0075));
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0070));
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0011));

			Assert::AreEqual(12345u, sampling_inverval[0x0075]);
			Assert::AreEqual(67890u, sampling_inverval[0x0070]);
			Assert::AreEqual(1234321u, sampling_inverval[0x0011]);

			Assert::AreEqual<size_t>(3, ioctl_events_sample.size());	// Sorted by index !

			Assert::AreEqual<uint32_t>(0x0070, ioctl_events_sample[0].index);			// ld_spec
			Assert::AreEqual<uint32_t>(67890, ioctl_events_sample[0].interval);

			Assert::AreEqual<uint32_t>(0x0075, ioctl_events_sample[1].index);			// vfp_spec
			Assert::AreEqual<uint32_t>(12345, ioctl_events_sample[1].interval);

			Assert::AreEqual<uint32_t>(CYCLE_EVT_IDX, ioctl_events_sample[2].index);	// cpu_cycles
			Assert::AreEqual<uint32_t>(1234321, ioctl_events_sample[2].interval);
		}

		TEST_METHOD(test_parse_events_str_for_sample_3_default_interval_ld_spec)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;	// Sorted by index !
			std::map<uint32_t, uint32_t> sampling_inverval;		// [event_idx] => frequency

			parse_events_str_for_sample(L"vfp_spec:12345,ld_spec,cpu_cycles:1234321", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual<size_t>(3, sampling_inverval.size());
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0075));
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0070));
			Assert::AreEqual<size_t>(1, sampling_inverval.count(0x0011));

			Assert::AreEqual(12345u, sampling_inverval[0x0075]);
			Assert::AreEqual(PARSE_INTERVAL_DEFAULT, sampling_inverval[0x0070]);
			Assert::AreEqual(1234321u, sampling_inverval[0x0011]);

			Assert::AreEqual<size_t>(3, ioctl_events_sample.size());	// Sorted by index !

			Assert::AreEqual<uint32_t>(0x0070, ioctl_events_sample[0].index);			// ld_spec
			Assert::AreEqual<uint32_t>(PARSE_INTERVAL_DEFAULT, ioctl_events_sample[0].interval);

			Assert::AreEqual<uint32_t>(0x0075, ioctl_events_sample[1].index);			// vfp_spec
			Assert::AreEqual<uint32_t>(12345, ioctl_events_sample[1].interval);

			Assert::AreEqual<uint32_t>(CYCLE_EVT_IDX, ioctl_events_sample[2].index);	// cpu_cycles
			Assert::AreEqual<uint32_t>(1234321, ioctl_events_sample[2].interval);
		}

		TEST_METHOD(test_parse_events_str_for_sample_interval_ok_max)
		{
			// 0xFFFFFFFF: 4294967295
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_interval;
			parse_events_str_for_sample(L"ld_spec:4294967295", ioctl_events_sample, sampling_interval);

			Assert::AreEqual<size_t>(1, ioctl_events_sample.size());
			Assert::AreEqual<uint32_t>(0x0070, ioctl_events_sample[0].index);
			Assert::AreEqual<uint32_t>(0xFFFFFFFF, ioctl_events_sample[0].interval);
		}

		TEST_METHOD(test_parse_events_str_for_sample_interval_ok_max_hex)
		{
			// prefix 0x or 0X indicates hexadecimal value if base==0
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_interval;
			parse_events_str_for_sample(L"ld_spec:0xFFFFFFFF", ioctl_events_sample, sampling_interval);

			Assert::AreEqual<size_t>(1, ioctl_events_sample.size());
			Assert::AreEqual<uint32_t>(0x0070, ioctl_events_sample[0].index);
			Assert::AreEqual<uint32_t>(0xFFFFFFFF, ioctl_events_sample[0].interval);
		}

		TEST_METHOD(test_parse_events_str_for_sample_interval_invalid_argument)
		{
			// std::invalid_argument
			{
				auto wrapper_sample_interval = [=]() {
					std::vector<struct evt_sample_src> ioctl_events_sample;
					std::map<uint32_t, uint32_t> sampling_interval;
					parse_events_str_for_sample(L"ld_spec:bum!", ioctl_events_sample, sampling_interval);
					};
				Assert::ExpectException<fatal_exception>(wrapper_sample_interval);
			}

			{
				auto wrapper_sample_interval = [=]() {
					std::vector<struct evt_sample_src> ioctl_events_sample;
					std::map<uint32_t, uint32_t> sampling_interval;
					parse_events_str_for_sample(L"ld_spec:number", ioctl_events_sample, sampling_interval);
					};
				Assert::ExpectException<fatal_exception>(wrapper_sample_interval);
			}

			{
				auto wrapper_sample_interval = [=]() {
					std::vector<struct evt_sample_src> ioctl_events_sample;
					std::map<uint32_t, uint32_t> sampling_interval;
					parse_events_str_for_sample(L"ld_spec:_123456", ioctl_events_sample, sampling_interval);
					};
				Assert::ExpectException<fatal_exception>(wrapper_sample_interval);
			}

			// std::out_of_range
			{
				auto wrapper_sample_interval = [=]() {
					std::vector<struct evt_sample_src> ioctl_events_sample;
					std::map<uint32_t, uint32_t> sampling_interval;
					parse_events_str_for_sample(L"ld_spec:10000000000", ioctl_events_sample, sampling_interval);
					};
				Assert::ExpectException<fatal_exception>(wrapper_sample_interval);
			}

			{
				// 0xFFFFFFFF + 1
				auto wrapper_sample_interval = [=]() {
					std::vector<struct evt_sample_src> ioctl_events_sample;
					std::map<uint32_t, uint32_t> sampling_interval;
					parse_events_str_for_sample(L"ld_spec:4294967296", ioctl_events_sample, sampling_interval);
					};
				Assert::ExpectException<fatal_exception>(wrapper_sample_interval);
			}

			{
				auto wrapper_sample_interval = [=]() {
					std::vector<struct evt_sample_src> ioctl_events_sample;
					std::map<uint32_t, uint32_t> sampling_interval;
					parse_events_str_for_sample(L"ld_spec:0x1FFFFFFFF", ioctl_events_sample, sampling_interval);
					};
				Assert::ExpectException<fatal_exception>(wrapper_sample_interval);
			}
		}
	};

	/****************************************************************************/

	TEST_CLASS(wperftest_parsers_raw_events_sampling)
	{
	public:
		TEST_METHOD(test_parse_raw_events_str_for_sample_rf)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_inverval;

			parse_events_str_for_sample(L"rf", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual<size_t>(1, ioctl_events_sample.size());
			Assert::AreEqual<uint32_t>(0x0F, ioctl_events_sample[0].index);
			Assert::AreEqual(ioctl_events_sample[0].interval, PARSE_INTERVAL_DEFAULT);
		}

		TEST_METHOD(test_parse_raw_events_str_for_sample_rf_interval)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_inverval;

			parse_events_str_for_sample(L"rf:12345", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual< size_t>(1, ioctl_events_sample.size());
			Assert::AreEqual<uint32_t>(0x0F, ioctl_events_sample[0].index);
			Assert::AreEqual<uint32_t>(12345, ioctl_events_sample[0].interval);
		}

		TEST_METHOD(test_parse_raw_events_str_for_sample_r1b)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_inverval;

			parse_events_str_for_sample(L"r1b", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual<size_t>(1, ioctl_events_sample.size());
			Assert::AreEqual<uint32_t>(0x1B, ioctl_events_sample[0].index);
			Assert::AreEqual(ioctl_events_sample[0].interval, PARSE_INTERVAL_DEFAULT);
		}

		TEST_METHOD(test_parse_raw_events_str_for_sample_r12_rab)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_inverval;

			parse_events_str_for_sample(L"r12,rab", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual<size_t>(2, ioctl_events_sample.size());
			Assert::AreEqual<uint32_t>(0x12, ioctl_events_sample[0].index);
			Assert::AreEqual(ioctl_events_sample[0].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0xAB, ioctl_events_sample[1].index);
			Assert::AreEqual(ioctl_events_sample[1].interval, PARSE_INTERVAL_DEFAULT);
		}

		TEST_METHOD(test_parse_raw_events_str_for_sample_r12_rab_interval)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_inverval;

			parse_events_str_for_sample(L"r12:12345,rab:67890", ioctl_events_sample, sampling_inverval);

			Assert::AreEqual<size_t>(2, ioctl_events_sample.size());
			Assert::AreEqual<uint32_t>(0x12, ioctl_events_sample[0].index);
			Assert::AreEqual<uint32_t>(12345, ioctl_events_sample[0].interval);

			Assert::AreEqual<uint32_t>(0xAB, ioctl_events_sample[1].index);
			Assert::AreEqual<uint32_t>(67890, ioctl_events_sample[1].interval);
		}

		TEST_METHOD(test_parse_raw_events_str_for_sample_r_7_events)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_interval;

			parse_events_str_for_sample(L"ra,r3c,rdab,rdead,r1a2b,rfedc,r1234", ioctl_events_sample, sampling_interval);

			Assert::AreEqual<size_t>(7, ioctl_events_sample.size());

			Assert::AreEqual<uint32_t>(0xA, ioctl_events_sample[0].index);
			Assert::AreEqual(ioctl_events_sample[0].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0x3C, ioctl_events_sample[1].index);
			Assert::AreEqual(ioctl_events_sample[1].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0xDAB, ioctl_events_sample[2].index);
			Assert::AreEqual(ioctl_events_sample[2].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0x1234, ioctl_events_sample[3].index);
			Assert::AreEqual(ioctl_events_sample[3].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0x1A2B, ioctl_events_sample[4].index);
			Assert::AreEqual(ioctl_events_sample[4].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0xDEAD, ioctl_events_sample[5].index);
			Assert::AreEqual(ioctl_events_sample[5].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0xFEDC, ioctl_events_sample[6].index);
			Assert::AreEqual(ioctl_events_sample[6].interval, PARSE_INTERVAL_DEFAULT);
		}

		TEST_METHOD(test_parse_raw_events_str_for_sample_r_7_events_interval)
		{
			std::vector<struct evt_sample_src> ioctl_events_sample;
			std::map<uint32_t, uint32_t> sampling_interval;

			parse_events_str_for_sample(L"ra,r3c:10000,rdab,rdead:200000,r1a2b,rfedc:3000000,r1234", ioctl_events_sample, sampling_interval);

			Assert::AreEqual<size_t>(7, ioctl_events_sample.size());

			Assert::AreEqual<uint32_t>(0xA, ioctl_events_sample[0].index);
			Assert::AreEqual(ioctl_events_sample[0].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0x3C, ioctl_events_sample[1].index);
			Assert::AreEqual<uint32_t>(10000, ioctl_events_sample[1].interval);

			Assert::AreEqual<uint32_t>(0xDAB, ioctl_events_sample[2].index);
			Assert::AreEqual(ioctl_events_sample[2].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0x1234, ioctl_events_sample[3].index);
			Assert::AreEqual(ioctl_events_sample[3].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0x1A2B, ioctl_events_sample[4].index);
			Assert::AreEqual(ioctl_events_sample[4].interval, PARSE_INTERVAL_DEFAULT);

			Assert::AreEqual<uint32_t>(0xDEAD, ioctl_events_sample[5].index);
			Assert::AreEqual<uint32_t>(200000, ioctl_events_sample[5].interval);

			Assert::AreEqual<uint32_t>(0xFEDC, ioctl_events_sample[6].index);
			Assert::AreEqual<uint32_t>(3000000, ioctl_events_sample[6].interval);
		}

		TEST_METHOD(test_parse_raw_events_str_for_sample_misc)
		{
			{
				std::vector<struct evt_sample_src> ioctl_events_sample;
				std::map<uint32_t, uint32_t> sampling_interval;
				parse_events_str_for_sample(L"rffff", ioctl_events_sample, sampling_interval);
			}

			{
				auto wrapper_extra_large_num = [=]() {
					std::vector<struct evt_sample_src> ioctl_events_sample;
					std::map<uint32_t, uint32_t> sampling_interval;
					parse_events_str_for_sample(L"r10000", ioctl_events_sample, sampling_interval);
				};
				Assert::ExpectException<fatal_exception>(wrapper_extra_large_num);
			}

			{
				auto wrapper_extra_large_num_2 = [=]() {
					std::vector<struct evt_sample_src> ioctl_events_sample;
					std::map<uint32_t, uint32_t> sampling_interval;
					parse_events_str_for_sample(L"r1,r10000", ioctl_events_sample, sampling_interval);
				};
				Assert::ExpectException<fatal_exception>(wrapper_extra_large_num_2);
			}
		}
	};

	/****************************************************************************/

	TEST_CLASS(wperftest_parsers_extra_events)
	{
	public:

		TEST_METHOD(test_parse_events_extra_1_event_OK)
		{
			std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
			parse_events_extra(L"name1:0x1234", extra_core_events);

			Assert::AreEqual<size_t>(1, extra_core_events[EVT_CORE].size());

			Assert::AreEqual(L"name1"s, extra_core_events[EVT_CORE][0].name);
			Assert::AreEqual(EVT_CORE, extra_core_events[EVT_CORE][0].hdr.evt_class);
			Assert::AreEqual<UINT16>(0x1234, extra_core_events[EVT_CORE][0].hdr.num);
		}

		TEST_METHOD(test_parse_events_extra_1_event_max_val_OK)
		{
			std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
			parse_events_extra(L"name1:0xffff", extra_core_events);

			Assert::AreEqual<size_t>(1, extra_core_events[EVT_CORE].size());

			Assert::AreEqual(L"name1"s, extra_core_events[EVT_CORE][0].name);
			Assert::AreEqual(EVT_CORE, extra_core_events[EVT_CORE][0].hdr.evt_class);
			Assert::AreEqual<UINT16>(0xffff, extra_core_events[EVT_CORE][0].hdr.num);
		}

		TEST_METHOD(test_parse_events_extra_2_events_OK)
		{
			std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
			parse_events_extra(L"name1:0x1234,name2:0xabcd", extra_core_events);

			Assert::AreEqual<size_t>(2, extra_core_events[EVT_CORE].size());

			Assert::AreEqual(L"name1"s, extra_core_events[EVT_CORE][0].name);
			Assert::AreEqual(EVT_CORE, extra_core_events[EVT_CORE][0].hdr.evt_class);
			Assert::AreEqual<UINT16>(0x1234, extra_core_events[EVT_CORE][0].hdr.num);

			Assert::AreEqual(L"name2"s, extra_core_events[EVT_CORE][1].name);
			Assert::AreEqual(EVT_CORE, extra_core_events[EVT_CORE][1].hdr.evt_class);
			Assert::AreEqual<UINT16>(0xabcd, extra_core_events[EVT_CORE][1].hdr.num);
		}

		TEST_METHOD(test_parse_events_extra_3_events_OK)
		{
			std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
			parse_events_extra(L"first:0x12,second:FF,third:12a", extra_core_events);

			Assert::AreEqual<size_t>(3, extra_core_events[EVT_CORE].size());

			Assert::AreEqual(L"first"s, extra_core_events[EVT_CORE][0].name);
			Assert::AreEqual(EVT_CORE, extra_core_events[EVT_CORE][0].hdr.evt_class);
			Assert::AreEqual<UINT16>(0x12, extra_core_events[EVT_CORE][0].hdr.num);

			Assert::AreEqual(L"second"s, extra_core_events[EVT_CORE][1].name);
			Assert::AreEqual(EVT_CORE, extra_core_events[EVT_CORE][1].hdr.evt_class);
			Assert::AreEqual<UINT16>(0xff, extra_core_events[EVT_CORE][1].hdr.num);

			Assert::AreEqual(L"third"s, extra_core_events[EVT_CORE][2].name);
			Assert::AreEqual(EVT_CORE, extra_core_events[EVT_CORE][2].hdr.evt_class);
			Assert::AreEqual<UINT16>(0x12a, extra_core_events[EVT_CORE][2].hdr.num);
		}

		TEST_METHOD(test_parse_events_extra_whitespace_NOK)
		{
			{
				auto wrapper_whitespace_in_name = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"first:0x12,second:FF, third:12a", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_whitespace_in_name);
			}

			{
				auto wrapper_whitespace_in_num = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"first: 0x12,second:FF,third:12a", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_whitespace_in_num);
			}

			{
				auto wrapper_whitespace_tab = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"first:0x12,second:FF,third:12a\t", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_whitespace_tab);
			}
		}

		TEST_METHOD(test_parse_events_extra_delim_NOK)
		{
			{
				auto wrapper_no_delim_in_name = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"first0x12,second:FF,third:12a", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_no_delim_in_name);
			}

			{
				auto wrapper_no_delim_in_num = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"first:0x12,second:FF,0x1234", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_no_delim_in_num);
			}
		}

		TEST_METHOD(test_parse_events_extra_format_misc_NOK)
		{
			{
				auto wrapper_empty_str = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_empty_str);
			}

			{
				auto wrapper_extra_delim = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"first:0x12:", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_extra_delim);
			}

			{
				auto wrapper_extra_large_num = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"first:0xffffaaaa", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_extra_large_num);
			}

			{
				auto wrapper_extra_large_num_2 = [=]() {
					std::map<enum evt_class, std::vector<struct extra_event>> extra_core_events;
					parse_events_extra(L"name1:0x1000,name2:0x10000", extra_core_events);;
				};
				Assert::ExpectException<fatal_exception>(wrapper_extra_large_num_2);
			}
		}
	};

	/****************************************************************************/

	TEST_CLASS(wperftest_parsers_counting)
	{
		public:
			TEST_METHOD(test_parse_events_str_EVT_CORE_rf_in_group)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{rf}", note, pmu_cfg);

			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<uint16_t>(0xF, events.ungrouped[EVT_CORE][0].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_CORE_2_raw_events_in_group)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{r1b,r75}", note, pmu_cfg);

			Assert::AreEqual<size_t>(3, events.groups[EVT_CORE].size());

			Assert::AreEqual<uint16_t>(0x2, events.groups[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x1B, events.groups[EVT_CORE][1].index);
			Assert::AreEqual<uint16_t>(0x75, events.groups[EVT_CORE][2].index);

			Assert::AreEqual(EVT_HDR, events.groups[EVT_CORE][0].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][1].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][2].type);
		}

		TEST_METHOD(test_parse_events_str_EVT_CORE_raw_events_in_group_4)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"{r1b,r75},r74,r73,r70,r71", note, pmu_cfg);

			Assert::AreEqual<size_t>(4, events.ungrouped[EVT_CORE].size());

			Assert::AreEqual<uint16_t>(0x74, events.ungrouped[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x73, events.ungrouped[EVT_CORE][1].index);
			Assert::AreEqual<uint16_t>(0x70, events.ungrouped[EVT_CORE][2].index);
			Assert::AreEqual<uint16_t>(0x71, events.ungrouped[EVT_CORE][3].index);

			Assert::AreEqual<size_t>(3, events.groups[EVT_CORE].size());

			Assert::AreEqual<uint16_t>(0x2, events.groups[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x1B, events.groups[EVT_CORE][1].index);
			Assert::AreEqual<uint16_t>(0x75, events.groups[EVT_CORE][2].index);

			Assert::AreEqual(EVT_HDR, events.groups[EVT_CORE][0].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][1].type);
			Assert::AreEqual(EVT_GROUPED, events.groups[EVT_CORE][2].type);
		}

		/****************************************************************************/

		TEST_METHOD(test_parse_events_str_EVT_CORE_rf)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"rf", note, pmu_cfg);

			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<uint16_t>(0xF, events.ungrouped[EVT_CORE][0].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_CORE_r1b)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"r1b", note, pmu_cfg);

			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<uint16_t>(0x1B, events.ungrouped[EVT_CORE][0].index);
		}

		TEST_METHOD(test_parse_events_str_raw_event_max_num)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"rffff", note, pmu_cfg);

			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<uint16_t>(0xFFFF, events.ungrouped[EVT_CORE][0].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_CORE_r_7_events)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

			auto events = parse_events_str(L"ra,r3c,rdab,rdead,r1a2b,rfedc,r1234", note, pmu_cfg);

			Assert::AreEqual<size_t>(7, events.ungrouped[EVT_CORE].size());

			Assert::AreEqual<uint16_t>(0xA, events.ungrouped[EVT_CORE][0].index);
			Assert::AreEqual<uint16_t>(0x3C, events.ungrouped[EVT_CORE][1].index);
			Assert::AreEqual<uint16_t>(0xDAB, events.ungrouped[EVT_CORE][2].index);
			Assert::AreEqual<uint16_t>(0xDEAD, events.ungrouped[EVT_CORE][3].index);
			Assert::AreEqual<uint16_t>(0x1A2B, events.ungrouped[EVT_CORE][4].index);
			Assert::AreEqual<uint16_t>(0xFEDC, events.ungrouped[EVT_CORE][5].index);
			Assert::AreEqual<uint16_t>(0x1234, events.ungrouped[EVT_CORE][6].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_DSU_1_event)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]
			pmu_cfg.gpc_nums[EVT_DSU] = 6;

			auto events = parse_events_str(L"/dsu/l3d_cache_refill", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_DSU].size());

			Assert::AreEqual<uint16_t>(0x2A, events.ungrouped[EVT_DSU][0].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_DSU_2_events)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]
			pmu_cfg.gpc_nums[EVT_DSU] = 6;

			auto events = parse_events_str(L"/dsu/l3d_cache,/dsu/l3d_cache_refill", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<size_t>(2, events.ungrouped[EVT_DSU].size());

			Assert::AreEqual<uint16_t>(0x2B, events.ungrouped[EVT_DSU][0].index);
			Assert::AreEqual<uint16_t>(0x2A, events.ungrouped[EVT_DSU][1].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_DSU_2_events_case_insensitive_prefix)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]
			pmu_cfg.gpc_nums[EVT_DSU] = 6;

			auto events = parse_events_str(L"/DSU/l3d_cache,/Dsu/l3d_cache_refill", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<size_t>(2, events.ungrouped[EVT_DSU].size());

			Assert::AreEqual<uint16_t>(0x2B, events.ungrouped[EVT_DSU][0].index);
			Assert::AreEqual<uint16_t>(0x2A, events.ungrouped[EVT_DSU][1].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_DSU_2_events_uppercase)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]
			pmu_cfg.gpc_nums[EVT_DSU] = 6;

			auto events = parse_events_str(L"/DSU/L3D_CACHE,/DSU/L3D_CACHE_REFILL", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<size_t>(2, events.ungrouped[EVT_DSU].size());

			Assert::AreEqual<uint16_t>(0x2B, events.ungrouped[EVT_DSU][0].index);
			Assert::AreEqual<uint16_t>(0x2A, events.ungrouped[EVT_DSU][1].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_DMC_CLKDIV2_1_event)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]
			pmu_cfg.gpc_nums[EVT_DSU] = 6;
			pmu_cfg.gpc_nums[EVT_DMC_CLK] = 2;
			pmu_cfg.gpc_nums[EVT_DMC_CLKDIV2] = 8;

			auto events = parse_events_str(L"/dmc_clkdiv2/rdwr", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_DSU].size());
			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_DMC_CLK].size());
			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_DMC_CLKDIV2].size());

			Assert::AreEqual<uint16_t>(0x12, events.ungrouped[EVT_DMC_CLKDIV2][0].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_DMC_CLKDIV2_1_event_uppercase)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]
			pmu_cfg.gpc_nums[EVT_DSU] = 6;
			pmu_cfg.gpc_nums[EVT_DMC_CLK] = 2;
			pmu_cfg.gpc_nums[EVT_DMC_CLKDIV2] = 8;

			auto events = parse_events_str(L"/dmc_clkdiv2/RDWR", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_DSU].size());
			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_DMC_CLK].size());
			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_DMC_CLKDIV2].size());

			Assert::AreEqual<uint16_t>(0x12, events.ungrouped[EVT_DMC_CLKDIV2][0].index);
		}

		TEST_METHOD(test_parse_events_str_EVT_DMC_CLKDIV2_1_event_prefix_uppercase)
		{
			std::wstring note;
			struct pmu_device_cfg pmu_cfg = { 0 };

			pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]
			pmu_cfg.gpc_nums[EVT_DSU] = 6;
			pmu_cfg.gpc_nums[EVT_DMC_CLK] = 2;
			pmu_cfg.gpc_nums[EVT_DMC_CLKDIV2] = 8;

			auto events = parse_events_str(L"/DMC_CLKDIV2/rdwr", note, pmu_cfg);

			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_CORE].size());
			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_DSU].size());
			Assert::AreEqual<size_t>(0, events.ungrouped[EVT_DMC_CLK].size());
			Assert::AreEqual<size_t>(1, events.ungrouped[EVT_DMC_CLKDIV2].size());

			Assert::AreEqual<uint16_t>(0x12, events.ungrouped[EVT_DMC_CLKDIV2][0].index);
		}

		TEST_METHOD(test_parse_events_str_misc)
		{
			{
				auto wrapper_extra_large_num = [=]() {
					std::wstring note;
					struct pmu_device_cfg pmu_cfg = { 0 };

					pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

					auto events = parse_events_str(L"r10000", note, pmu_cfg);
				};
				Assert::ExpectException<fatal_exception>(wrapper_extra_large_num);
			}

			{
				auto wrapper_extra_large_num_2 = [=]() {
					std::wstring note;
					struct pmu_device_cfg pmu_cfg = { 0 };

					pmu_cfg.gpc_nums[EVT_CORE] = 6;	// parse_events_str only uses gpc_nums[]

					auto events = parse_events_str(L"r1,r10000", note, pmu_cfg);
				};
				Assert::ExpectException<fatal_exception>(wrapper_extra_large_num_2);
			}
		}
	};
}

template<> std::wstring CppUnitTestFramework::ToString<evt_class>(const evt_class& q)
{
	switch (q)
	{
	case EVT_CORE:        return L"EVT_CORE";
	case EVT_DSU:         return L"EVT_DSU";
	case EVT_DMC_CLK:     return L"EVT_DMC_CLK";
	case EVT_DMC_CLKDIV2: return L"EVT_DMC_CLKDIV2";
	case EVT_CLASS_NUM:   return L"EVT_CLASS_NUM";
	}
	return L"???";
}

template<> std::wstring CppUnitTestFramework::ToString<evt_type>(const evt_type& q)
{
	switch (q)
	{
	case EVT_NORMAL:         return L"EVT_NORMAL";
	case EVT_GROUPED:        return L"EVT_GROUPED";
	case EVT_METRIC_NORMAL:  return L"EVT_METRIC_NORMAL";
	case EVT_METRIC_GROUPED: return L"EVT_METRIC_GROUPED";
	case EVT_PADDING:        return L"EVT_PADDING";
	case EVT_HDR:            return L"EVT_HDR";
	}
	return L"???";
}
