// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "pch.h"
#include "CppUnitTest.h"
#include <windows.h>
#include "wperf/spe_parser.h"

using namespace std::literals;
using namespace Microsoft::VisualStudio;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace SPEParser;

namespace wperftest
{
    TEST_CLASS(wperftest_spe_parser)
    {
    public:
        TEST_METHOD(test_spe_parser_get_address_packet_security_state)
        {
            auto process = [](AddressType index, UINT64 address)
            {
                Packet packet;
                packet.add(static_cast<UINT8>(PacketType::ADDRESS) | static_cast<UINT8>(index));
                UINT8* p = reinterpret_cast<UINT8*>(&address);
                for (int i = 0; i < sizeof(address); i++)
                    packet.add(*p++);
                Assert::AreEqual(ParsingState::FINISHED, packet.m_state);
                AddressPacket addr(packet);
                Assert::AreEqual(index, addr.m_address_type);
                return addr.get_security_state();
            };
            Assert::AreEqual(SecurityState::SECURE,     process(AddressType::INSTRUCTION_VIRTUAL, 0x00000000'00000000));
            Assert::AreEqual(SecurityState::NON_SECURE, process(AddressType::INSTRUCTION_VIRTUAL, 0x80000000'00000000));
            Assert::AreEqual(SecurityState::REALM,      process(AddressType::INSTRUCTION_VIRTUAL, 0x90000000'00000000));

            Assert::AreEqual(SecurityState::SECURE,     process(AddressType::DATA_ACCESS_PHYSICAL, 0x00000000'00000000));
            Assert::AreEqual(SecurityState::NON_SECURE, process(AddressType::DATA_ACCESS_PHYSICAL, 0x10000000'00000000));
            Assert::AreEqual(SecurityState::REALM,      process(AddressType::DATA_ACCESS_PHYSICAL, 0x90000000'00000000));
        }

        TEST_METHOD(test_spe_parser_get_address_packet_exception_level)
        {
            auto process = [](UINT64 address)
            {
                Packet packet;
                packet.add(static_cast<UINT8>(PacketType::ADDRESS));
                UINT8* p = reinterpret_cast<UINT8*>(&address);
                for (int i = 0; i < sizeof(address); i++)
                    packet.add(*p++);
                Assert::AreEqual(ParsingState::FINISHED, packet.m_state);
                AddressPacket addr(packet);
                Assert::AreEqual(AddressType::INSTRUCTION_VIRTUAL, addr.m_address_type);
                return addr.get_exception_level();
            };
            Assert::AreEqual<int>(0, process(0x00000000'00000000));
            Assert::AreEqual<int>(1, process(0x20000000'00000000));
            Assert::AreEqual<int>(2, process(0x40000000'00000000));
            Assert::AreEqual<int>(3, process(0x60000000'00000000));
        }

        TEST_METHOD(test_spe_parser_get_address_packet_tag)
        {
            auto process = [](UINT64 address)
            {
                Packet packet;
                packet.add(static_cast<UINT8>(PacketType::ADDRESS) | static_cast<UINT8>(AddressType::DATA_ACCESS_VIRTUAL));
                UINT8* p = reinterpret_cast<UINT8*>(&address);
                for (int i = 0; i < sizeof(address); i++)
                    packet.add(*p++);
                Assert::AreEqual(ParsingState::FINISHED, packet.m_state);
                AddressPacket addr(packet);
                Assert::AreEqual(AddressType::DATA_ACCESS_VIRTUAL, addr.m_address_type);
                return addr.get_tag();
            };
            Assert::AreEqual<int>(0x00, process(0x00000000'00000000));
            Assert::AreEqual<int>(0x55, process(0x55000000'00000000));
            Assert::AreEqual<int>(0xAA, process(0xAA000000'00000000));
            Assert::AreEqual<int>(0xF5, process(0xF5000000'00000000));
        }

        TEST_METHOD(test_spe_parser_get_address_packet_tag_checked)
        {
            auto process = [](UINT64 address)
            {
                Packet packet;
                packet.add(static_cast<UINT8>(PacketType::ADDRESS) | static_cast<UINT8>(AddressType::DATA_ACCESS_PHYSICAL));
                UINT8* p = reinterpret_cast<UINT8*>(&address);
                for (int i = 0; i < sizeof(address); i++)
                    packet.add(*p++);
                Assert::AreEqual(ParsingState::FINISHED, packet.m_state);
                AddressPacket addr(packet);
                Assert::AreEqual(AddressType::DATA_ACCESS_PHYSICAL, addr.m_address_type);
                return addr.get_tag_checked();
            };
            Assert::IsFalse(process(0x00000000'00000000));
            Assert::IsTrue (process(0x40000000'00000000));
        }

        TEST_METHOD(test_spe_parser_get_address_packet_physical_address_tag)
        {
            auto process = [](UINT64 address)
            {
                Packet packet;
                packet.add(static_cast<UINT8>(PacketType::ADDRESS) | static_cast<UINT8>(AddressType::DATA_ACCESS_PHYSICAL));
                UINT8* p = reinterpret_cast<UINT8*>(&address);
                for (int i = 0; i < sizeof(address); i++)
                    packet.add(*p++);
                Assert::AreEqual(ParsingState::FINISHED, packet.m_state);
                AddressPacket addr(packet);
                Assert::AreEqual(AddressType::DATA_ACCESS_PHYSICAL, addr.m_address_type);
                return addr.get_physical_address_tag();
            };
            Assert::AreEqual<int>( 0, process(0x00000000'00000000));
            Assert::AreEqual<int>( 1, process(0x01000000'00000000));
            Assert::AreEqual<int>( 5, process(0x05000000'00000000));
            Assert::AreEqual<int>(15, process(0x0F000000'00000000));
        }

        TEST_METHOD(test_spe_parser_get_counter_packet)
        {
            auto process = [](CounterType index, UINT16 value)
            {
                Packet packet;
                packet.add(static_cast<UINT8>(PacketType::COUNTER) | static_cast<UINT8>(index));
                UINT8* p = reinterpret_cast<UINT8*>(&value);
                for (int i = 0; i < sizeof(value); i++)
                    packet.add(*p++);
                Assert::AreEqual(ParsingState::FINISHED, packet.m_state);
                CounterPacket ctr(packet);
                Assert::AreEqual(index, ctr.get_counter_type());
                return ctr;
            };

            Assert::AreEqual(CounterType::TOTAL_LATENCY, process(CounterType::TOTAL_LATENCY, 0).get_counter_type());
            Assert::AreEqual(CounterType::ISSUE_LATENCY, process(CounterType::ISSUE_LATENCY, 0).get_counter_type());
            Assert::AreEqual(CounterType::TRANSLATION_LATENCY, process(CounterType::TRANSLATION_LATENCY, 0).get_counter_type());
            Assert::AreEqual(CounterType::ALTERNATE_CLOCK_ISSUE_LATENCY, process(CounterType::ALTERNATE_CLOCK_ISSUE_LATENCY, 0).get_counter_type());

            Assert::AreEqual<int>(     0, process(CounterType::TOTAL_LATENCY,      0).get_count());
            Assert::AreEqual<int>(  1234, process(CounterType::TOTAL_LATENCY,   1234).get_count());
            Assert::AreEqual<int>( 0xFFF, process(CounterType::TOTAL_LATENCY,  0xFFF).get_count());
            Assert::AreEqual<int>(0xFFFF, process(CounterType::TOTAL_LATENCY, 0xFFFF).get_count());
        }

        TEST_METHOD(test_spe_parser_get_event_packet)
        {
            auto process = [](int size, UINT64 events)
            {
                Packet packet;
                packet.add(static_cast<UINT8>(PacketType::EVENTS) | (static_cast<UINT8>(size) << 4));
                UINT8* p = reinterpret_cast<UINT8*>(&events);
                for (int i = 0; i < (1 << size); i++)
                    packet.add(*p++);
                Assert::AreEqual(ParsingState::FINISHED, packet.m_state);
                EventsPacket e(packet);
                Assert::AreEqual(1 << size, e.get_size());
                return e.get_event_desc();
            };

            Assert::AreEqual(L""s, process(0, 0));
            Assert::AreEqual(L""s, process(1, 0));
            Assert::AreEqual(L""s, process(2, 0));
            Assert::AreEqual(L""s, process(3, 0));
            Assert::AreEqual(L"generated-exception"s, process(0, 0b00000001));
            Assert::AreEqual(L"generated-exception+mispredicted"s, process(0, 0b10000001));
            Assert::AreEqual(L"last-level-cache-miss+implementation-defined-event-14"s, process(1, 0b01000010 << 8));
            Assert::AreEqual(L"last-level-cache-access+implementation-defined-event-15"s, process(1, 0b10000001 << 8));
            Assert::AreEqual(L"level1-data-cache-access+tlb-walk+last-level-cache-access+implementation-defined-event-13"s, process(1, 0b00100001'00100100));
            Assert::AreEqual(L"transactional+level-2-data-cache-access+data-snooped"s, process(2, 0b10001001 << 16));
            Assert::AreEqual(L"tlb-access+remote-access+cache-data-modified"s, process(2, 0b00100000'00000100'00010000));
            Assert::AreEqual(L"generated-exception+implementation-defined-event-49+implementation-defined-event-63"s, process(3, 0x80020000'00000001));
        }

        TEST_METHOD(test_spe_parser_get_operation_type_packet)
        {
            auto process = [](OperationTypeClass class_, OperationTypeSubclass subclass, UINT8 payload = 0)
            {
                Packet packet;
                packet.add(static_cast<UINT8>(PacketType::OPERATION_TYPE) | static_cast<UINT8>(class_));
                packet.add(static_cast<UINT8>(subclass) | payload);
                Assert::AreEqual(ParsingState::FINISHED, packet.m_state);
                OperationTypePacket op(packet);
                return op.get_desc();
            };

            Assert::AreEqual(L"OTHER"s, process(OperationTypeClass::OTHER, OperationTypeSubclass::OTHER));
            Assert::AreEqual(L"OTHER-SVE"s, process(OperationTypeClass::OTHER, OperationTypeSubclass::DATA_PROCESSING_SVE_VECTOR));
            Assert::AreEqual(L"OTHER-SME"s, process(OperationTypeClass::OTHER, OperationTypeSubclass::DATA_PROCESSING_SME_ARRAY));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-LOAD-GP"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_GENERAL_PURPOSE));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-STORE-SIMD-FP"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_SIMD_FP, 1));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-LOAD-UNSPECIFIED"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_UNSPECIFIED));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-STORE-TAG"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_ALLOCATION_TAG, 1));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-LOAD-SYSREG"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_SYSTEM_REGISTER));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-STORE-MEMCPY"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_MEMORY_COPY, 1));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-LOAD-EXT"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_EXTENDED));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-STORE-SVE-SME"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_SVE_OR_SME, 1));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-LOAD-GCS"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_GCS));
            Assert::AreEqual(L"LOAD_STORE_ATOMIC-STORE-MEMSET"s, process(OperationTypeClass::LOAD_STORE_ATOMIC, OperationTypeSubclass::LOAD_STORE_MEMORY_SET));
            Assert::AreEqual(L"BRANCH_OR_EXCEPTION-UNCONDITIONAL-DIRECT"s, process(OperationTypeClass::BRANCH_OR_EXCEPTION, OperationTypeSubclass::BRANCH_OR_EXCEPTION));
            Assert::AreEqual(L"BRANCH_OR_EXCEPTION-CONDITIONAL-DIRECT"s, process(OperationTypeClass::BRANCH_OR_EXCEPTION, OperationTypeSubclass::BRANCH_OR_EXCEPTION, 1));
            Assert::AreEqual(L"BRANCH_OR_EXCEPTION-CONDITIONAL-INDIRECT"s, process(OperationTypeClass::BRANCH_OR_EXCEPTION, OperationTypeSubclass::BRANCH_OR_EXCEPTION, 3));
        }
    };
}

template<> std::wstring CppUnitTestFramework::ToString<AddressType>(const AddressType& q)
{
    switch (q)
    {
    case AddressType::INSTRUCTION_VIRTUAL:    return L"INSTRUCTION_VIRTUAL";
    case AddressType::BRANCH_TARGET:          return L"BRANCH_TARGET";
    case AddressType::DATA_ACCESS_VIRTUAL:    return L"DATA_ACCESS_VIRTUAL";
    case AddressType::DATA_ACCESS_PHYSICAL:   return L"DATA_ACCESS_PHYSICAL";
    case AddressType::PREVIOUS_BRANCH_TARGET: return L"PREVIOUS_BRANCH_TARGET";
    }
    return L"???";
}

template<> std::wstring CppUnitTestFramework::ToString<CounterType>(const CounterType& q)
{
    switch (q)
    {
    case CounterType::TOTAL_LATENCY:                 return L"TOTAL_LATENCY";
    case CounterType::ISSUE_LATENCY:                 return L"ISSUE_LATENCY";
    case CounterType::TRANSLATION_LATENCY:           return L"TRANSLATION_LATENCY";
    case CounterType::ALTERNATE_CLOCK_ISSUE_LATENCY: return L"ALTERNATE_CLOCK_ISSUE_LATENCY";
    }
    return L"???";
}

template<> std::wstring CppUnitTestFramework::ToString<ParsingState>(const ParsingState& q)
{
    switch (q)
    {
    case ParsingState::READING_HEADER: return L"READING_HEADER";
    case ParsingState::READING_DATA:   return L"READING_DATA";
    case ParsingState::FINISHED:       return L"FINISHED";
    }
    return L"???";
}

template<> std::wstring CppUnitTestFramework::ToString<SecurityState>(const SecurityState& q)
{
    switch (q)
    {
    case SecurityState::SECURE:     return L"SECURE";
    case SecurityState::NON_SECURE: return L"NON_SECURE";
    case SecurityState::REALM:      return L"REALM";
    }
    return L"???";
}
