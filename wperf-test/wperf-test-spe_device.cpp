// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "pch.h"
#include "CppUnitTest.h"

#include "wperf\spe_device.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace wperftest
{
	TEST_CLASS(wperftest_spe_device)
	{
	public:

		TEST_METHOD(test_spe_device_get_spe_version_name)
		{
			Assert::AreEqual(spe_device::get_spe_version_name(0x00000000000110305408), std::wstring(L"FEAT_SPE"));
			Assert::AreEqual(spe_device::get_spe_version_name(0x0000000000f210305609), std::wstring(L"FEAT_SPEv1p1"));
		}

		TEST_METHOD(test_spe_device_is_spe_supported)
		{
			Assert::IsTrue(spe_device::is_spe_supported(0x00000000000110305408));
			Assert::IsTrue(spe_device::is_spe_supported(0x0000000000f210305609));
		}

		TEST_METHOD(test_spe_device_ID_AA64DFR0_EL1_PMSVer)
		{
			Assert::AreEqual(uint64_t(0b001), uint64_t(ID_AA64DFR0_EL1_PMSVer(0x00000000000110305408)));
			Assert::AreEqual(uint64_t(0b010), uint64_t(ID_AA64DFR0_EL1_PMSVer(0x0000000000f210305609)));
		}

		TEST_METHOD(test_spe_device_get_spe_version_name_nok)
		{
			Assert::IsFalse(spe_device::is_spe_supported(0ULL));
		}

		TEST_METHOD(test_spe_device_filter_name)
		{
			Assert::IsTrue(spe_device::is_filter_name(L"load_filter"));
			Assert::IsTrue(spe_device::is_filter_name(L"store_filter"));
			Assert::IsTrue(spe_device::is_filter_name(L"branch_filter"));
			Assert::IsTrue(spe_device::is_filter_name(L"ts_enable"));
			Assert::IsTrue(spe_device::is_filter_name(L"min_latency"));
			Assert::IsTrue(spe_device::is_filter_name(L"jitter"));
			Assert::IsTrue(spe_device::is_filter_name(L"period"));
		}

		TEST_METHOD(test_spe_device_filter_name_as_alias)
		{
			Assert::IsTrue(spe_device::is_filter_name(L"ld"));
			Assert::IsTrue(spe_device::is_filter_name(L"st"));
			Assert::IsTrue(spe_device::is_filter_name(L"b"));
			Assert::IsTrue(spe_device::is_filter_name(L"ts"));
			Assert::IsTrue(spe_device::is_filter_name(L"min"));
			Assert::IsTrue(spe_device::is_filter_name(L"j"));
			Assert::IsTrue(spe_device::is_filter_name(L"per"));
		}

		TEST_METHOD(test_spe_device_filter_name_is_alias)
		{
			Assert::IsTrue(spe_device::is_filter_name_alias(L"ld"));
			Assert::IsTrue(spe_device::is_filter_name_alias(L"st"));
			Assert::IsTrue(spe_device::is_filter_name_alias(L"b"));
			Assert::IsTrue(spe_device::is_filter_name_alias(L"ts"));
			Assert::IsTrue(spe_device::is_filter_name_alias(L"min"));
			Assert::IsTrue(spe_device::is_filter_name_alias(L"j"));
			Assert::IsTrue(spe_device::is_filter_name_alias(L"per"));
		}

		TEST_METHOD(test_spe_device_get_filter_name)
		{
			Assert::AreEqual(spe_device::get_filter_name(L"ld"), std::wstring(L"load_filter"));
			Assert::AreEqual(spe_device::get_filter_name(L"st"), std::wstring(L"store_filter"));
			Assert::AreEqual(spe_device::get_filter_name(L"b"), std::wstring(L"branch_filter"));
			Assert::AreEqual(spe_device::get_filter_name(L"ts"), std::wstring(L"ts_enable"));
			Assert::AreEqual(spe_device::get_filter_name(L"min"), std::wstring(L"min_latency"));
			Assert::AreEqual(spe_device::get_filter_name(L"j"), std::wstring(L"jitter"));
			Assert::AreEqual(spe_device::get_filter_name(L"per"), std::wstring(L"period"));

			Assert::AreEqual(spe_device::get_filter_name(L"load_filter"), std::wstring(L"load_filter"));
			Assert::AreEqual(spe_device::get_filter_name(L"store_filter"), std::wstring(L"store_filter"));
			Assert::AreEqual(spe_device::get_filter_name(L"branch_filter"), std::wstring(L"branch_filter"));
			Assert::AreEqual(spe_device::get_filter_name(L"ts_enable"), std::wstring(L"ts_enable"));
			Assert::AreEqual(spe_device::get_filter_name(L"min_latency"), std::wstring(L"min_latency"));
			Assert::AreEqual(spe_device::get_filter_name(L"jitter"), std::wstring(L"jitter"));
			Assert::AreEqual(spe_device::get_filter_name(L"period"), std::wstring(L"period"));
		}

		TEST_METHOD(test_get_filter_min_max_values_n1_0_or_1)
		{
			const uint64_t pmsidr_el1_value = 0x00000000000000026417;	// PMSIDR_EL1 from example neoverse-n1

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"load_filter", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"load_filter", pmsidr_el1_value).second);

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"store_filter", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"store_filter", pmsidr_el1_value).second);

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"branch_filter", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"branch_filter", pmsidr_el1_value).second);

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"ts_enable", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"ts_enable", pmsidr_el1_value).second);

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"jitter", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"jitter", pmsidr_el1_value).second);
		}

		TEST_METHOD(test_get_filter_min_max_values_n1_2)
		{
			const uint64_t pmsidr_el1_value = 0x00000000000000026417;						// PMSIDR_EL1 from example neoverse-n1
			const uint64_t min_latency = SPE_CTL_FLAG_MINLAT_MASK;							// By default 16-bit value
			const uint64_t min_latency_12 = min_latency & SPE_CTL_FLAG_VAL_12_BIT_MASK;		// Can be 12-bit depending on SPE HW configuration

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"min_latency", pmsidr_el1_value).first);
			Assert::AreEqual(min_latency_12, spe_device::get_filter_min_max_values(L"min_latency", pmsidr_el1_value).second);

			Assert::AreEqual(1024ull, spe_device::get_filter_min_max_values(L"period", pmsidr_el1_value).first);
			Assert::AreEqual(uint64_t(SPE_CTL_INTERVAL_VAL_MASK), spe_device::get_filter_min_max_values(L"period", pmsidr_el1_value).second);
		}

		TEST_METHOD(test_get_filter_min_max_values_n2_0_or_1)
		{
			const uint64_t pmsidr_el1_value = 0x0000000000f210305609;	// PMSIDR_EL1 from example neoverse-n2

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"load_filter", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"load_filter", pmsidr_el1_value).second);

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"store_filter", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"store_filter", pmsidr_el1_value).second);

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"branch_filter", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"branch_filter", pmsidr_el1_value).second);

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"ts_enable", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"ts_enable", pmsidr_el1_value).second);

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"jitter", pmsidr_el1_value).first);
			Assert::AreEqual(1ull, spe_device::get_filter_min_max_values(L"jitter", pmsidr_el1_value).second);
		}

		TEST_METHOD(test_get_filter_min_max_values_n2_2)
		{
			const uint64_t pmsidr_el1_value = 0x0000000000f210305609;						// PMSIDR_EL1 from example neoverse-n2
			const uint64_t min_latency = SPE_CTL_FLAG_MINLAT_MASK;							// By default 16-bit value
			const uint64_t min_latency_12 = min_latency & SPE_CTL_FLAG_VAL_12_BIT_MASK;		// Can be 12-bit depending on SPE HW configuration

			Assert::AreEqual(0ull, spe_device::get_filter_min_max_values(L"min_latency", pmsidr_el1_value).first);
			Assert::AreEqual(uint64_t(min_latency), spe_device::get_filter_min_max_values(L"min_latency", pmsidr_el1_value).second);

			Assert::AreEqual(2048ull, spe_device::get_filter_min_max_values(L"period", pmsidr_el1_value).first);
			Assert::AreEqual(uint64_t(SPE_CTL_INTERVAL_VAL_MASK), spe_device::get_filter_min_max_values(L"period", pmsidr_el1_value).second);
		}
	};
}
