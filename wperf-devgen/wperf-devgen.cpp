// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <iomanip>
#include <iostream>
#include <fstream>
#include <conio.h>
#include <windows.h>
#include <swdevice.h>
#include <newdev.h>
#include <optional>
#include <sstream>
#include <strsafe.h>
#include <SetupAPI.h>
#include <initguid.h>
#include <devpropdef.h>
#include <devpkey.h>
#include <newdev.h>

#include "wperf-common\public_ver.h"
#include "wperf-common\gitver.h"

#define MAX_STRING_LENGTH 1024
#define MAX_HARDWARELIST_SIZE 2048

// Taken from wperf-driver INF file.
PCWSTR instanceId = L"WPERFDRIVER";
PCWSTR hardwareId = L"Root\\WPERFDRIVER";
PCWSTR compatibleIds = L"Root\\WPERFDRIVER";
PCWSTR devDescription = L"WPERFDRIVER Driver";

std::optional<std::wstring> FullInfPath;
std::wstring thisExeName = L"wperf-devgen.exe";
std::wstring thisProjectName = L"wperf-devgen";
std::wstring driverProjectName = L"wperf-driver";
const std::wstring InfName(L"wperf-driver.inf");

void GetFullInfPath()
{
    if (!FullInfPath)
    {
        // Try finding the .inf in the current working directory first
        DWORD size = GetCurrentDirectory(0, NULL);
        try
        {
            std::unique_ptr<wchar_t[]> buff = std::make_unique<wchar_t[]>(size);
            GetCurrentDirectory(size, buff.get());
            std::wstringstream wstream;
            wstream << std::wstring(buff.get()) << L"\\" << InfName;
            
            std::wifstream inf;
            inf.open(wstream.str(), std::wios::in);
            if (inf.is_open())
            {
                FullInfPath = wstream.str();
                std::wcout << "Found path " << *FullInfPath << std::endl;
                inf.close();
                return;
            }
        }
        catch (const std::bad_alloc& e)
        {
            std::cerr << "Error allocating buffer: " << e.what() << std::endl;
            exit(-1);
        }

        // Next, try finding the .inf file next to this program, if it wasn't run from its own directory
        size = MAX_PATH;
        try {
            std::unique_ptr<wchar_t[]> buff = std::make_unique<wchar_t[]>(size);
            GetModuleFileNameW(NULL, buff.get(), size);
            std::wstringstream wstream;
            std::wstring moduleFileName = std::wstring(buff.get());
            
            wstream << moduleFileName.erase(moduleFileName.length() - thisExeName.length(), thisExeName.length()) << L"\\" << InfName;

            std::wifstream inf;
            inf.open(wstream.str(), std::wios::in);
            if (inf.is_open())
            {
                FullInfPath = wstream.str();
                std::wcout << "Found path " << *FullInfPath << std::endl;
                inf.close();
                return;
            }
        }
        catch (const std::bad_alloc& e)
        {
            std::cerr << "Error allocating buffer: " << e.what() << std::endl;
            exit(-1);
        }

        // Finally, attempt to transform the wperf-devgen build directory into wperf-driver's, and look there
        size = MAX_PATH;
        try {
            std::unique_ptr<wchar_t[]> buff = std::make_unique<wchar_t[]>(size);
            GetModuleFileNameW(nullptr, buff.get(), size);

            std::wstring path = std::wstring(buff.get());
            path.erase(path.length() - thisExeName.length(), thisExeName.length());

            if (size_t pos = path.find(thisProjectName); pos != std::wstring::npos)
            {
                // Look for the driver with the same configuration and platform as the currently-running wperf-devgen
                path.replace(pos, thisProjectName.length(), driverProjectName);
                path += driverProjectName + L'\\' + InfName;

                std::wifstream inf;
                inf.open(path, std::wios::in);
                if (inf.is_open())
                {
                    FullInfPath = path;
                    std::wcout << "Found path " << *FullInfPath << std::endl;
                    inf.close();
                    return;
                }
            }
        }
        catch (const std::bad_alloc& e)
        {
            std::cerr << "Error allocating buffer: " << e.what() << std::endl;
            exit(-1);
        }

        std::wcerr << L"Unable to find " << InfName << L", make sure it's on the same path as wperf-devgen.exe" << std::endl;
        exit(-1);
    }
}

BOOL do_update_driver()
{
    BOOL exit = true, reboot = false;

    if (!UpdateDriverForPlugAndPlayDevices(NULL, hardwareId, FullInfPath->c_str(), INSTALLFLAG_FORCE, &reboot)) {
        exit = false;

        std::cerr << "Error updating driver";
        if (reboot)
        {
            std::cerr << " - Reboot please.";
        }
        std::cerr << std::endl;

        goto clean_update_driver;
    }

clean_update_driver:
    return exit;
}

BOOL do_search(HDEVINFO& deviceInfoSet, SP_DEVINFO_DATA& devInfoData)
{
    DWORD index = 0;
    SP_DEVINFO_DATA searchDevInfoData;

    searchDevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
    while (SetupDiEnumDeviceInfo(deviceInfoSet, index, &searchDevInfoData))
    {
        index++;
        DEVPROPTYPE propType;
        WCHAR returnedHardwareID[MAX_HARDWARELIST_SIZE];
        DWORD returnedSize = 0;

        ZeroMemory(returnedHardwareID, sizeof(returnedHardwareID));

        if (!SetupDiGetDeviceProperty(deviceInfoSet, &searchDevInfoData, &DEVPKEY_Device_HardwareIds, &propType, (PBYTE)returnedHardwareID, sizeof(returnedHardwareID), &returnedSize, 0))
        {
            /* We might not be able to get information for all devices. */
        }

        WCHAR* ptr = returnedHardwareID;
        while (ptr[0] != L'\0')
        {
            std::wstring currHardwareId(ptr);

            if (std::wstring(hardwareId) == currHardwareId)
            {
                std::wcout << std::wstring(ptr) << std::endl;
                devInfoData = searchDevInfoData;
                return true;
            }
            ptr += currHardwareId.size() + 1; // Jump to next string
        }
    }
    return false;
}

BOOL do_remove_device()
{
    BOOL exit = true;
    BOOL found = false;
    GUID classGUID;
    WCHAR className[MAX_STRING_LENGTH];
    WCHAR hwIdList[MAX_STRING_LENGTH];
    HDEVINFO deviceInfoSet = INVALID_HANDLE_VALUE;
    SP_DEVINFO_DATA deviceInfoData{ 0 };

    ZeroMemory(hwIdList, sizeof(hwIdList));
    if (FAILED(StringCchCopy(hwIdList, LINE_LEN, hardwareId))) {
        exit = false;
        goto clean_remove_device;
    }

    if (!SetupDiGetINFClass(FullInfPath->c_str(), &classGUID, className, sizeof(className) / sizeof(WCHAR), 0))
    {
        std::cerr << "Error getting INF Class information" << std::endl;
        exit = false;
        goto clean_remove_device;
    }

    deviceInfoSet = SetupDiGetClassDevs(NULL, NULL, NULL, DIGCF_ALLCLASSES | DIGCF_PRESENT);
    if (deviceInfoSet == INVALID_HANDLE_VALUE)
    {
        std::cerr << "Error retrieving class deviceInfoSet " <<  std::endl;
        exit = false;
        goto clean_remove_device;
    }

    found = do_search(deviceInfoSet, deviceInfoData);

    if(found)
    {
        std::cout << "Device found" << std::endl;
        if (!SetupDiCallClassInstaller(DIF_REMOVE, deviceInfoSet, &deviceInfoData))
        {
            std::cerr << "Error uninstalling device (" << GetLastError() << ")" << std::endl;
            exit = false;
            goto clean_remove_device;
        }
    }
    else {
        std::cerr << "Error - WindowsPerf Device not found" << std::endl;
        exit = false;
        goto clean_remove_device;
    }

clean_remove_device:
    if (deviceInfoSet != INVALID_HANDLE_VALUE) {
        SetupDiDestroyDeviceInfoList(deviceInfoSet);
    }

    return exit;
}

BOOL do_create_device()
{
    BOOL exit = true;
    GUID classGUID;
    WCHAR className[MAX_STRING_LENGTH];
    WCHAR hwIdList[MAX_STRING_LENGTH];
    HDEVINFO deviceInfoSet = INVALID_HANDLE_VALUE;
    SP_DEVINFO_DATA deviceInfoData{ 0 };

    ZeroMemory(hwIdList, sizeof(hwIdList));
    if (FAILED(StringCchCopy(hwIdList, LINE_LEN, hardwareId))) {
        exit = false;
        goto clean_create_device;
    }

     if (!SetupDiGetINFClass(FullInfPath->c_str(), &classGUID, className, sizeof(className) / sizeof(WCHAR), 0))
    {
        std::cerr << "Error getting INF Class information" << std::endl;
        exit = false;
        goto clean_create_device;
    }

    // Search for the device first
    {
        HDEVINFO deviceInfoSet = SetupDiGetClassDevs(NULL, NULL, NULL, DIGCF_ALLCLASSES | DIGCF_PRESENT);
        if (deviceInfoSet == INVALID_HANDLE_VALUE)
        {
            std::cerr << "Error retrieving class deviceInfoSet " << std::endl;
            exit = false;
            goto clean_create_device;
        }

        if (do_search(deviceInfoSet, deviceInfoData))
        {
            std::cerr << "Device already installed" << std::endl;
            exit = false;
            goto clean_create_device;
        }
    }

    deviceInfoSet = SetupDiCreateDeviceInfoList(&classGUID, 0);
    if (deviceInfoSet == INVALID_HANDLE_VALUE)
    {
        std::cerr << "Error creating device info list" << std::endl;
        exit = false;
        goto clean_create_device;
    }

    deviceInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
    if (!SetupDiCreateDeviceInfo(deviceInfoSet,
        className,
        &classGUID,
        NULL,
        0,
        DICD_GENERATE_ID,
        &deviceInfoData))
    {
        std::cerr << "Error creating device info" << std::endl;
        exit = false;
        goto clean_create_device;
    }

    if (!SetupDiSetDeviceRegistryProperty(deviceInfoSet, &deviceInfoData, SPDRP_HARDWAREID, (LPBYTE)hwIdList, ((DWORD)wcslen(hwIdList) + 1 + 1) * sizeof(WCHAR)))
    {
        std::cerr << "Error setting device registry property" << std::endl;
        exit = false;
        goto clean_create_device;
    }

    if (!SetupDiCallClassInstaller(DIF_REGISTERDEVICE,
        deviceInfoSet,
        &deviceInfoData))
    {
        std::cerr << "Error calling class installer" << std::endl;
        exit = false;
    }
clean_create_device:
    if (deviceInfoSet != INVALID_HANDLE_VALUE) {
        SetupDiDestroyDeviceInfoList(deviceInfoSet);
    }

    return exit;
}

BOOL do_install_device()
{
    if (do_create_device())
        return do_update_driver();
    else
        return false;
}

BOOL do_state_change(DWORD StateChange)
{
    BOOL exit = true, found = false;
    HDEVINFO deviceInfoSet = INVALID_HANDLE_VALUE;
    SP_DEVINFO_DATA deviceInfoData{ 0 };
    SP_PROPCHANGE_PARAMS propChangeParams{ 0 };

    deviceInfoSet = SetupDiGetClassDevs(nullptr, nullptr, nullptr, DIGCF_ALLCLASSES | DIGCF_PRESENT);
    if (deviceInfoSet == INVALID_HANDLE_VALUE)
    {
        std::cerr << "Error retrieving class deviceInfoSet " << std::endl;
        exit = false;
        goto clean_state_change;
    }

    found = do_search(deviceInfoSet, deviceInfoData);
    if (!found)
    {
        std::cerr << "Error - WindowsPerf Device not found" << std::endl;
        exit = false;
        goto clean_state_change;
    }

    std::cout << "Device found" << std::endl;

    propChangeParams.ClassInstallHeader.cbSize = sizeof(SP_CLASSINSTALL_HEADER);
    propChangeParams.ClassInstallHeader.InstallFunction = DIF_PROPERTYCHANGE;

    propChangeParams.StateChange = StateChange;
    propChangeParams.Scope = DICS_FLAG_GLOBAL;
    propChangeParams.HwProfile = 0;

    if (!SetupDiSetClassInstallParams(deviceInfoSet, &deviceInfoData, &propChangeParams.ClassInstallHeader, sizeof(propChangeParams)))
    {
        std::cerr << "Error - Could not set property change parameter" << std::endl;
        exit = false;
        goto clean_state_change;
    }

    if (!SetupDiChangeState(deviceInfoSet, &deviceInfoData))
    {
        std::cerr << "Error - could not change device state" << std::endl;
        exit = false;
        goto clean_state_change;
    }

clean_state_change:
    if (deviceInfoSet != INVALID_HANDLE_VALUE) {
        SetupDiDestroyDeviceInfoList(deviceInfoSet);
    }
    return exit;
}

BOOL do_enable_device()
{
    return do_state_change(DICS_ENABLE);
}

BOOL do_disable_device()
{
    return do_state_change(DICS_DISABLE);
}

void print_help_header()
{
    std::wcout << L"WindowsPerf-DevGen"
        << L" ver. " << MAJOR << "." << MINOR << "." << PATCH
        << L" ("
        << WPERF_GIT_VER_STR
        << L"/"
#ifdef _DEBUG
        << L"Debug"
#else
        << L"Release"
#endif
        << L") WindowsPerf's Kernel Driver Installer."
        << std::endl;

    std::wcout << L"Report bugs to: https://gitlab.com/Linaro/WindowsPerf/windowsperf/-/issues"
        << std::endl;
}

void print_usage()
{
    const wchar_t* wsHelp = LR"(
NAME:
    wperf-devgen - Install WindowsPerf Kernel Driver

SYNOPSIS:
    wperf [--version] [--help] [OPTIONS]

OPTIONS:

    -h, /h, -?, /?, --help
        Display wperf-devgen help.

    --version
        Display version.

    install
        Install WindowsPerf Kernel Driver.

    uninstall
        Uninstall previously installed WindowsPerf Kernel Driver

    reinstall
        Acts as uninstall, followed by install.

    enable
        Enable previously installed WindowsPerf Kernel Driver

    disable
        Disable previously installed WindowsPerf Kernel Driver

    restart
        Acts as disable, followed by enable.
)";

    std::wcout << wsHelp;
}

enum class Command
{
    Invalid,

    Install,
    Uninstall,
    Reinstall,

    Enable,
    Disable,
    Restart,
};

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        print_help_header();
        print_usage();
        return -1;
    }
    else
    {
        // Simple command line parser with only one valid CLI argument
        std::string cli_arg = argv[1];
        if (cli_arg == "--help" || cli_arg == "-h" || cli_arg == "/h" || cli_arg == "-?" || cli_arg == "/?")
        {
            print_help_header();
            print_usage();
            return 0;
        }
        else if (cli_arg == "--version")
        {
            print_help_header();
            return 0;
        }
    }

    int errorCode = 0;

    GetFullInfPath();

    Command command = Command::Invalid;

    std::string user_command(argv[1]);
    std::cout << "Executing command: " << user_command << "." << std::endl;

    if ("install" == user_command)
    {
        std::cout << "Install requested." << std::endl;
        command = Command::Install;
    }
    else if ("uninstall" == user_command)
    {
        std::cout << "Uninstall requested." << std::endl;
        command = Command::Uninstall;
    }
    else if ("reinstall" == user_command)
    {
        std::cout << "Reinstall requested." << std::endl;
        command = Command::Reinstall;
    }
    else if ("enable" == user_command)
    {
        std::cout << "Enable requested." << std::endl;
        command = Command::Enable;
    }
    else if ("disable" == user_command)
    {
        std::cout << "Disable requested." << std::endl;
        command = Command::Disable;
    }
    else if ("restart" == user_command)
    {
        std::cout << "Restart requested." << std::endl;
        command = Command::Restart;
    }

    switch (command)
    {
    case Command::Invalid:
    default:
        std::cerr << "Unrecognized command " << user_command << "." << std::endl;
        errorCode = -1;
        goto clean_exit;

    case Command::Install:
        if (!do_install_device())
        {
            errorCode = GetLastError();
            goto clean_exit;
        }

        std::cout << "Device installed successfully" << std::endl;
        break;

    case Command::Uninstall:
        if (!do_remove_device())
        {
            errorCode = GetLastError();
            goto clean_exit;
        }
        std::cout << "Device uninstalled successfully" << std::endl;
        break;

    case Command::Reinstall:
        if (!do_remove_device())
        {
            errorCode = GetLastError();
            goto clean_exit;
        }
        std::cout << "Device uninstalled successfully" << std::endl;

        if (!do_install_device())
        {
            errorCode = GetLastError();
            goto clean_exit;
        }

        std::cout << "Device installed successfully" << std::endl;
        break;

    case Command::Enable:
        if (!do_enable_device())
        {
            errorCode = GetLastError();
            goto clean_exit;
        }
        std::cout << "Device enabled successfully" << std::endl;
        break;

    case Command::Disable:
        if (!do_disable_device())
        {
            errorCode = GetLastError();
            goto clean_exit;
        }
        std::cout << "Device disabled successfully" << std::endl;
        break;

    case Command::Restart:
        if (!do_disable_device())
        {
            errorCode = GetLastError();
            goto clean_exit;
        }
        std::cout << "Device disabled successfully" << std::endl;

        if (!do_enable_device())
        {
            errorCode = GetLastError();
            goto clean_exit;
        }
        std::cout << "Device enabled successfully" << std::endl;
        break;
    }

clean_exit:
    if (errorCode)
    {
        // GetLastError() can return error codes from multiple ranges: winerror.h, setupapi.h, and so on.
        // Small values are often referred to in decimal, while large ones are usually written in hex.
        // The largest system error code range ends with 15999, so switch over after that.
        if (static_cast<DWORD>(errorCode) >= 16000) {
            std::cout << std::hex << std::showbase << std::setw(8);
        }
        std::cout << "GetLastError " << errorCode << std::endl;
    }
    return errorCode;
}
