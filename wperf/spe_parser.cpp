// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "spe_parser.h"
#include <cassert>
#include <sstream>

using namespace SPEParser;

PacketType SPEParser::get_packet_type(UINT8 hdr)
{
    if (static_cast<PacketType>(hdr)    == PacketType::PADDING)        return PacketType::PADDING;
    if (static_cast<PacketType>(hdr)    == PacketType::END)            return PacketType::END;
    if (static_cast<PacketType>(hdr)    == PacketType::TIMESTAMP)      return PacketType::TIMESTAMP;
    if ((hdr & PacketType::MASK_MIDDLE) == PacketType::EVENTS)         return PacketType::EVENTS;
    if ((hdr & PacketType::MASK_MIDDLE) == PacketType::DATA_SOURCE)    return PacketType::DATA_SOURCE;
    if ((hdr & PacketType::MASK_LAST2)  == PacketType::CONTEXT)        return PacketType::CONTEXT;
    if ((hdr & PacketType::MASK_LAST2)  == PacketType::OPERATION_TYPE) return PacketType::OPERATION_TYPE;
    if ((hdr & PacketType::MASK_LAST2)  == PacketType::LONG_HEADER)    return PacketType::LONG_HEADER;
    if ((hdr & PacketType::MASK_LAST3)  == PacketType::ADDRESS)        return PacketType::ADDRESS;
    if ((hdr & PacketType::MASK_LAST3)  == PacketType::COUNTER)        return PacketType::COUNTER;

    return PacketType::UNKNOWN;
}

void Packet::add(UINT8 byte)
{
    if (m_state == ParsingState::READING_HEADER)
    {
        switch (PacketType type = get_packet_type(byte))
        {
        case PacketType::ADDRESS:
        case PacketType::COUNTER:
            if (m_bytes_read >= 1)
            {
                m_type = type == PacketType::ADDRESS ? PacketType::ADDRESS_LONG : PacketType::COUNTER_LONG;
                m_header1 = byte;
            }
            else {
                m_type = type;
                m_header0 = byte;
                m_bytes_to_read = 8;
            }
            m_bytes_to_read = type == PacketType::ADDRESS ? 8 : 2;
            m_state = ParsingState::READING_DATA;
            break;
        case PacketType::END:
            m_type = type;
            m_header0 = byte;
            m_state = ParsingState::FINISHED;
            m_bytes_to_read = 0;
            break;
        case PacketType::EVENTS:
        case PacketType::DATA_SOURCE:
            m_type = type;
            m_header0 = byte;
            m_state = ParsingState::READING_DATA;
            m_bytes_to_read = static_cast<size_t>(1) << ((m_header0 & (~PacketType::MASK_MIDDLE)) >> 4);
            break;
        case PacketType::OPERATION_TYPE:
            m_type = type;
            m_header0 = byte;
            m_state = ParsingState::READING_DATA;
            m_bytes_to_read = 1;
            break;
        case PacketType::CONTEXT:
            m_type = type;
            m_header0 = byte;
            m_state = ParsingState::READING_DATA;
            m_bytes_to_read = 4;
            break;
        case PacketType::TIMESTAMP:
            m_type = type;
            m_header0 = byte;
            m_state = ParsingState::READING_DATA;
            m_bytes_to_read = 8;
            break;
        case PacketType::LONG_HEADER: m_header0 = byte; break;
        }
    }
    else if (m_state == ParsingState::FINISHED) {
    }
    else {
        m_bytes_to_read--;
        m_payload |= (static_cast<uint64_t>(byte) << m_payload_index * 8);
        m_payload_index++;
        if (m_bytes_to_read == 0)
        {
            m_state = ParsingState::FINISHED;
        }
    }
    m_bytes_read++;
}

AddressPacket::AddressPacket(const Packet& p)
{
    m_payload = p.m_payload;
    m_type = p.m_type;
    m_header0 = p.m_header0;
    m_header1 = p.m_header1;
    m_address = m_payload & 0x00FFFFFFFFFFFF;

    if (m_type == PacketType::ADDRESS_LONG)
    {
        m_index = ((m_header0 & (~PacketType::MASK_LAST2)) << 3) |
                   (m_header1 & (~PacketType::MASK_LAST3));
    }
    else {
        m_index = m_header0 & (~PacketType::MASK_LAST3);
    }

    m_address_type = static_cast<AddressType>(m_index);
}

SecurityState AddressPacket::get_security_state() const
{
    bool ns = false, nse = false;
    switch (m_address_type)
    {
    case AddressType::INSTRUCTION_VIRTUAL:
    case AddressType::BRANCH_TARGET:
        ns =  m_payload & 0x80000000'00000000;
        nse = m_payload & 0x10000000'00000000;
        break;
    case AddressType::DATA_ACCESS_PHYSICAL:
        ns  = m_payload & 0x10000000'00000000;
        nse = m_payload & 0x80000000'00000000;
        break;
    default: assert(false);
    }
    return static_cast<SecurityState>((nse ? 2 : 0) | (ns ? 1 : 0));
}

UINT8 AddressPacket::get_exception_level() const
{
    assert(m_address_type == AddressType::INSTRUCTION_VIRTUAL || m_address_type == AddressType::BRANCH_TARGET);
    return (m_payload >> 61) & 0b11;
}

UINT8 AddressPacket::get_tag() const
{
    assert(m_address_type == AddressType::DATA_ACCESS_VIRTUAL);
    return m_payload >> 56;
}

bool AddressPacket::get_tag_checked() const
{
    assert(m_address_type == AddressType::DATA_ACCESS_PHYSICAL);
    return m_payload & 0x40000000'00000000;
}

UINT8 AddressPacket::get_physical_address_tag() const
{
    assert(m_address_type == AddressType::DATA_ACCESS_PHYSICAL);
    return (m_payload >> 56) & 0b1111;
}

CounterPacket::CounterPacket(const Packet& p)
    : Packet(p)
{
    assert(p.m_type == PacketType::COUNTER || p.m_type == PacketType::COUNTER_LONG);
    assert(p.m_state == ParsingState::FINISHED);
}

UINT8 CounterPacket::get_index() const
{
    if (m_type == PacketType::COUNTER_LONG)
        return (m_header0 & ~PacketType::MASK_LAST2) << 3 |
                m_header1 & ~PacketType::MASK_LAST3;
    else
        return m_header0 & ~PacketType::MASK_LAST3;
}

UINT16 CounterPacket::get_count() const
{
    return m_payload & 0xFFFF;
}

CounterType CounterPacket::get_counter_type() const
{
    return static_cast<CounterType>(get_index());
}

EventsPacket::EventsPacket(const Packet& p)
    : Packet(p)
{
}

int EventsPacket::get_size() const
{
    return 1 << ((m_header0 & ~PacketType::MASK_MIDDLE) >> 4);
}

std::wstring EventsPacket::get_event_desc() const
{
    EventBit eb;
    eb.value = m_payload;
    std::wstringstream s;
    int size = get_size();

    // byte 0
    if (eb.bitFields.generated_exception) s << L"generated-exception+";
    if (eb.bitFields.architecturally_retired) s << L"architecturally-retired+";
    if (eb.bitFields.level1_data_cache_access) s << L"level1-data-cache-access+";
    if (eb.bitFields.level1_data_cache_refill_or_miss) s << L"level1-data-cache-refill-or-miss+";
    if (eb.bitFields.tlb_access) s << L"tlb-access+";
    if (eb.bitFields.tlb_walk) s << L"tlb-walk+";
    if (eb.bitFields.not_taken) s << L"not-taken+";
    if (eb.bitFields.mispredicted) s << L"mispredicted+";
    if (size == 1) goto end;

    // byte 1
    if (eb.bitFields.last_level_cache_access) s << L"last-level-cache-access+";
    if (eb.bitFields.last_level_cache_miss) s << L"last-level-cache-miss+";
    if (eb.bitFields.remote_access) s << L"remote-access+";
    if (eb.bitFields.misalignment) s << L"misalignment+";
    if (eb.bitFields.implementation_defined_events_12_to_15 != 0)
        for (int i = 12; i <= 15; i++)
            if (eb.bitFields.implementation_defined_events_12_to_15 & (1 << (i - 12)))
                s << L"implementation-defined-event-" << i << '+';
    if (size == 2) goto end;

    // byte 2
    if (eb.bitFields.transactional) s << L"transactional+";
    if (eb.bitFields.partial_or_empty_predicate) s << L"partial-or-empty-predicate+";
    if (eb.bitFields.empty_predicate) s << L"empty-predicate+";
    if (eb.bitFields.level_2_data_cache_access) s << L"level-2-data-cache-access+";
    if (eb.bitFields.level_2_data_cache_miss) s << L"level-2-data-cache-miss+";
    if (eb.bitFields.cache_data_modified) s << L"cache-data-modified+";
    if (eb.bitFields.recently_fetched) s << L"recently-fetched+";
    if (eb.bitFields.data_snooped) s << L"data-snooped+";

    // byte 3
    if (eb.bitFields.streaming_sve_mode) s << L"streaming-sve-mode+";
    if (eb.bitFields.smcu_or_external_coprocessor_operation) s << L"smcu-or-external-coprocessor-operation+";
    if (eb.bitFields.implementation_defined_events_26_to_31 != 0)
        for (int i = 26; i <= 31; i++)
            if (eb.bitFields.implementation_defined_events_26_to_31 & (1 << (i - 26)))
                s << L"implementation-defined-event-" << i << '+';
    if (size == 4) goto end;

    // bytes 4 and 5 are unused

    // bytes 6 and 7
    if (eb.bitFields.implementation_defined_events_48_to_63 != 0)
        for (int i = 48; i <= 63; i++)
            if (eb.bitFields.implementation_defined_events_48_to_63 & (1 << (i - 48)))
                s << L"implementation-defined-event-" << i << '+';

end:
    auto desc = std::move(s).str();
    if (!desc.empty()) // Remove trailing +, if there is one
        desc.erase(desc.end() - 1);
    return desc;
}

OperationTypePacket::OperationTypePacket(const Packet& p)
	: Packet(p)
{
    m_class = static_cast<OperationTypeClass>(m_header0 & (~PacketType::MASK_LAST2));
    m_subclass = m_payload & 0xFF;
}

/// <summary>
/// Gets a clean version of the subclass field, with all variable bits set to 0.
/// </summary>
OperationTypeSubclass OperationTypePacket::get_subclass_category() const
{
    switch (m_class)
    {
    case OperationTypeClass::OTHER:
        return m_payload & OperationTypeSubclass::OTHER_MASK;
    case OperationTypeClass::LOAD_STORE_ATOMIC:
        // Try the default mask first
        switch (auto subclass = m_payload & OperationTypeSubclass::LOAD_STORE_ATOMIC_DEFAULT_MASK)
        {
        case OperationTypeSubclass::LOAD_STORE_GENERAL_PURPOSE:
        case OperationTypeSubclass::LOAD_STORE_SIMD_FP:
        case OperationTypeSubclass::LOAD_STORE_UNSPECIFIED:
        case OperationTypeSubclass::LOAD_STORE_ALLOCATION_TAG:
        case OperationTypeSubclass::LOAD_STORE_SYSTEM_REGISTER:
        case OperationTypeSubclass::LOAD_STORE_MEMORY_COPY:
            return subclass;
        // Fall back to the custom masks
        default:
            if ((m_payload & OperationTypeSubclass::LOAD_STORE_EXTENDED_MASK) == OperationTypeSubclass::LOAD_STORE_EXTENDED)
                return OperationTypeSubclass::LOAD_STORE_EXTENDED;
            if ((m_payload & OperationTypeSubclass::LOAD_STORE_SVE_OR_SME_MASK) == OperationTypeSubclass::LOAD_STORE_SVE_OR_SME)
                return OperationTypeSubclass::LOAD_STORE_SVE_OR_SME;
            if ((m_payload & OperationTypeSubclass::LOAD_STORE_GCS_MASK) == OperationTypeSubclass::LOAD_STORE_GCS)
                return OperationTypeSubclass::LOAD_STORE_GCS;
            if ((m_payload & OperationTypeSubclass::LOAD_STORE_MEMORY_SET_MASK) == OperationTypeSubclass::LOAD_STORE_MEMORY_SET)
                return OperationTypeSubclass::LOAD_STORE_MEMORY_SET;
            break;
        }
        break;
    case OperationTypeClass::BRANCH_OR_EXCEPTION:
        return OperationTypeSubclass::BRANCH_OR_EXCEPTION;
    }
    assert(false);
    return static_cast<OperationTypeSubclass>(0xFF);
}

std::wstring OperationTypePacket::get_subclass() const
{
    // This may only be interpreted according to the class, there are overlapping values
    auto subclass = get_subclass_category();

    switch (m_class)
    {
    case OperationTypeClass::OTHER:
        switch (subclass)
        {
        case OperationTypeSubclass::OTHER:                      return L"";
        case OperationTypeSubclass::DATA_PROCESSING_SVE_VECTOR: return L"-SVE";
        case OperationTypeSubclass::DATA_PROCESSING_SME_ARRAY:  return L"-SME";
        }
        break;
    case OperationTypeClass::LOAD_STORE_ATOMIC:
    {
        std::wstring ldst = (m_payload & 1) ? L"STORE-" : L"LOAD-";
        switch (subclass)
        {
        case OperationTypeSubclass::LOAD_STORE_GENERAL_PURPOSE: return ldst + L"GP";
        case OperationTypeSubclass::LOAD_STORE_SIMD_FP:         return ldst + L"SIMD-FP";
        case OperationTypeSubclass::LOAD_STORE_UNSPECIFIED:     return ldst + L"UNSPECIFIED";
        case OperationTypeSubclass::LOAD_STORE_ALLOCATION_TAG:  return ldst + L"TAG";
        case OperationTypeSubclass::LOAD_STORE_SYSTEM_REGISTER: return ldst + L"SYSREG";
        case OperationTypeSubclass::LOAD_STORE_MEMORY_COPY:     return ldst + L"MEMCPY";
        case OperationTypeSubclass::LOAD_STORE_EXTENDED:        return ldst + L"EXT";
        case OperationTypeSubclass::LOAD_STORE_SVE_OR_SME:      return ldst + L"SVE-SME";
        case OperationTypeSubclass::LOAD_STORE_GCS:             return ldst + L"GCS";
        case OperationTypeSubclass::LOAD_STORE_MEMORY_SET:      return ldst + L"MEMSET";
        }
        break;
    }
    case OperationTypeClass::BRANCH_OR_EXCEPTION:
        // There's only one subclass, OperationTypeSubclass::BRANCH_OR_EXCEPTION
        return std::wstring(((m_payload & 1) ? L"CONDITIONAL-" : L"UNCONDITIONAL-")) + std::wstring(((m_payload & 2) ? L"INDIRECT" : L"DIRECT"));
    }
    return L"";
}

std::wstring OperationTypePacket::get_class() const
{
    switch (m_class)
    {
    case OperationTypeClass::OTHER:                return L"OTHER";
    case OperationTypeClass::LOAD_STORE_ATOMIC:    return L"LOAD_STORE_ATOMIC";
    case OperationTypeClass::BRANCH_OR_EXCEPTION:  return L"BRANCH_OR_EXCEPTION";
    }
    return L"";
}

std::wstring OperationTypePacket::get_desc() const
{
    return get_class() + ((m_class != OperationTypeClass::OTHER) ? L"-" : L"") + get_subclass();
}

void Record::parse_record_data()
{
    for (const auto& packet : m_packets)
    {
        switch (packet.m_type)
        {
        case PacketType::ADDRESS:
        case PacketType::ADDRESS_LONG:
        {
            AddressPacket ap(packet);
            if (ap.m_address_type == AddressType::INSTRUCTION_VIRTUAL)
            {
                m_pc = ap.m_address;
            }
            break;
        }
        case PacketType::OPERATION_TYPE:
        {
            OperationTypePacket op(packet);
            m_optype_name = op.get_desc();
            break;
        }
        case PacketType::EVENTS:
        {
            EventsPacket ep(packet);
            m_event_name = ep.get_event_desc();
            break;
        }
        }
    }
    m_full_desc = m_optype_name + L"/" + m_event_name;
}

std::vector<std::pair<std::wstring, UINT64>> SPEParser::read_spe_buffer(const std::vector<UINT8>& buffer)
{
    std::vector<std::pair<std::wstring, UINT64>> records;
    std::vector<Packet> packets;
    packets.emplace_back();
    for (const UINT8 byte : buffer)
    {
        Packet& last = packets.back();
        last.add(byte);
        if (last.m_state == ParsingState::FINISHED)
        {
            if (last.m_type == PacketType::END || last.m_type == PacketType::TIMESTAMP)
            {
                Record rec;
                rec.m_packets = packets;
                rec.parse_record_data();
                records.emplace_back(rec.m_full_desc, rec.m_pc);
                packets.clear();
            }
            packets.emplace_back();
        }
    }
    return records;
}
