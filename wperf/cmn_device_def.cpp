// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <map>
#include <string>
#include "cmn_device.h"


void cmn_device::init_mesh_family_name()
{
    m_mesh_family_name[cmn_mesh_family::CMN_FAMILY_UNKNOWN] = L"CMN_FAMILY_UNKNOWN";

#define WPERF_TS_CMN(CMN)               m_mesh_family_name[cmn_mesh_family::CMN] = L#CMN;
#define WPERF_TS_CMN_DEVICE(...)
#define WPERF_TS_CMN_EVENTS(...)
#include "wperf-common/cmn700.def"
// #include "wperf-common/cmns3.def"		// Example new CMN mesh family
#undef WPERF_TS_CMN
#undef WPERF_TS_CMN_DEVICE
#undef WPERF_TS_CMN_EVENTS
}

void cmn_device::init_mesh_device_to_id()
{
    // WPERF_TS_CMN_DEVICE(CMN700_FAMILY, DN, 1, "Telemetry Specification (PMU Events, Metrics and Methodology) for DN CMN")
    // std::map<cmn_mesh_family, std::map<std::wstring, cmn_mesh_device_ids>> cmn_device::m_mesh_device_to_id;
#define WPERF_TS_CMN(...)
#define WPERF_TS_CMN_DEVICE(A,B,C,D)		m_mesh_device_to_id[cmn_mesh_family::A][std::wstring(L#B)] = cmn_mesh_device_ids::A##_##B;
#define WPERF_TS_CMN_EVENTS(...)
#include "wperf-common/cmn700.def"
    // #include "wperf-common/cmns3.def"		// Example new CMN mesh family
#undef WPERF_TS_CMN
#undef WPERF_TS_CMN_DEVICE
#undef WPERF_TS_CMN_EVENTS
}

void cmn_device::init_mesh_mesh_devices_desc()
{
    // WPERF_TS_CMN_DEVICE(CMN700_FAMILY, DN, 1, "Telemetry Specification (PMU Events, Metrics and Methodology) for DN CMN")
    //std::map<cmn_mesh_family, std::map<cmn_mesh_device_ids, cmn_device_description>> cmn_device::m_mesh_devices_desc = {
#define WPERF_TS_CMN(...)
#define WPERF_TS_CMN_DEVICE(A,B,C,D)		m_mesh_devices_desc[cmn_mesh_family::A][cmn_mesh_device_ids::A##_##B] = \
    { cmn_mesh_family::A, std::wstring(L#B), cmn_mesh_device_ids::A##_##B, std::wstring(L##D)};
#define WPERF_TS_CMN_EVENTS(...)
#include "wperf-common/cmn700.def"
//#include "wperf-common/cmns3.def"		// Example new CMN mesh family
#undef WPERF_TS_CMN
#undef WPERF_TS_CMN_DEVICE
#undef WPERF_TS_CMN_EVENTS
}

void cmn_device::init_mesh_mesh_devices_event_desc()
{
    // WPERF_TS_CMN_EVENTS(CMN700_FAMILY, DN, PMU_DN_TLBI_COUNT, 0x0001, "pmu_dn_tlbi_count", "Number of TLBI DVM op requests", "Indicates an incoming TLBI DVMOp Request was received")
    // std::map<cmn_mesh_family, std::map<cmn_mesh_device_ids, std::map<cmn_mesh_event_code, cmd_device_event_description>>> m_mesh_devices_event_desc
#define EVENT_NAME_SHORT(S)     cmn_device::set_cmn_event_name_short(S) // "PMU_HNF_CACHE_MISS_EVENT" -> "HNF_CACHE_MISS"
#define WPERF_TS_CMN(...)
#define WPERF_TS_CMN_DEVICE(...)
#define WPERF_TS_CMN_EVENTS(A,B,C,D,E,F,G)	m_mesh_devices_event_desc[cmn_mesh_family::A][cmn_mesh_device_ids::A##_##B][cmn_mesh_event_code::A##_##B##_##C] = \
    { cmn_mesh_family::A, cmn_mesh_device_ids::A##_##B, std::wstring(L##E), EVENT_NAME_SHORT(std::wstring(L##E)), cmn_mesh_event_code::A##_##B##_##C, std::wstring(L##F), std::wstring(L##G) };
#include "wperf-common/cmn700.def"
// #include "wperf-common/cmns3.def"		// Example new CMN mesh family
#undef WPERF_TS_CMN
#undef WPERF_TS_CMN_DEVICE
#undef WPERF_TS_CMN_EVENTS
#undef EVENT_NAME_SHORT
}
