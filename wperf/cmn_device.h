// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <map>
#include <string>
#include <vector>
#include "utils.h"


//
// Available CMN mesh(es) enum
// Place here all CMN mesh related files, for example: cmn700.def or cmns3.def
//
enum class cmn_mesh_family : uint16_t {
#define WPERF_TS_CMN(CMN) CMN##,            // E.g. CMN700_FAMILY,
#define WPERF_TS_CMN_DEVICE(...)
#define WPERF_TS_CMN_EVENTS(...)
#include "wperf-common/cmn700.def"
// #include "wperf-common/cmns3.def"		// Example new CMN mesh family
#undef WPERF_TS_CMN
#undef WPERF_TS_CMN_DEVICE
#undef WPERF_TS_CMN_EVENTS
    CMN_FAMILY_UNKNOWN
};

//
// CMN mesh family device IDs, squash <mesh_family> and <device_id>, for example:
//
//  CMN700_FAMILY_DN = 1,
//  CMN700_FAMILY_HNF = 5,
//  ...
//  CMN700_FAMILY_XP = 6
//
enum class cmn_mesh_device_ids : uint16_t {

#define WPERF_TS_CMN(...)
#define WPERF_TS_CMN_DEVICE(A,B,C,D)    A##_##B = C,       // E.g. CMN700_FAMILY_DN = 1,
#define WPERF_TS_CMN_EVENTS(...)
#include "wperf-common/cmn700.def"
// #include "wperf-common/cmns3.def"		// Example new CMN mesh family
#undef WPERF_TS_CMN
#undef WPERF_TS_CMN_DEVICE
#undef WPERF_TS_CMN_EVENTS
    CMN_DEVICE_ID_UNKNOWN = 0xFFFF
};

//
// CMN mesh family -> device ID -> event code, squash <mesh_family> and <device_id>, and <event code> for example:
//
// CMN700_FAMILY_DN_PMU_DN_TLBI_COUNT = 0x0001,
// CMN700_FAMILY_DN_PMU_DN_BPI_COUNT = 0x0002,
// CMN700_FAMILY_DN_PMU_DN_PICI_COUNT = 0x0003,
//
enum class cmn_mesh_event_code : uint16_t {

#define WPERF_TS_CMN(...)
#define WPERF_TS_CMN_DEVICE(...)
#define WPERF_TS_CMN_EVENTS(A,B,C,D,E,F,G)    A##_##B##_##C = D,       // E.g. CMN700_FAMILY_PMU_DN_TLBI_COUNT = 0x0001,
#include "wperf-common/cmn700.def"
    // #include "wperf-common/cmns3.def"		// Example new CMN mesh family
#undef WPERF_TS_CMN
#undef WPERF_TS_CMN_DEVICE
#undef WPERF_TS_CMN_EVENTS
    CMN_EVENT_CODE_UNKNOWN = 0xFFFF
};


struct cmn_device_description
{
    cmn_mesh_family mesh_family{ cmn_mesh_family::CMN_FAMILY_UNKNOWN }; // E.g. CMN700_FAMILY
    std::wstring name;  // E.g. "XP"
    cmn_mesh_device_ids index{ cmn_mesh_device_ids::CMN_DEVICE_ID_UNKNOWN };    // E.g. 6 (for "XP")
    std::wstring desc;  // Verbose description, e.g. "Telemetry Specification (PMU Events, Metrics and Methodology) for XP CMN"
};

struct cmd_device_event_description
{
    cmn_mesh_family mesh_family{ cmn_mesh_family::CMN_FAMILY_UNKNOWN }; // E.g. CMN700_FAMILY
    cmn_mesh_device_ids device_index{ cmn_mesh_device_ids::CMN_DEVICE_ID_UNKNOWN }; // E.g. `1` for "DN"
    std::wstring name;          // E.g. "pmu_dn_tlbi_count"
    std::wstring name_short;    // E.g. "pmu_hnf_cache_miss_event" -> short name "HNF_CACHE_MISS"   (no prefix: "PMU_" and/or no suffix"_EVENT")
    cmn_mesh_event_code code{ cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN };    // (also called event "code") E.g. 0x0001
    std::wstring title; // Event title e.g. "Number of TLBI DVM op requests"
    std::wstring desc;  // Verbose description, e.g. "Indicates an incoming TLBI DVMOp Request was received"
};


class cmn_device
{
    cmn_mesh_family m_mesh_family = cmn_mesh_family::CMN_FAMILY_UNKNOWN;
    void init_mesh_family_name();
    void init_mesh_device_to_id();
    void init_mesh_mesh_devices_desc();
    void init_mesh_mesh_devices_event_desc();

    //
    // This will be always populated for all available CMN mesh families and its devices, events.
    //
    std::map<cmn_mesh_family, std::map<std::wstring, cmn_mesh_device_ids>> m_mesh_device_to_id;             // e.g. [CMN700_FAMILY] -> { map [DN] -> [device_id] }
    std::map<cmn_mesh_family, std::map<cmn_mesh_device_ids, cmn_device_description>> m_mesh_devices_desc;   // e.g. [CMN700_FAMILY] -> { map [device_id] -> `cmn_device_description` }
    std::map<cmn_mesh_family, std::map<cmn_mesh_device_ids, std::map<cmn_mesh_event_code, cmd_device_event_description>>> m_mesh_devices_event_desc;   // e.g. [CMN700_FAMILY] -> { map [device_id] -> `[event_code] -> cmd_device_event_description` }
    std::map<cmn_mesh_family, std::wstring> m_mesh_family_name;             // e.g. [CMN700_FAMILY] -> "CMN700_FAMILY"

public:
    cmn_device(cmn_mesh_family mesh_family) : m_mesh_family(mesh_family) {
        init_mesh_family_name();
        init_mesh_device_to_id();
        init_mesh_mesh_devices_desc();
        init_mesh_mesh_devices_event_desc();
    }

    //
    // CMN mesh family interface when we initialize with `m_mesh_family`
    //

    // Predicates return `true` / `false`
    bool is_device_id(const cmn_mesh_device_ids _device_id) const;
    bool is_device_event_code(const cmn_mesh_device_ids _device_id, const cmn_mesh_event_code _event_code) const;

    // Extractors
    cmn_mesh_family get_mesh_familty() const { return m_mesh_family; }
    cmn_mesh_device_ids get_mesh_device_id(const std::wstring& _device_name) const;
    bool get_device_description(const cmn_mesh_device_ids _device_id, cmn_device_description& _device_desc) const;
    cmn_mesh_event_code get_mesh_device_event_code(const cmn_mesh_device_ids _device_id, const std::wstring& _event_nam) const;
    std::wstring get_cmn_version_name(const cmn_mesh_family _mesh_family) const;

    std::vector<std::pair<cmn_mesh_device_ids, std::wstring>> get_cmn_device_name_list(cmn_mesh_family _mesh_family) const;
    std::vector<const cmd_device_event_description*> get_cmn_device_event_list(cmn_mesh_device_ids _device_id, cmn_mesh_family _mesh_family) const;

    // Manipulators
    static std::wstring set_cmn_event_name_short(std::wstring _event_code);
};

