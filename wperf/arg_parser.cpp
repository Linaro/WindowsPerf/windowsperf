// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <iostream>
#include <codecvt>
#include <locale>
#include <cwchar>
#include <vector>
#include <sstream>
#include "arg_parser.h"
#include "output.h"
#include "exception.h"
#include "wperf-common/public.h"
#include "wperf-common/gitver.h"

namespace ArgParser {

#define WINDOWSPERF_ARGS_OPT(A,B,C,D,E,F)  arg_parser_arg_opt A(B,C,D,E,F);
#define WINDOWSPERF_ARGS_POS(A,B,C,D,E,F,G)  arg_parser_arg_pos A(B,C,D,E,F,G);
#define WINDOWSPERF_ARG_COMMAND(A,B,C,D,E,F,G,H,I) arg_parser_arg_command A(B,C,D,E,COMMAND_CLASS::##F,G,H,I);
#include "wperf-common/wperf-cli-args.def"
#undef WINDOWSPERF_ARGS_OPT
#undef WINDOWSPERF_ARGS_POS
#undef WINDOWSPERF_ARG_COMMAND


std::vector<arg_parser_arg_command*> commands_list = {
#define WINDOWSPERF_ARGS_OPT(...)
#define WINDOWSPERF_ARG_COMMAND(A,B,C,D,E,F,G,H,I) &A,
#define WINDOWSPERF_ARGS_POS(...)
#include "wperf-common/wperf-cli-args.def"
#undef WINDOWSPERF_ARG_COMMAND
#undef WINDOWSPERF_ARGS_OPT
#undef WINDOWSPERF_ARGS_POS
};

std::vector<arg_parser_arg*> flags_list = {
#define WINDOWSPERF_ARG_COMMAND(...)
#define WINDOWSPERF_ARGS_OPT(A,B,C,D,E,F) &A,
#define WINDOWSPERF_ARGS_POS(A,B,C,D,E,F,G) &A,
#include "wperf-common/wperf-cli-args.def"
#undef WINDOWSPERF_ARG_COMMAND
#undef WINDOWSPERF_ARGS_OPT
#undef WINDOWSPERF_ARGS_POS
};
COMMAND_CLASS selected_command = COMMAND_CLASS::NO_COMMAND;

    void arg_parser::parse(
        _In_ const int argc,
        _In_reads_(argc) const wchar_t* argv[]
    )
    {
        wstr_vec raw_args;
        for (int i = 1; i < argc; i++)
        {
            raw_args.push_back(argv[i]);
            m_arg_array.push_back(argv[i]);
        }

        if (raw_args.size() == 0)
        {
            print_help_header();
            print_help_prompt();
            return;
        }

        try
        {
            for (auto& command : commands_list)
            {
                if (command->parse(raw_args)) {
                    selected_command = command->m_command;
                    raw_args.erase(raw_args.begin(), raw_args.begin() + command->get_arg_count() + 1);

                    break;
                }
            }
        }
        catch (const std::exception& err)
        {
            throw_invalid_arg(raw_args.front(), L"Error: " + WideStringFromMultiByte(err.what()));
        }

        if (selected_command == COMMAND_CLASS::NO_COMMAND) {
            throw_invalid_arg(raw_args.front(), L"warning: command not recognized!");
        }
        if (selected_command == COMMAND_CLASS::HELP) {
            return;
        }

        while (raw_args.size() > 0)
        {
            size_t initial_size = raw_args.size();

            for (auto& current_flag : flags_list)
            {
                try
                {
                    if (current_flag->parse(raw_args)) {
                        raw_args.erase(raw_args.begin(), raw_args.begin() + current_flag->get_arg_count() + 1);
						parsed_args.push_back(current_flag);
                    }
                }
                catch (const std::exception& err)
                {
                    throw_invalid_arg(raw_args.front(), L"Error: " + std::wstring(err.what(), err.what() + std::strlen(err.what())));
                }
            }

            // if going over all the known arguments doesn't affect the size of the raw_args, then the command is unkown
            if (initial_size == raw_args.size())
            {
                throw_invalid_arg(raw_args.front(), L"Error: Unrecognized command");
            }

        }
    }

    void arg_parser::print_help_header() const
    {
        m_out.GetOutputStream() << L"WindowsPerf"
            << L" ver. " << MAJOR << "." << MINOR << "." << PATCH
            << L" ("
            << WPERF_GIT_VER_STR
            << L"/"
#ifdef _DEBUG
            << L"Debug"
#else
            << L"Release"
#endif
            << ENABLE_FEAT_STR
            << L") WOA profiling with performance counters."
            << std::endl;
    
        m_out.GetOutputStream() << L"Report bugs to: https://gitlab.com/Linaro/WindowsPerf/windowsperf/-/issues"
            << std::endl;
    }
    void arg_parser::print_help_prompt() const
    {
        m_out.GetOutputStream() << L"Use --help for help." << std::endl;
    }
    void arg_parser::print_help() const
    {
        print_help_header();
        m_out.GetOutputStream() << L"NAME:\n"
            << L"\twperf - Performance analysis tools for Windows on Arm\n\n"
            << L"\tUsage: wperf <command> [options]\n\n"
            << L"SYNOPSIS:\n";
        for (auto& command : commands_list)
        {
            m_out.GetOutputStream() << L"\t" << command->get_all_flags_string() << L"\n" << command->get_usage_text() << L"\n";
        }

        m_out.GetOutputStream() << L"OPTIONS:\n";
        for (auto& flag : flags_list)
        {
            m_out.GetOutputStream() << flag->get_help() << L"\n\n";
        }
        m_out.GetOutputStream() << L"EXAMPLES:\n";
        for (auto& command : commands_list)
        {
            if (command->get_examples().empty()) continue;
            m_out.GetOutputStream() << command->get_examples() << L"\n\n";
        }
    }

    void arg_parser::throw_invalid_arg(const std::wstring& arg, const std::wstring& additional_message) const
    {
        std::wstring command = L"wperf ";
		command += WStringJoin(m_arg_array, L" ");

        std::size_t pos = command.find(arg);
        if (pos == 0 || pos == std::wstring::npos) {
            pos = command.length();
        }

        std::wstring indicator(pos, ERROR_INDICATOR_PADDING);
        indicator += ERROR_INDICATOR_CARET;

        std::wostringstream error_message;
        error_message << L"Invalid argument detected:\n"
            << command << L"\n"
            << indicator << L"\n";
        if (!additional_message.empty()) {
            error_message << additional_message << L"\n";
        }
        m_out.GetErrorOutputStream() << error_message.str();
        throw fatal_exception("INVALID_ARGUMENT");
    }


    std::wstring arg_parser_arg_command::get_usage_text() const
    {
        return arg_parser_add_wstring_behind_multiline_text(arg_parser_format_string_to_length(
            m_useage_text + L"\n" + m_description), L"\t   ") + L"\n";
    }

    std::wstring arg_parser_arg_command::get_examples() const
    {
        std::wstring example_output;
        for (auto& example : m_examples) {
            example_output += example + L"\n\n";
        }
        return arg_parser_add_wstring_behind_multiline_text(arg_parser_format_string_to_length(example_output), L"\t");
    }
}
