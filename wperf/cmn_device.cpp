// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include <map>
#include <string>
#include "utils.h"
#include "cmn_device.h"


//
// API implementation (predicates)
//

/**
 * @brief Checks if the given device ID exists within the mesh devices description.
 *
 * This function verifies whether the specified device ID is present in the
 * mesh devices description for the current mesh family.
 *
 * @param _device_id The device ID to check.
 * @return true if the device ID exists, false otherwise.
 */
bool cmn_device::is_device_id(const cmn_mesh_device_ids _device_id) const
{
    if (m_mesh_devices_desc.count(m_mesh_family) == 0)
        return false;

    return m_mesh_devices_desc.at(m_mesh_family).count(_device_id);
};

/**
 * @brief Checks if the given event code exists for a specific device ID within the mesh devices event description.
 *
 * This function verifies whether the specified event code is present for the given device ID
 * in the mesh devices event description for the current mesh family.
 *
 * @param _device_id The device ID to check.
 * @param _event_code The event code to check.
 * @return true if the event code exists for the device ID, false otherwise.
 */
bool cmn_device::is_device_event_code(const cmn_mesh_device_ids _device_id, const cmn_mesh_event_code _event_code) const
{
    if (m_mesh_devices_event_desc.count(m_mesh_family) == 0)
        return false;

    if (m_mesh_devices_event_desc.at(m_mesh_family).count(_device_id) == 0)
        return false;

    return m_mesh_devices_event_desc.at(m_mesh_family).at(_device_id).count(_event_code);
};

//
// CMN API implementation (extractors)
//

/**
 * @brief Retrieves the device ID for a given device name within the mesh devices description.
 *
 * This function searches for the specified device name in the mesh devices description for the
 * current mesh family and returns the corresponding device ID. If the device name is not found,
 * it returns CMN_DEVICE_ID_UNKNOWN.
 *
 * @param _device_name The name of the device to search for.
 * @return The device ID corresponding to the device name, or CMN_DEVICE_ID_UNKNOWN if not found.
 */
cmn_mesh_device_ids cmn_device::get_mesh_device_id(const std::wstring& _device_name) const
{
    auto result = cmn_mesh_device_ids::CMN_DEVICE_ID_UNKNOWN;
    if (m_mesh_device_to_id.count(m_mesh_family))
    {
        for (const auto& [device_name, device_id] : m_mesh_device_to_id.at(m_mesh_family))
            if (CaseInsensitiveWStringComparision(device_name, _device_name))
                return device_id;
    }

    return result;
}

/**
 * @brief Retrieves the description for a specific device ID within the mesh devices description.
 *
 * This function attempts to retrieve the description for the specified device ID
 * from the mesh devices description for the current mesh family.
 *
 * @param _device_id The device ID for which the description is to be retrieved.
 * @param _device_desc A reference to a cmn_device_description object where the description will be stored.
 * @return true if the description is successfully retrieved, false otherwise.
 */
bool cmn_device::get_device_description(const cmn_mesh_device_ids _device_id, cmn_device_description& _device_desc) const
{
    if (m_mesh_devices_desc.count(m_mesh_family) &&
        m_mesh_devices_desc.at(m_mesh_family).count(_device_id))
    {
        _device_desc = m_mesh_devices_desc.at(m_mesh_family).at(_device_id);
        return true;
    }

    return false;
}

/**
 * @brief Retrieves the event code for a specific event name associated with a given device ID.
 *
 * This function searches for the specified event name within the event descriptions
 * for the given device ID in the mesh devices event description for the current mesh family.
 *
 * @param _device_id The device ID for which the event code is to be retrieved.
 * @param _event_name The name of the event to search for.
 * @return The event code if the event name is found, CMN_EVENT_CODE_UNKNOWN otherwise.
 */
cmn_mesh_event_code cmn_device::get_mesh_device_event_code(const cmn_mesh_device_ids _device_id, const std::wstring& _event_name) const
{
    if (m_mesh_devices_event_desc.count(m_mesh_family) &&
        m_mesh_devices_event_desc.at(m_mesh_family).count(_device_id))
    {
        for (const auto& [_, event_desc] : m_mesh_devices_event_desc.at(m_mesh_family).at(_device_id))
        {
            if (CaseInsensitiveWStringComparision(event_desc.name, _event_name))
                return event_desc.code;  // event_desc.event_index == event_index (also called "code")

            if (CaseInsensitiveWStringComparision(event_desc.name_short, _event_name))
                return event_desc.code;  // event_desc.event_index == event_index (also called "code")
        }
    }

    return cmn_mesh_event_code::CMN_EVENT_CODE_UNKNOWN;
}

/// <summary>
/// Retrieves the version name of the given CMN mesh family.
/// </summary>
/// <param name="_mesh_family">The CMN mesh family for which the version name is requested.</param>
/// <returns>
/// A wide string representing the name of the specified CMN mesh family.
/// If the provided mesh family is not found, it returns the name associated with CMN_FAMILY_UNKNOWN.
/// </returns>
std::wstring cmn_device::get_cmn_version_name(const cmn_mesh_family _mesh_family) const
{
    if (m_mesh_family_name.count(_mesh_family))
        return m_mesh_family_name.at(_mesh_family);

    return m_mesh_family_name.at(cmn_mesh_family::CMN_FAMILY_UNKNOWN);
}

/// @brief Removes the "pmu_" prefix and "_event" suffix from the given event code, if they exist.
/// @param _event_code The input event code as a std::wstring.
/// @return The modified event name with the prefix and suffix removed if they were present.
/// @note The function assumes the input event code may be in mixed case but normalizes it 
///       using WStringToLower() before checking for the prefix and suffix.
/// @example 
///     std::wstring result = set_cmn_event_name_short(L"pmu_test_event");
///     // result = L"test"
std::wstring cmn_device::set_cmn_event_name_short(std::wstring _event_code)
{
    const std::wstring prefix = L"pmu_";    // Keep lowercase!
    const std::wstring suffix = L"_event";  // Keep lowercase!

    if (WStringToLower(_event_code).starts_with(prefix))
        _event_code.erase(0, prefix.size());

    // Remove suffix if it exists
    if (WStringToLower(_event_code).ends_with(suffix))
        _event_code.erase(_event_code.size() - suffix.size());

    return _event_code;
}

/// <summary>
/// Retrieves a list of common device names for a given mesh family.
/// </summary>
/// <param name="_mesh_family">The mesh family for which device names are to be retrieved.</param>
/// <returns>
/// A vector of pairs, where each pair consists of a device ID (`cmn_mesh_device_ids`)
/// and the corresponding device name (`std::wstring`).
/// </returns>
std::vector<std::pair<cmn_mesh_device_ids, std::wstring>> cmn_device::get_cmn_device_name_list(cmn_mesh_family _mesh_family) const
{
    std::vector<std::pair<cmn_mesh_device_ids, std::wstring>> result;

    if (m_mesh_devices_desc.count(_mesh_family))
        for (const auto& [dev_id, dev_desc] : m_mesh_devices_desc.at(_mesh_family))
            result.push_back(std::make_pair(dev_id, dev_desc.name));

    return result;
}

/// <summary>
/// Retrieves a list of event descriptions for a specified device within a given mesh family.
/// </summary>
/// <param name="_device_id">The ID of the device for which event descriptions are to be retrieved.</param>
/// <param name="_mesh_family">The mesh family to which the device belongs.</param>
/// <returns>
/// A vector of pointers to `cmd_device_event_description` structures, representing
/// the event descriptions associated with the specified device.
/// </returns>
std::vector<const cmd_device_event_description*> cmn_device::get_cmn_device_event_list(cmn_mesh_device_ids _device_id, cmn_mesh_family _mesh_family) const
{
    std::vector<const cmd_device_event_description*> result;

    if (m_mesh_devices_desc.count(_mesh_family) &&
        m_mesh_devices_desc.at(_mesh_family).count(_device_id))
        for (const auto& [_, event_desc] : m_mesh_devices_event_desc.at(_mesh_family).at(_device_id))
            result.push_back(&event_desc);

    return result;
}
