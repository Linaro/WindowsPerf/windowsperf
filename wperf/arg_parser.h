// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <map>
#include <vector>
#include <deque>
#include <array>
#include <string>
#include <set>
#include <unordered_map>
#include "arg_parser_arg.h"

typedef std::vector<std::wstring> wstr_vec;
using namespace ArgParserArg;

namespace ArgParser {
    constexpr auto ERROR_INDICATOR_PADDING = '~';
    constexpr auto ERROR_INDICATOR_CARET = '^';

    enum class COMMAND_CLASS {
#define WINDOWSPERF_ARGS_OPT(...)
#define WINDOWSPERF_ARG_COMMAND(A,B,C,D,E,F,G,H,I) F,
#define WINDOWSPERF_ARGS_POS(...)
#include "wperf-common/wperf-cli-args.def"
#undef WINDOWSPERF_ARGS_OPT
#undef WINDOWSPERF_ARG_COMMAND
#undef WINDOWSPERF_ARGS_POS
        NO_COMMAND
    };

    class arg_parser_arg_command : public arg_parser_arg_pos {

    public:
        arg_parser_arg_command(
            const std::wstring name,
            const std::vector<std::wstring> alias,
            const std::wstring description,
            const std::wstring useage_text,
            const COMMAND_CLASS command,
            const wstr_vec examples,
			const int arg_count = 0,
            check_funcs_vect_t check_funcs = {}
        ) : arg_parser_arg_pos(name, alias, description, {}, arg_count, check_funcs), m_examples(examples), m_command(command), m_useage_text(useage_text) {};
        const COMMAND_CLASS m_command = COMMAND_CLASS::NO_COMMAND;
        const wstr_vec m_examples;
        const std::wstring m_useage_text;

        std::wstring get_usage_text() const override;

        std::wstring get_examples() const;

    };


#define WINDOWSPERF_ARG_COMMAND(A,B,C,D,E,F,G,H,I) extern arg_parser_arg_command A;
#define WINDOWSPERF_ARGS_OPT(A,B,C,D,E,F) extern arg_parser_arg_opt A;
#define WINDOWSPERF_ARGS_POS(A,B,C,D,E,F,G) extern arg_parser_arg_pos A;
#include "wperf-common/wperf-cli-args.def"
#undef WINDOWSPERF_ARGS_OPT
#undef WINDOWSPERF_ARGS_POS
#undef WINDOWSPERF_ARG_COMMAND

    extern std::vector<arg_parser_arg_command*> commands_list;

    extern std::vector<arg_parser_arg*> flags_list;
    extern COMMAND_CLASS selected_command;

    class arg_parser
    {
    public:
        void parse(
            _In_ const int argc,
            _In_reads_(argc) const wchar_t* argv[]
        );
        void print_help() const;

        std::vector<arg_parser_arg*> parsed_args = {};
        wstr_vec m_arg_array;

    protected:
        void throw_invalid_arg(const std::wstring& arg, const std::wstring& additional_message = L"") const;
    private:
        void print_help_header() const;
        void print_help_prompt() const;

    };

}
