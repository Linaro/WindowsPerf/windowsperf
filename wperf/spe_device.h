#pragma once
// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <windows.h>
#include <vector>
#include <map>
#include <string>
#include <assert.h>
#include "wperf-common/macros.h"
#include "wperf-common/iorequest.h"
#include "utils.h"

#define SPE_BUFFER_PAGE_COUNT       512
#define SPE_INTERVAL_DEFAULT_VAL    0 // [UINT32] PMSIDR_EL1.Interval

class spe_device
{
public:
    spe_device();
    ~spe_device();

    void init();

    // Consts

    // All availabkle filters for SPE `arm_spe_0//`
    static const std::vector<std::wstring> m_filter_names;

    // Filters also have aliases, this structure helps to translate alias to filter name
    static const std::map<std::wstring, std::wstring> m_filter_names_aliases;

    // Filter names have also short descriptions
    static const std::map<std::wstring, std::wstring> m_filter_names_description;

    // Helper functions

    static std::wstring get_spe_version_name(UINT64 id_aa64dfr0_el1_value);
    static bool is_spe_supported(UINT64 id_aa64dfr0_el1_value);
    static void get_samples(const std::vector<UINT8>& spe_buffer, std::vector<FrameChain>& raw_samples, std::map<UINT64, std::wstring>& spe_events);

    static bool is_filter_name(std::wstring fname) {
        assert(m_filter_names.size() == m_filter_names_aliases.size());
        assert(m_filter_names.size() == m_filter_names_description.size());

        if (is_filter_name_alias(fname))
            fname = m_filter_names_aliases.at(fname);
        return std::find(m_filter_names.begin(), m_filter_names.end(), fname) != m_filter_names.end();
    }

    static bool is_filter_name_alias(std::wstring fname) {
        assert(m_filter_names.size() == m_filter_names_aliases.size());
        assert(m_filter_names.size() == m_filter_names_description.size());

        return m_filter_names_aliases.count(fname);
    }

    static std::wstring get_filter_name(std::wstring fname);

    static uint64_t spe_recommended_min_sampling_interval(uint64_t pmsidr_el1_value)
    {
        // Interval, bits[11:8]
        // Recommended minimum sampling interval.This provides guidance from the implementer to the smallest minimum sampling interval, N.
        const uint64_t interval = (pmsidr_el1_value & PMSIDR_EL1_Interval_MASK) >> 8;
        switch (interval)
        {
        // All other values are reserved.
        case 0b0000: return 256;
        case 0b0010: return 512;
        case 0b0011: return 768;
        case 0b0100: return 1024;
        case 0b0101: return 1536;
        case 0b0110: return 2048;
        case 0b0111: return 3072;
        default:
        case 0b1000: return 4096;
        }
    }

    static uint64_t spe_recommended_max_minlat(uint64_t pmsidr_el1_value)
    {
        uint64_t min_latency = SPE_CTL_FLAG_MINLAT_MASK;    // By default 16-bit value
        // CountSize, bits [19:16] - Defines the size of the counters.
        // 0b0010	    12-bit saturating counters.
        // 0b0011       16-bit saturating counters.
        uint64_t countsize = (pmsidr_el1_value & PMSIDR_EL1_CountSize_MASK) >> 16;
        if ((countsize & 0xF) == PMSIDR_EL1_CountSize_12Bit)
            min_latency &= SPE_CTL_FLAG_VAL_12_BIT_MASK;  // trim latency to 12-bit value

        return min_latency;
    }

    static std::pair<uint64_t, uint64_t> get_filter_min_max_values(std::wstring filter_name, uint64_t pmsidr_el1_value)
    {
        assert(m_filter_names.size() == m_filter_names_aliases.size());
        assert(m_filter_names.size() == m_filter_names_description.size());

        /* Maps filter (full) name to its min/max filter values.
        * Stores min, max value of the filter in the std::pair.
        * Here, we initialize:
        * - only filters which are on/off (0/1) or
        * - filters with known min or max value.
        *
        * Rest of these filters should get dynamic min/max values deduction.
        */
        std::map<std::wstring, std::pair<uint64_t, uint64_t>> m_filter_min_max_values = {
            {L"load_filter",    {0, 1}},    // on/off filter
            {L"store_filter",   {0, 1}},    // on/off filter
            {L"branch_filter",  {0, 1}},    // on/off filter
            {L"ts_enable",      {0, 1}},    // on/off filter
            {L"min_latency",    {0, spe_recommended_max_minlat(pmsidr_el1_value)}}, // Can be 12-bit or 16-bit
            {L"jitter",         {0, 1}},    // on/off filter
            {L"period",         {spe_recommended_min_sampling_interval(pmsidr_el1_value), SPE_CTL_INTERVAL_VAL_MASK}}
        };

        /*
        * We want to make sure each place has the same number of filters specified.
        */
        assert(m_filter_names.size() == m_filter_min_max_values.size());
        assert(m_filter_min_max_values.count(filter_name) == 1);
        return m_filter_min_max_values[filter_name];
    }

private:

};
