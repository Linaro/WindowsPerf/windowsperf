// BSD 3-Clause License
//
// Copyright (c) 2024, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "spe_device.h"
#include "spe_parser.h"

const std::vector<std::wstring> spe_device::m_filter_names = {
    L"load_filter",
    L"store_filter",
    L"branch_filter",
    L"ts_enable",
    L"min_latency",
    L"jitter",
    L"period"
};


// Filters also have aliases, this structure helps to translate alias to filter name
const std::map<std::wstring, std::wstring> spe_device::m_filter_names_aliases = {
    { L"ld",  L"load_filter" },
    { L"st",  L"store_filter" },
    { L"b" ,  L"branch_filter" },
    { L"ts",  L"ts_enable" },
    { L"min", L"min_latency" },
    { L"j",   L"jitter" },
    { L"per", L"period" }
};

// Filters also have aliases, this structure helps to translate alias to filter name
const std::map<std::wstring, std::wstring> spe_device::m_filter_names_description = {
    { L"load_filter",   L"Enables collection of load sampled operations, including atomic operations that return a value to a register." },
    { L"store_filter",  L"Enables collection of store sampled operations, including all atomic operations." },
    { L"branch_filter", L"Enables collection of branch sampled operations, including direct and indirect branches and exception returns." },
    { L"ts_enable",     L"Enables timestamping with value of generic timer." },
    { L"min_latency",   L"Collect only samples with this latency or higher." },
    { L"jitter",        L"Use jitter to avoid resonance when sampling." },
    { L"period",        L"Use period to set interval counter reload value." },
};

spe_device::spe_device() {}

spe_device::~spe_device() {}

void spe_device::init()
{

}

void spe_device::get_samples(const std::vector<UINT8>& spe_buffer, std::vector<FrameChain>& raw_samples, std::map<UINT64, std::wstring>& spe_events)
{
    std::vector<std::pair<std::wstring, UINT64>> records = SPEParser::read_spe_buffer(spe_buffer);
    std::map<std::wstring, unsigned int> event_map;
    UINT32 events_idx = 0;
    for (const auto& rec : records)
    {
        FrameChain fc{ 0 };
        fc.pc = rec.second;
        if (event_map.find(rec.first) != event_map.end())
        {
            fc.spe_event_idx = event_map[rec.first];
        }
        else {
            fc.spe_event_idx = events_idx;
            spe_events[events_idx] = rec.first;
            event_map[rec.first] = events_idx;
            events_idx++;
        }
        raw_samples.push_back(fc);
    }
}

std::wstring spe_device::get_spe_version_name(UINT64 id_aa64dfr0_el1_value)
{
    UINT8 aa64_pms_ver = ID_AA64DFR0_EL1_PMSVer(id_aa64dfr0_el1_value);

    // Print SPE feature version
    std::wstring spe_str = L"unknown SPE configuration!";
    switch (aa64_pms_ver)
    {
    case 0b000: spe_str = L"not implemented"; break;
    case 0b001: spe_str = L"FEAT_SPE"; break;
    case 0b010: spe_str = L"FEAT_SPEv1p1"; break;
    case 0b011: spe_str = L"FEAT_SPEv1p2"; break;
    case 0b100: spe_str = L"FEAT_SPEv1p3"; break;
    }
    return spe_str;
}

bool spe_device::is_spe_supported(UINT64 id_aa64dfr0_el1_value)
{
    UINT8 aa64_pms_ver = ID_AA64DFR0_EL1_PMSVer(id_aa64dfr0_el1_value);
    return aa64_pms_ver >= 0b001;
}

/// <summary>
/// Retrieves the filter name for the given filter name or alias. If the filter name is an alias,
/// the corresponding filter name is returned; otherwise, the original filter name is returned.
/// </summary>
/// <param name="fname">The filter name or alias for which the filter name is to be retrieved.</param>
/// <returns>The filter name if the full filter name.</returns>
std::wstring spe_device::get_filter_name(std::wstring fname)
{
    assert(is_filter_name(fname));

    if (is_filter_name_alias(fname))
        return m_filter_names_aliases.at(fname);
    return fname;
}
