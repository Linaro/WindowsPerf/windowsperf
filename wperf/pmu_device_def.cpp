// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#include "pmu_device.h"
#include "parsers.h"
#include "config.h"


std::map<uint8_t, const wchar_t*> pmu_device::arm64_vendor_names =
{
    {static_cast<uint8_t>(0x41), L"Arm Limited"},
    {static_cast<uint8_t>(0x42), L"Broadcomm Corporation"},
    {static_cast<uint8_t>(0x43), L"Cavium Inc"},
    {static_cast<uint8_t>(0x44), L"Digital Equipment Corporation"},
    {static_cast<uint8_t>(0x46), L"Fujitsu Ltd"},
    {static_cast<uint8_t>(0x49), L"Infineon Technologies AG"},
    {static_cast<uint8_t>(0x4D), L"Motorola or Freescale Semiconductor Inc"},
    {static_cast<uint8_t>(0x4E), L"NVIDIA Corporation"},
    {static_cast<uint8_t>(0x50), L"Applied Micro Circuits Corporation"},
    {static_cast<uint8_t>(0x51), L"Qualcomm Inc"},
    {static_cast<uint8_t>(0x56), L"Marvell International Ltd"},
    {static_cast<uint8_t>(0x69), L"Intel Corporation"},
    {static_cast<uint8_t>(0xC0), L"Ampere Computing"},
    {static_cast<uint8_t>(0x6D), L"Microsoft Corporation"}
};

std::map<std::wstring, struct product_configuration> pmu_device::m_product_configuration = {
#define WPERF_TS_EVENTS(...)
#define WPERF_TS_METRICS(...)
#define WPERF_TS_PRODUCT_CONFIGURATION(A,B,C,D,E,F,G,H,I,J) {std::wstring(L##A),{std::wstring(L##B),C,D,E,F,G,H,std::wstring(L##I),std::wstring(L##J)}},
#define WPERF_TS_ALIAS(...)
#define WPERF_TS_GROUPS_METRICS(...)
#include "wperf-common/neoverse-n1.def"
#include "wperf-common/neoverse-n2-r0p0.def"
#include "wperf-common/neoverse-n2-r0p1.def"
#include "wperf-common/neoverse-n2-r0p3.def"
#include "wperf-common/neoverse-n2.def"
#include "wperf-common/neoverse-n3.def"
#include "wperf-common/neoverse-v1.def"
#include "wperf-common/neoverse-v2.def"
#include "wperf-common/neoverse-v3.def"
#undef WPERF_TS_EVENTS
#undef WPERF_TS_METRICS
#undef WPERF_TS_PRODUCT_CONFIGURATION
#undef WPERF_TS_ALIAS
#undef WPERF_TS_GROUPS_METRICS
};

std::map<std::wstring, std::wstring> pmu_device::m_product_alias =
{
#define WPERF_TS_EVENTS(...)
#define WPERF_TS_METRICS(...)
#define WPERF_TS_PRODUCT_CONFIGURATION(...)
#define WPERF_TS_ALIAS(A,B) {L##A,L##B},
#define WPERF_TS_GROUPS_METRICS(...)
#include "wperf-common/neoverse-n2-r0p0.def"
#include "wperf-common/neoverse-n2-r0p1.def"
#undef WPERF_TS_EVENTS
#undef WPERF_TS_METRICS
#undef WPERF_TS_PRODUCT_CONFIGURATION
#undef WPERF_TS_ALIAS
#undef WPERF_TS_GROUPS_METRICS
};

void pmu_device::init_ts_metrics()
{   // Initialize Telemetry Solution metrics from each product
#       define WPERF_TS_EVENTS(...)
#       define WPERF_TS_METRICS(A,B,C,D,E,F,G,H) m_product_metrics[std::wstring(L##A)][std::wstring(L##B)] = { std::wstring(L##B),std::wstring(L##C),std::wstring(L##D),std::wstring(L##E),std::wstring(L##F),std::wstring(L##G), std::wstring(L##H) };
#       define WPERF_TS_PRODUCT_CONFIGURATION(...)
#       define WPERF_TS_ALIAS(...)
#       define WPERF_TS_GROUPS_METRICS(...)
#include "wperf-common/neoverse-n1.def"
#include "wperf-common/neoverse-n2-r0p0.def"
#include "wperf-common/neoverse-n2-r0p1.def"
#include "wperf-common/neoverse-n2-r0p3.def"
#include "wperf-common/neoverse-n2.def"
#include "wperf-common/neoverse-n3.def"
#include "wperf-common/neoverse-v1.def"
#include "wperf-common/neoverse-v2.def"
#include "wperf-common/neoverse-v3.def"

#       undef WPERF_TS_EVENTS
#       undef WPERF_TS_METRICS
#       undef WPERF_TS_PRODUCT_CONFIGURATION
#       undef WPERF_TS_ALIAS
#       undef WPERF_TS_GROUPS_METRICS
}

void pmu_device::init_ts_groups_metrics()
{   // Initialize Telemetry Solution groups of metrics from each product
#       define WPERF_TS_EVENTS(...)
#       define WPERF_TS_METRICS(...)
#       define WPERF_TS_PRODUCT_CONFIGURATION(...)
#       define WPERF_TS_ALIAS(...)
#       define WPERF_TS_GROUPS_METRICS(A,B,C,D,E) m_product_groups_metrics[std::wstring(L##A)][std::wstring(L##B)] = { std::wstring(L##B),std::wstring(L##C),std::wstring(L##D), std::wstring(L##E) };
#include "wperf-common/neoverse-n1.def"
#include "wperf-common/neoverse-n2-r0p0.def"
#include "wperf-common/neoverse-n2-r0p1.def"
#include "wperf-common/neoverse-n2-r0p3.def"
#include "wperf-common/neoverse-n2.def"
#include "wperf-common/neoverse-n3.def"
#include "wperf-common/neoverse-v1.def"
#include "wperf-common/neoverse-v2.def"
#include "wperf-common/neoverse-v3.def"
#       undef WPERF_TS_EVENTS
#       undef WPERF_TS_METRICS
#       undef WPERF_TS_PRODUCT_CONFIGURATION
#       undef WPERF_TS_ALIAS
#       undef WPERF_TS_GROUPS_METRICS
}

// Initialize Telemetry Solution events for Noeverse-N1/V1
void pmu_device::init_ts_events_nv1()
{
#       define WPERF_TS_EVENTS(A,B,C,D,E,F) m_product_events[std::wstring(L##A)][std::wstring(L##D)] = { std::wstring(L##D),uint16_t(C),std::wstring(L##E),std::wstring(L##F) };
#       define WPERF_TS_METRICS(...)
#       define WPERF_TS_PRODUCT_CONFIGURATION(...)
#       define WPERF_TS_ALIAS(...)
#       define WPERF_TS_GROUPS_METRICS(...)
#include "wperf-common/neoverse-n1.def"
#include "wperf-common/neoverse-v1.def"
#       undef WPERF_TS_EVENTS
#       undef WPERF_TS_METRICS
#       undef WPERF_TS_PRODUCT_CONFIGURATION
#       undef WPERF_TS_ALIAS
#       undef WPERF_TS_GROUPS_METRICS

}

// Initialize Telemetry Solution events for Noeverse-N2/V2 (all revisions)
void pmu_device::init_ts_events_nv2()
{
#       define WPERF_TS_EVENTS(A,B,C,D,E,F) m_product_events[std::wstring(L##A)][std::wstring(L##D)] = { std::wstring(L##D),uint16_t(C),std::wstring(L##E),std::wstring(L##F) };
#       define WPERF_TS_METRICS(...)
#       define WPERF_TS_PRODUCT_CONFIGURATION(...)
#       define WPERF_TS_ALIAS(...)
#       define WPERF_TS_GROUPS_METRICS(...)
#include "wperf-common/neoverse-n2-r0p0.def"
#include "wperf-common/neoverse-n2-r0p1.def"
#include "wperf-common/neoverse-n2-r0p3.def"
#include "wperf-common/neoverse-n2.def"
#include "wperf-common/neoverse-v2.def"
#       undef WPERF_TS_EVENTS
#       undef WPERF_TS_METRICS
#       undef WPERF_TS_PRODUCT_CONFIGURATION
#       undef WPERF_TS_ALIAS
#       undef WPERF_TS_GROUPS_METRICS
}

// Initialize Telemetry Solution events for Noeverse-N3/V3
void pmu_device::init_ts_events_nv3()
{
#       define WPERF_TS_EVENTS(A,B,C,D,E,F) m_product_events[std::wstring(L##A)][std::wstring(L##D)] = { std::wstring(L##D),uint16_t(C),std::wstring(L##E),std::wstring(L##F) };
#       define WPERF_TS_METRICS(...)
#       define WPERF_TS_PRODUCT_CONFIGURATION(...)
#       define WPERF_TS_ALIAS(...)
#       define WPERF_TS_GROUPS_METRICS(...)
#include "wperf-common/neoverse-n3.def"
#include "wperf-common/neoverse-v3.def"
#       undef WPERF_TS_EVENTS
#       undef WPERF_TS_METRICS
#       undef WPERF_TS_PRODUCT_CONFIGURATION
#       undef WPERF_TS_ALIAS
#       undef WPERF_TS_GROUPS_METRICS
}

void pmu_device::init_armv8_events()
{
#       define WPERF_ARMV8_ARCH_EVENTS(A,B,C,D,E) m_product_events[std::wstring(L##A)][std::wstring(L##D)] = { std::wstring(L##D),uint16_t(C),std::wstring(L##E) };
#       include "wperf-common/armv8-arch-events.def"
#       undef WPERF_ARMV8_ARCH_EVENTS
}

void pmu_device::init_armv9_events()
{
#       define WPERF_ARMV9_ARCH_EVENTS(A,B,C,D,E) m_product_events[std::wstring(L##A)][std::wstring(L##D)] = { std::wstring(L##D),uint16_t(C),std::wstring(L##E) };
#       include "wperf-common/armv9-arch-events.def"
#       undef WPERF_ARMV9_ARCH_EVENTS
}
