#pragma once
// BSD 3-Clause License
//
// Copyright (c) 2025, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <string>
#include <vector>
#include <windows.h>

namespace SPEParser
{
    /// <summary>
    /// The known bits of packet types.
    /// Some bits may vary, so certain values need to be masked to determine the packet type.
    /// </summary>
    /// <seealso cref="get_packet_type"/>
    enum class PacketType : UINT8
    {
        PADDING        = 0x00, // precise
        END            = 0x01, // precise
        EVENTS         = 0x42, // use MASK_MIDDLE
        DATA_SOURCE    = 0x43, // use MASK_MIDDLE
        OPERATION_TYPE = 0x48, // use MASK_LAST2
        CONTEXT        = 0x64, // use MASK_LAST2
        TIMESTAMP      = 0x71, // precise
        COUNTER        = 0x98, // use MASK_LAST3
        ADDRESS        = 0xB0, // use MASK_LAST3

        LONG_HEADER    = 0x20, // use MASK_LAST2
        ADDRESS_LONG   = 0xFD, // Pseudo-value, indicating LONG_HEADER + ADDRESS
        COUNTER_LONG   = 0xFE, // Pseudo-value, indicating LONG_HEADER + COUNTER

        MASK_MIDDLE    = 0b11001111,
        MASK_LAST2     = 0b11111100,
        MASK_LAST3     = 0b11111000,

        UNKNOWN        = 0xFF, // A value known to be invalid
    };

    // Allow masking PacketTypes
    inline PacketType operator&(UINT8 left, PacketType right)
    {
        return static_cast<PacketType>(left & static_cast<UINT8>(right));
    }

    // Allow extracting values from PacketTypes
    inline UINT8 operator~(PacketType type)
    {
        return ~static_cast<UINT8>(type);
    }

    enum class ParsingState : UINT8
    {
        READING_HEADER,
        READING_DATA,
        FINISHED,
    };

    enum class AddressType : UINT8
    {
        INSTRUCTION_VIRTUAL    = 0b00000,
        BRANCH_TARGET          = 0b00001,
        DATA_ACCESS_VIRTUAL    = 0b00010,
        DATA_ACCESS_PHYSICAL   = 0b00011,
        PREVIOUS_BRANCH_TARGET = 0b00100,

        // 0b0011x and 0b1xxxx are implementation defined, all other values reserved
    };

    enum class CounterType : UINT8
    {
        TOTAL_LATENCY                 = 0b00000,
        ISSUE_LATENCY                 = 0b00001,
        TRANSLATION_LATENCY           = 0b00010,
        ALTERNATE_CLOCK_ISSUE_LATENCY = 0b00100,

        // 0b0011x and 0b1xxxx are implementation defined, all other values reserved
    };

    enum class OperationTypeClass : UINT8
    {
        OTHER               = 0b00,
        LOAD_STORE_ATOMIC   = 0b01,
        BRANCH_OR_EXCEPTION = 0b10,
    };

    enum class OperationTypeSubclass : UINT8
    {
        // === Other
        OTHER_MASK                     = 0b10001000,
        OTHER                          = 0b00000000,
        DATA_PROCESSING_SVE_VECTOR     = 0b00001000,
        DATA_PROCESSING_SME_ARRAY      = 0b10001000,

        // === Load, store, or atomic
        // Most subclasses share this mask
        LOAD_STORE_ATOMIC_DEFAULT_MASK = 0b11111110,
        LOAD_STORE_GENERAL_PURPOSE     = 0b00000000,
        LOAD_STORE_SIMD_FP             = 0b00000100,
        LOAD_STORE_UNSPECIFIED         = 0b00010000,
        LOAD_STORE_ALLOCATION_TAG      = 0b00010100,
        LOAD_STORE_SYSTEM_REGISTER     = 0b00110000,
        LOAD_STORE_MEMORY_COPY         = 0b00100000,

        // Some subclasses come with their own masks
        LOAD_STORE_EXTENDED_MASK       = 0b11100010,
        LOAD_STORE_EXTENDED            = 0b00000010,
        LOAD_STORE_SVE_OR_SME_MASK     = 0b00001010,
        LOAD_STORE_SVE_OR_SME          = 0b00001000,
        LOAD_STORE_GCS_MASK            = 0b11111010,
        LOAD_STORE_GCS                 = 0b01000000,
        LOAD_STORE_MEMORY_SET_MASK     = 0b11111111,
        LOAD_STORE_MEMORY_SET          = 0b00100101,

        // === Branch or exception doesn't use enumerated subclasses
        BRANCH_OR_EXCEPTION            = 0b00000000,
    };

    inline OperationTypeSubclass operator&(UINT64 left, OperationTypeSubclass right)
    {
        return static_cast<OperationTypeSubclass>(left & static_cast<UINT8>(right));
    }

    PacketType get_packet_type(UINT8 hdr);

    class Packet
    {
    public:
        UINT8 m_header0 = 0, m_header1 = 0;
        size_t m_bytes_read = 0;
        size_t m_bytes_to_read = 0;
        size_t m_payload_index = 0;
        UINT64 m_payload = 0;
        PacketType m_type = PacketType::UNKNOWN;
        ParsingState m_state = ParsingState::READING_HEADER;

        Packet() = default;
        void add(UINT8 byte);
    };

    enum class SecurityState : UINT8
    {
        SECURE     = 0b00,
        NON_SECURE = 0b01,
        REALM      = 0b11,
        // There is no encoding for Root
    };

    class AddressPacket : public Packet
    {
    public:
        UINT64 m_address;
        UINT8 m_index;
        AddressType m_address_type;

        explicit AddressPacket(const Packet& p);

        SecurityState get_security_state() const;
        UINT8 get_exception_level() const; // 0 to 3
        UINT8 get_tag() const;
        bool get_tag_checked() const;
        UINT8 get_physical_address_tag() const; // 0 to 15
    };

    class CounterPacket : public Packet
    {
    public:
        explicit CounterPacket(const Packet& p);
        UINT8 get_index() const;
        UINT16 get_count() const;
        CounterType get_counter_type() const;
    };

    class EventsPacket : public Packet
    {
        union EventBit
        {
            struct
            {
                // byte 0
                unsigned generated_exception : 1;
                unsigned architecturally_retired : 1;
                unsigned level1_data_cache_access : 1;
                unsigned level1_data_cache_refill_or_miss : 1;
                unsigned tlb_access : 1;
                unsigned tlb_walk : 1;
                unsigned not_taken : 1;
                unsigned mispredicted : 1;

                // byte 1
                unsigned last_level_cache_access : 1;
                unsigned last_level_cache_miss : 1;
                unsigned remote_access : 1;
                unsigned misalignment : 1;
                unsigned implementation_defined_events_12_to_15 : 4;

                // byte 2
                unsigned transactional : 1;
                unsigned partial_or_empty_predicate : 1;
                unsigned empty_predicate : 1;
                unsigned level_2_data_cache_access : 1;
                unsigned level_2_data_cache_miss : 1;
                unsigned cache_data_modified : 1;
                unsigned recently_fetched : 1;
                unsigned data_snooped : 1;

                // byte 3
                unsigned streaming_sve_mode : 1;
                unsigned smcu_or_external_coprocessor_operation : 1;
                unsigned implementation_defined_events_26_to_31 : 6;

                // bytes 4 and 5
                unsigned reads_as_zero : 16; // Unused

                // bytes 6 and 7
                unsigned implementation_defined_events_48_to_63 : 16;
            } bitFields;
            UINT64 value;
        };

    public:
        explicit EventsPacket(const Packet& p);
        int get_size() const; // The number of bytes that are valid: 1, 2, 4, 8
        std::wstring get_event_desc() const;
    };

    class OperationTypePacket : public Packet
    {
    public:
        OperationTypeClass m_class;
        UINT8 m_subclass;

        explicit OperationTypePacket(const Packet& p);
        OperationTypeSubclass get_subclass_category() const;
        std::wstring get_subclass() const;
        std::wstring get_class() const;
        std::wstring get_desc() const;
    };

    class Record
    {
    public:
        std::vector<Packet> m_packets;
        std::wstring m_event_name;
        std::wstring m_optype_name;
        std::wstring m_full_desc;
        UINT64 m_pc = 0;

        Record() = default;
        void parse_record_data();
    };

    std::vector<std::pair<std::wstring, UINT64>> read_spe_buffer(const std::vector<UINT8>& buffer);
}
