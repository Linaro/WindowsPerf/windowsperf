// BSD 3-Clause License
//
// Copyright (c) 2023, Arm Limited
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <cassert>
#include "timeline.h"
#include "utils.h"

void timeline::write_to_file() const
{
    std::wofstream timeline_outfile;
    timeline_outfile.open(header.filename);

    print_header(timeline_outfile);
    timeline_outfile << std::endl;

    for (const auto& item : events_hdr)
        timeline_outfile << item.source << L',';
    for (const auto& item : metrics_hdr)
        timeline_outfile << item.source << L',';
    timeline_outfile << std::endl;

    for (const auto& item : events_hdr)
        timeline_outfile << item.name << L',';
    for (const auto& item : metrics_hdr)
        timeline_outfile << L"M@" << item.name << L',';
    timeline_outfile << std::endl;

    // Expecting one metrics row for one events row, except if one of the collections is empty
    assert(events.size() == metrics.size() || events.empty() ^ metrics.empty());
    for (size_t i = 0; i < std::max(events.size(), metrics.size()); i++)
    {
        assert(events_hdr.empty() || events.at(i).size() == events_hdr.size());
        assert(metrics_hdr.empty() || metrics.at(i).size() == metrics_hdr.size());

        if (i < events.size())
            for (const auto& value : events[i])
                timeline_outfile << value << L',';
        if (i < metrics.size())
            for (const auto& value : metrics[i])
                timeline_outfile << value << L',';
        timeline_outfile << std::endl;
    }
}

void timeline::print_header(std::wofstream& timeline_outfile) const
{
    timeline_outfile << L"Multiplexing,";
    timeline_outfile << (header.multiplexing ? L"TRUE" : L"FALSE");
    timeline_outfile << std::endl;

    if (header.include_kernel)
    {
        timeline_outfile << L"Kernel mode,";
        timeline_outfile << (*header.include_kernel ? L"TRUE" : L"FALSE");
        timeline_outfile << std::endl;
    }

    timeline_outfile << L"Count interval,";
    timeline_outfile << DoubleToWideString(header.count_interval);
    timeline_outfile << std::endl;

    timeline_outfile << L"Vendor," << header.vendor_name;
    timeline_outfile << std::endl;

    timeline_outfile << L"Event class," << header.event_class;
    timeline_outfile << std::endl;
}
