#!/bin/bash

# BSD 3-Clause License
#
# Copyright (c) 2025, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#
# Script Description
#
#
# This bash script processes JSON files in a specified directory. It looks for
# files with names starting with "cortex-" and having a ".json" extension. For
# each matching file, it extracts specific fields using the jq command: cpuid,
# cpu, architecture, and pmu_architecture. The script then performs the following operations:
#
# 1.  Splits the cpuid field into the last three characters and the rest.
# 2.  Removes the suffix "-a" from the architecture field.
# 3.  Skips printing the details if the architecture field contains the "armv7" prefix.
# 4.  Prints the extracted fields along with the file name without the extension.
# 5.  Printouts are in WindowsPerf project macro `WPERF_TS_PRODUCT_CONFIGURATION` format.
#
# Usage:
#
#   $ git clone git@github.com:ARM-software/data.git
#   $ ./scan-data.sh ./data
#


# Check if the directory is provided as an argument
if [ -z "$1" ]; then
    echo "This bash script processes JSON files in a ARM PMU data directory,"
    echo "extracting and printing specific fields in a WindowsPerf macro format"
    echo "Usage: $0 <directory>"
    exit 1
fi

# Directory containing the JSON files
directory="$1"

# Loop through each file in the directory with name starting with "cortex-" and extension .json
for file in "$directory"/cortex-*.json; do
    # Check if the file exists
    if [[ -f "$file" ]]; then
        # Extract the required fields using jq
        cpuid=$(jq -r '.cpuid' "$file")
        cpu=$(jq -r '.cpu' "$file")
        architecture=$(jq -r '.architecture' "$file")
        pmu_architecture=$(jq -r '.pmu_architecture' "$file")
        cpu_name=$(basename "$file" .json)

        # Split cpuid into the last three characters and the rest
        part_num=${cpuid: -3}
        implementer=${cpuid:0:-3}   # 0x...

        # Architecture in Telemetry solution format
        architecture_ts=${architecture%-a}

        # Skip printing if architecture contains "armv7" prefix
        if [[ "$architecture" == armv7* ]]; then
          continue
        fi

        # Skip printing if architecture contains "-r" prefix
        if [[ "$architecture" == *-r ]]; then
          continue
        fi

        #     WPERF_TS_PRODUCT_CONFIGURATION("neoverse-n1","armv8.2",0x41,0,0,0,5,0xd0c,"pmu_v3","Neoverse N1")
        echo 'WPERF_TS_PRODUCT_CONFIGURATION("'"$cpu_name"'"'","'"'"$architecture_ts"'"'",$implementer,0,0,0,0,0x$part_num,"'"'"$pmu_architecture"'"'","'"'"$cpu"'")'
    fi
done
