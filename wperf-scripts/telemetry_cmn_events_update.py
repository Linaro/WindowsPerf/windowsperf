#!/usr/bin/env python3

# BSD 3-Clause License
#
# Copyright (c) 2025, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


"""
Module is designed to automate the generation of .def files containing macros
that describe the CMN mesh, its devices, and associated events. This module
simplifies the process of defining and managing CMN components by providing a
structured and efficient way to create the necessary definitions.
"""

import json
import sys
import os


def load_json_file(file_path):
    """
    Loads the content of a JSON file into a Python data structure.

    Args:
        file_path (str): The path to the JSON file.

    Returns:
        dict: The content of the JSON file as a dictionary.

    Raises:
        ValueError: If the file does not have a .json extension.
        FileNotFoundError: If the file does not exist.
"""
    if not file_path.endswith('.json'):
        raise ValueError("Only .json files are allowed!")

    if not os.path.isfile(file_path):
        raise FileNotFoundError(f"No such file: '{file_path}'")

    with open(file_path, 'r', encoding='utf-8') as file:
        data = json.load(file)

    return data


def ts_parse_cmn(j):
    """
    Parses CMN information from a JSON object and prints the CMN configuration.

    Args:
        j (dict): A dictionary containing CMN information.

    The function extracts the CMN name from the provided JSON object and prints the CMN configuration
    in a formatted manner.
"""
    DEFINE = "WPERF_TS_CMN"
    cmn_name = j["name"]

    print("//")
    print("// CMN (Coherent Mesh Network):")
    print("//")
    print(f"{DEFINE}({cmn_name})")


def ts_parse_cmn_devices(j):
    """
    Parses CMN device information from a JSON object and prints device configurations.

    Args:
        j (dict): A dictionary containing CMN device information.

    The function extracts the CMN name and device specifications from the provided JSON object.
    It then prints the device configuration in a formatted manner, including the CMN name,
    device name, device ID, and device description.
"""
    DEFINE = "WPERF_TS_CMN_DEVICE"
    cmn_name = j["name"]

    print("//")
    print("// Device configuration for: cmn_name,device_name,device_id,device_description")
    print("//")
    for device_name, device_desc in j["pmu_specs"]["CMN"].items():
        device_id = device_desc["product_configuration"]["device_id"]
        device_description = device_desc["document"]["description"]

        print(f"{DEFINE}({cmn_name}, {device_name}, {device_id}, \"{device_description}\")")


def ts_parse_cmn_events(j):
    """
    Parses CMN event information from a JSON object and prints event configurations.

    Args:
        j (dict): A dictionary containing CMN event information.

    The function extracts the CMN name and event specifications from the provided JSON object.
    It then prints the event configuration in a formatted manner, including the CMN name,
    device name, event name, event index, event title, and event description.
"""
    DEFINE = "WPERF_TS_CMN_EVENTS"
    cmn_name = j["name"]

    for device_name, device_desc in j["pmu_specs"]["CMN"].items():
        device_events = device_desc["events"]
        device_description = device_desc["document"]["description"]
        print("//")
        print(f"// Events for CMN device '{device_name}' a '{device_description}'")
        print("// Columns: cmn_name,device_name,event_name.upper,event_index,event_name.lower,event_title,event_description")
        print("//")
        for event_name, event_desc  in device_events.items():
            # "PMU_HNF_CACHE_MISS_EVENT": {
            #     "code": "0x0001",
            #     "title": "PMU_HNF_CACHE_MISS_EVENT",
            #     "description": "Counts total cache misses in first lookup result (high priority)",
            #     "common": false,
            #     "accesses": [
            #         "PMU"
            #     ],
            #     "architectural": false,
            #     "impdef": false,
            #     "system": false
            # },
            if "code" in event_desc:
                #
                # If `core` is not specified, most likely this event is not a standard CMN PMU event.
                #
                event_index = event_desc["code"]
                event_title = event_desc["title"]
                event_description = event_desc["description"].replace('\n', '\\n')

                print(f"{DEFINE}({cmn_name}, {device_name}, {event_name.upper()}, {event_index}, \"{event_name.lower()}\", \"{event_title}\", \"{event_description}\")")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"Usage: python {os.path.basename(sys.argv[0])} <path_to_json_file>")
        sys.exit(1)

    json_file_path = sys.argv[1]

    try:
        data = load_json_file(json_file_path)
    except Exception as e:
        print(f"Error: opening file {json_file_path}: {e}")

    ts_parse_cmn(data)
    print()
    ts_parse_cmn_devices(data)
    print()
    ts_parse_cmn_events(data)
    print()
