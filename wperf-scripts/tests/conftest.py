#!/usr/bin/env python3

# BSD 3-Clause License
#
# Copyright (c) 2024, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

""" Module is a handler to pytest hook `pytest_terminal_summary`.
"""

import json
import multiprocessing
import os
import platform
import pytest
import re
import shutil
import sys

from datetime import datetime
from os import linesep
from common import is_json, run_command

# ANSI escape codes
RED = "\033[31m"
GREEN = "\033[32m"
RESET = "\033[0m"

PYTEST_CLI_OPTION_ENABLE_BENCH_XPERF = "--enable-bench-xperf"
PYTEST_CLI_OPTION_ENABLE_BUILD_TARGET = "--use"


def pytest_addoption(parser):
    """ Pass different values to a test function, depending on command line options
    """
    parser.addoption(
        PYTEST_CLI_OPTION_ENABLE_BENCH_XPERF, action="store_true",
    )
    parser.addoption(
        PYTEST_CLI_OPTION_ENABLE_BUILD_TARGET, action="store", default=None,
        help="Select solution configuration for test (copy atrifacts to tests/ directory)"
    )


def pytest_terminal_summary(terminalreporter, exitstatus, config):
    """ Create sections for terminal summary.
        See https://docs.pytest.org/en/6.2.x/reference.html#pytest.hookspec.pytest_terminal_summary
    """
    terminalreporter.section("WindowsPerf Test Configuration")

    # OS information
    p = platform.platform()
    m = platform.machine()
    v = sys.version
    proc = platform.processor()
    cnt = multiprocessing.cpu_count()
    terminalreporter.write(f"OS: {p}, {m} {linesep}")
    terminalreporter.write(f"CPU: {cnt} x {proc} {linesep}")
    terminalreporter.write(f"Python: {v} {linesep}")

    # Test time
    now = datetime.now()
    date_time = now.strftime("%d/%m/%Y, %H:%M:%S")
    terminalreporter.write(f"Time: {date_time} {linesep}")

    # Get WindowsPerf versions
    cmd = 'wperf --version --json'
    stdout, _ = run_command(cmd.split())
    assert is_json(stdout)
    json_output = json.loads(stdout)

    for w in json_output["Version"]:
        component = w["Component"]
        version = w["Version"]
        gitver = w["GitVer"]
        featurestring = w["FeatureString"]
        # Add section entry
        terminalreporter.write(f"{component}: {version}.{gitver}{featurestring} {linesep}")

    my_build_target = config.getoption(PYTEST_CLI_OPTION_ENABLE_BUILD_TARGET)
    terminalreporter.write(f"Configuration: {PYTEST_CLI_OPTION_ENABLE_BUILD_TARGET}={my_build_target}")


def pytest_configure(config):
    """
    This hook is used to perform global setup tasks.
    """
    my_build_target = config.getoption(PYTEST_CLI_OPTION_ENABLE_BUILD_TARGET)
    DEFAULT_PLATFORM = "ARM64"

    def parse_solution_configuration(solution_str):
        """
        Parses the SolutionConfigurationPlatforms section from a given solution text.

        Extracts the lines between 'preSolution' and 'EndGlobalSection' within the
        'GlobalSection(SolutionConfigurationPlatforms)' and creates a dictionary
        where the keys are the text before '=' and the values are the text after '='.

        Parameters:
        solution_str (str): The larger text containing the relevant section.

        Returns:
        dict: A dictionary with keys and values parsed from the section.
        """
        match = re.search(r'GlobalSection\(SolutionConfigurationPlatforms\) = preSolution(.*?)EndGlobalSection', solution_str, re.DOTALL)
        section_text = match.group(1) if match else ""

        # Parse lines and create a dictionary
        result = {}
        if section_text:
            lines = section_text.strip().split('\n')
            for line in lines:
                if '=' in line:
                    key, value = line.strip().split('=')
                    result[key.strip()] = value.strip()

        return result

    def gen_files_to_copy(config, platform):
        """ Create list of files to copy based on build `configuration` and `platform`.
        """
        files_to_copy = [
            # wperf
            os.path.join(current_dir, "..", "..", "wperf", platform, config, "wperf.exe"),
            # wperf-lib
            os.path.join(current_dir, "..", "..", "wperf-lib", platform, config, "wperf-lib.dll"),
            os.path.join(current_dir, "..", "..", "wperf-lib", platform, config, "wperf-lib.lib"),
            os.path.join(current_dir, "..", "..", "wperf-lib-app", platform, config, "wperf-lib-app.exe"),
            os.path.join(current_dir, "..", "..", "wperf-lib-app", "wperf-lib-c-compat", platform, config, "wperf-lib-c-compat.exe"),
            os.path.join(current_dir, "..", "..", "wperf-lib-app", "wperf-lib-timeline", platform, config, "wperf-lib-timeline.exe"),
            # Kernel Driver
            os.path.join(current_dir, "..", "..", "wperf-driver", platform, config, "wperf-driver", "wperf-driver.sys"),
            os.path.join(current_dir, "..", "..", "wperf-driver", platform, config, "wperf-driver", "wperf-driver.cat"),
            os.path.join(current_dir, "..", "..", "wperf-driver", platform, config, "wperf-driver", "wperf-driver.inf"),
        ]
        files_to_copy = list(map(os.path.normpath, files_to_copy))

        return files_to_copy

    def copy_file(source_path, destination_folder):
        """ Copy file from `source_path` to `destination_folder`.
        """
        if not os.path.isfile(source_path):
            raise FileNotFoundError(f"Source file does not exist: {source_path}")
        destination_path = os.path.join(destination_folder, os.path.basename(source_path))
        if os.path.exists(destination_path):
            os.remove(destination_path)
        shutil.copy2(source_path, destination_path)

    def chech_build_consistency(files_to_copy):
        """ Check if files from `files_to_copy` list exist.
            Result:
                map: map of file_path -> bool (file exist)
        """
        result = {}
        for file_path in files_to_copy:
            result[file_path] = os.path.exists(file_path)
        return result

    #
    # --use, requires full repository structure in place
    #
    if my_build_target:
        # Get path to project solution
        current_dir = os.path.dirname(os.path.abspath(__file__))
        file_path = os.path.join(current_dir, '..', '..', 'windowsperf.sln')

        # Get all configurations from the solution file
        solution_configurations = {}
        with open(file_path, 'r') as file:
            solution_configurations = parse_solution_configuration(file.read())

        if "|" not in my_build_target:
            my_build_target += f"|{DEFAULT_PLATFORM}"

        config, platform = None, None
        for configuration, _ in solution_configurations.items():
            if configuration.lower().strip() == my_build_target.lower():
                config, platform = my_build_target.split("|")
                break

        # Copy artifacts to test directory or yield error
        if config and platform:
            # Store in `configurations.cfg` information about current configuration settings
            with open(os.path.join(current_dir, 'configurations.cfg'), 'w') as configurations_file:
                configurations_file.write(f"[SolutionConfigurationPlatforms]\n")
                configurations_file.write(f"Configuration = {config}|{platform}\n")
                configurations_file.write(f"[Build Artifacts]\n")

                #
                # Check if any of the build artifacts is missing, if so print report and exit
                #
                files_to_copy = gen_files_to_copy(config, platform)
                required_files = chech_build_consistency(files_to_copy)
                if any(exist is False for exist in required_files.values()):
                    for file_path, exist in required_files.items():
                        indicator = GREEN+"OK"+RESET if exist else RED+"MISSING"+RESET
                        print (f"[{indicator}] -> {file_path}")
                    pytest.exit(f"Missing build artifact '{file_path}'!\nIs configuration '{PYTEST_CLI_OPTION_ENABLE_BUILD_TARGET}={my_build_target}' pre-built?")

                #
                # Copy build artifacts to `tests/`, check if artifacts are missing
                #
                for file_path in files_to_copy:
                    if not os.path.exists(file_path):
                        configurations_file.write(f"{file_path} = <missing>")
                        pytest.exit(f"Missing build artifact '{file_path}'!\nIs configuration '{PYTEST_CLI_OPTION_ENABLE_BUILD_TARGET}={my_build_target}' pre-built?")
                    configurations_file.write(f"{file_path} = {current_dir}\n")
                    try:
                        copy_file(file_path, current_dir)
                    except FileNotFoundError as e:
                        configurations_file.write(str(e))
        else:
            configurations_str = ''.join([configuration + "\n" for configuration in solution_configurations.keys()])
            pytest.exit(f"Unknown configuration '{PYTEST_CLI_OPTION_ENABLE_BUILD_TARGET}={my_build_target}', available configurations are:\n{configurations_str}")
