#!/usr/bin/env python3

# BSD 3-Clause License
#
# Copyright (c) 2024, Arm Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
This module provides a suite of tests for the WindowsPerf CLI, targeting
command line options related to the SPE feature. The tests ensure that the
CLI behaves as expected when using SPE-related options, requires `+spe`
WindowsPerf build.
"""


import pytest

from common import run_command
from common import get_spe_version, wperf_event_is_available
from common import get_spe_filter_min_max


#
# Skip whole module if SPE is not supported in this configuration
#

# Skip whole module if ustress is not supported by this CPU
spe_device = get_spe_version()
assert spe_device is not None
if not spe_device.startswith("FEAT_SPE"):
    pytest.skip(f"unsupported configuration: no SPE support in HW, see spe_device.version_name={spe_device}",
        allow_module_level=True)

## Is SPE enabled in `wperf` CLI?
if not wperf_event_is_available("arm_spe_0//"):
    pytest.skip(f"unsupported configuration: no SPE support in `wperf`, see spe_device.version_name={spe_device}",
        allow_module_level=True)

### Test cases

@pytest.mark.parametrize("spe_filters",
[
    ("x"),
    ("_"),
    ("asdasd"),
    ("b"),                  # Should be e.g. `b=0` or `b=1`
    ("ld"),                 # Should be e.g. `ld=0`
    ("st"),                 # Should be e.g. `st=0`
    ("load_filter"),        # Should be e.g. `load_filter=0`
    ("store_filter"),       # Should be e.g. `store_filter=0`
    ("branch_filter"),      # Should be e.g. `branch_filter=0`

    ("load_filter=0,x"),
    ("load_filter=0,_"),
    ("load_filter=0,asdasd"),
    ("load_filter=0,b"),                  # Should be e.g. `b=0` or `b=1`
    ("load_filter=0,ld"),                 # Should be e.g. `ld=0`
    ("load_filter=0,st"),                 # Should be e.g. `st=0`
    ("load_filter=0,load_filter"),        # Should be e.g. `load_filter=0`
    ("load_filter=0,store_filter"),       # Should be e.g. `store_filter=0`
    ("load_filter=0,branch_filter"),      # Should be e.g. `branch_filter=0`
]
)
def test_cpython_bench_spe_cli_incorrect_filter(spe_filters):
    """ Test `wperf record` with SPE CLI not OK filters, we expect:

    "incorrect SPE filter: '<spe_filters>' in <spe_filters>"

    """
    #
    # Run for CPython payload but we should fail when we hit CLI parser errors
    #
    cmd = f"wperf record -e arm_spe_0/{spe_filters}/ -c 4 --timeout 3 --json -- python_d.exe -c 10**10**100"
    _, stderr = run_command(cmd)

    assert b"unexpected arg" not in stderr
    assert b"incorrect SPE filter:" in stderr

@pytest.mark.parametrize("spe_filters",
[
    ("b=2"),
    ("ld=3"),
    ("st=4"),
    ("load_filter=10"),
    ("store_filter=0x2"),
    ("branch_filter=0xf1da"),
    ("b=1,load_filter=10"),
    ("b=0,store_filter=0x2"),
    ("b=1,branch_filter=0xf1da"),

    ("ld=1,min=0x10000"),
    ("load_filter=1,min=0x10000"),
    ("ld=1,min=0x10000,store_filter=0"),
    ("load_filter=1,min=0x10000,store_filter=0"),
    ("ld=1,min_latency=0x10000"),
    ("load_filter=1,min_latency=0x10000"),
    ("ld=1,min_latency=0x10000,store_filter=0"),
    ("load_filter=1,min_latency=0x10000,store_filter=0"),
]
)
def test_cpython_bench_spe_cli_filter_value_out_of_range(spe_filters):
    """ Test `wperf record` with SPE CLI filter value

    "SPE filter 'ts_enable' value out of range, use: 0-1"

    """
    #
    # Run for CPython payload but we should fail when we hit CLI parser errors
    #
    cmd = f"wperf record -e arm_spe_0/{spe_filters}/ -c 4 --timeout 3 --json -- python_d.exe -c 10**10**100"
    _, stderr = run_command(cmd)

    assert b"unexpected arg" not in stderr
    assert b"value out of range, use:" in stderr

@pytest.mark.parametrize("spe_filters",
[
    ("=0"),
    ("=1"),

    ("load_filter=0,=0"),
    ("load_filter=0,=1"),
]
)
def test_cpython_bench_spe_cli_incorrect_filter_name(spe_filters):
    """ Test `wperf record` with SPE CLI not OK filters, we expect:

    "incorrect SPE filter name: '<spe_filters>' in <spe_filters>"

    This error is for "empty" filter name.
    """
    #
    # Run for CPython payload but we should fail when we hit CLI parser errors
    #
    cmd = f"wperf record -e arm_spe_0/{spe_filters}/ -c 4 --timeout 3 --json -- python_d.exe -c 10**10**100"
    _, stderr = run_command(cmd)

    assert b"unexpected arg" not in stderr
    assert b"incorrect SPE filter name:" in stderr

@pytest.mark.parametrize("spe_filters",
[
    ("load_filter="),
    ("store_filter="),
    ("branch_filter="),
    ("ts_enable="),
    ("jitter="),
    ("period="),
    ("ld="),
    ("st="),
    ("b="),
    ("ts="),
    ("j="),
    ("per="),
    ("load_filter=a"),
    ("load_filter=one"),
    ("ts_enable=one"),
    ("jitter=one"),
    ("period=one"),

    ("load_filter=0,load_filter="),
    ("load_filter=0,store_filter="),
    ("load_filter=0,branch_filter="),
    ("load_filter=0,period="),
    ("load_filter=0,jitter="),
    ("load_filter=0,ld="),
    ("load_filter=0,st="),
    ("load_filter=0,b="),
    ("load_filter=0,ts="),
    ("load_filter=0,j="),
    ("load_filter=0,per="),
    ("load_filter=0,load_filter=a"),
    ("load_filter=0,load_filter=one"),
    ("ts_enable=1,ts_enable=one"),

    ("b=-1"),
    ("ld=-1"),
    ("st=-1"),
    ("j=-1"),
    ("per=-1"),
    ("load_filter=-10"),
    ("store_filter=-0x2"),
    ("branch_filter=-0xf"),

    ("load_filter=0,load_filter=-1"),
    ("load_filter=0,store_filter=-1"),
    ("load_filter=0,branch_filter=-1"),
    ("load_filter=0,jitter=-1"),
    ("load_filter=0,ld=-1"),
    ("load_filter=0,st=-1"),
    ("load_filter=0,b=-1"),
    ("load_filter=0,j=-1"),
    ("load_filter=0,per=-1"),
]
)
def test_cpython_bench_spe_cli_incorrect_filter_value(spe_filters):
    """ Test `wperf record` with SPE CLI not OK filters, we expect:

    "incorrect SPE filter value: '<spe_filters>' in <spe_filters>"
    """
    #
    # Run for CPython payload but we should fail when we hit CLI parser errors
    #
    cmd = f"wperf record -e arm_spe_0/{spe_filters}/ -c 4 --timeout 3 --json -- python_d.exe -c 10**10**100"
    _, stderr = run_command(cmd)

    assert b"unexpected arg" not in stderr
    assert b"incorrect SPE filter value:" in stderr


@pytest.mark.parametrize("filter_name",
[
    ("load_filter"),
    ("store_filter"),
    ("branch_filter"),
    ("ts_enable"),
    ("min_latency"),
    ("jitter"),
    ("period")
]
)
def test_bench_spe_cli_filter_min_max_values(filter_name):
    """ For specified SPE filters check if filter min and max values are reasonable.
    """
    filter_value_min, filter_value_max = get_spe_filter_min_max(filter_name)

    #
    # Basic smoke tests for filter min/max values
    #
    assert filter_value_min >= 0, f"filter_name={filter_name}, min={filter_value_min}, max={filter_value_max}"
    assert filter_value_max >= 0, f"filter_name={filter_name}, min={filter_value_min}, max={filter_value_max}"
    assert filter_value_max > filter_value_min, f"filter_name={filter_name}, min={filter_value_min}, max={filter_value_max}"


@pytest.mark.parametrize("filter_name",
[
    ("load_filter"),
    ("store_filter"),
    ("branch_filter"),
    ("ts_enable"),
    ("jitter"),
]
)
def test_bench_spe_cli_filter_min_max_0_1_filters(filter_name):
    """ For specified SPE filters check if filter min and max values are reasonable.
        Some filters are on/off filters.
    """
    filter_value_min, filter_value_max = get_spe_filter_min_max(filter_name)

    assert filter_value_min == 0
    assert filter_value_max == 1
